<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.btnClearList = New System.Windows.Forms.Button()
        Me.btnShutdown = New System.Windows.Forms.Button()
        Me.btnListen = New System.Windows.Forms.Button()
        Me.listView = New System.Windows.Forms.ListView()
        Me.lbl_TotalTrans = New System.Windows.Forms.Label()
        Me.lbl_DB = New System.Windows.Forms.Label()
        Me.cb_LogFile = New System.Windows.Forms.CheckBox()
        Me.lbl_IP_Address = New System.Windows.Forms.Label()
        Me.lbl_Build = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.cb_SLC = New System.Windows.Forms.CheckBox()
        Me.cb_LOGIX = New System.Windows.Forms.CheckBox()
        Me.cb2_LogFile = New System.Windows.Forms.CheckBox()
        Me.SuspendLayout()
        '
        'btnClearList
        '
        Me.btnClearList.Location = New System.Drawing.Point(12, 82)
        Me.btnClearList.Name = "btnClearList"
        Me.btnClearList.Size = New System.Drawing.Size(133, 27)
        Me.btnClearList.TabIndex = 13
        Me.btnClearList.Text = "Clear List"
        Me.btnClearList.UseVisualStyleBackColor = True
        '
        'btnShutdown
        '
        Me.btnShutdown.Enabled = False
        Me.btnShutdown.Location = New System.Drawing.Point(12, 53)
        Me.btnShutdown.Name = "btnShutdown"
        Me.btnShutdown.Size = New System.Drawing.Size(133, 27)
        Me.btnShutdown.TabIndex = 11
        Me.btnShutdown.Text = "Shut Down"
        Me.btnShutdown.UseVisualStyleBackColor = True
        '
        'btnListen
        '
        Me.btnListen.Location = New System.Drawing.Point(12, 24)
        Me.btnListen.Name = "btnListen"
        Me.btnListen.Size = New System.Drawing.Size(133, 27)
        Me.btnListen.TabIndex = 10
        Me.btnListen.Text = "Listen"
        Me.btnListen.UseVisualStyleBackColor = True
        '
        'listView
        '
        Me.listView.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.listView.Location = New System.Drawing.Point(3, 115)
        Me.listView.Name = "listView"
        Me.listView.Size = New System.Drawing.Size(985, 511)
        Me.listView.TabIndex = 7
        Me.listView.UseCompatibleStateImageBehavior = False
        '
        'lbl_TotalTrans
        '
        Me.lbl_TotalTrans.AutoSize = True
        Me.lbl_TotalTrans.Location = New System.Drawing.Point(194, 53)
        Me.lbl_TotalTrans.Name = "lbl_TotalTrans"
        Me.lbl_TotalTrans.Size = New System.Drawing.Size(121, 13)
        Me.lbl_TotalTrans.TabIndex = 18
        Me.lbl_TotalTrans.Text = "Total Transaction Count"
        '
        'lbl_DB
        '
        Me.lbl_DB.AutoSize = True
        Me.lbl_DB.Location = New System.Drawing.Point(194, 25)
        Me.lbl_DB.Name = "lbl_DB"
        Me.lbl_DB.Size = New System.Drawing.Size(22, 13)
        Me.lbl_DB.TabIndex = 19
        Me.lbl_DB.Text = "DB"
        '
        'cb_LogFile
        '
        Me.cb_LogFile.AutoSize = True
        Me.cb_LogFile.Location = New System.Drawing.Point(481, 30)
        Me.cb_LogFile.Name = "cb_LogFile"
        Me.cb_LogFile.Size = New System.Drawing.Size(103, 17)
        Me.cb_LogFile.TabIndex = 20
        Me.cb_LogFile.TabStop = False
        Me.cb_LogFile.Text = "Write to Log File"
        Me.cb_LogFile.UseVisualStyleBackColor = True
        '
        'lbl_IP_Address
        '
        Me.lbl_IP_Address.AutoSize = True
        Me.lbl_IP_Address.Location = New System.Drawing.Point(793, 34)
        Me.lbl_IP_Address.Name = "lbl_IP_Address"
        Me.lbl_IP_Address.Size = New System.Drawing.Size(58, 13)
        Me.lbl_IP_Address.TabIndex = 22
        Me.lbl_IP_Address.Text = "IP Address"
        '
        'lbl_Build
        '
        Me.lbl_Build.AutoSize = True
        Me.lbl_Build.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Build.Location = New System.Drawing.Point(193, 82)
        Me.lbl_Build.Name = "lbl_Build"
        Me.lbl_Build.Size = New System.Drawing.Size(39, 13)
        Me.lbl_Build.TabIndex = 23
        Me.lbl_Build.Text = "BUILD"
        '
        'Timer1
        '
        Me.Timer1.Interval = 15000
        '
        'cb_SLC
        '
        Me.cb_SLC.AutoSize = True
        Me.cb_SLC.Location = New System.Drawing.Point(641, 32)
        Me.cb_SLC.Name = "cb_SLC"
        Me.cb_SLC.Size = New System.Drawing.Size(46, 17)
        Me.cb_SLC.TabIndex = 25
        Me.cb_SLC.Text = "SLC"
        Me.cb_SLC.UseVisualStyleBackColor = True
        '
        'cb_LOGIX
        '
        Me.cb_LOGIX.AutoSize = True
        Me.cb_LOGIX.Location = New System.Drawing.Point(641, 53)
        Me.cb_LOGIX.Name = "cb_LOGIX"
        Me.cb_LOGIX.Size = New System.Drawing.Size(51, 17)
        Me.cb_LOGIX.TabIndex = 26
        Me.cb_LOGIX.Text = "Logix"
        Me.cb_LOGIX.UseVisualStyleBackColor = True
        '
        'cb2_LogFile
        '
        Me.cb2_LogFile.AutoSize = True
        Me.cb2_LogFile.Location = New System.Drawing.Point(481, 48)
        Me.cb2_LogFile.Name = "cb2_LogFile"
        Me.cb2_LogFile.Size = New System.Drawing.Size(104, 17)
        Me.cb2_LogFile.TabIndex = 27
        Me.cb2_LogFile.Text = "Write To Screen"
        Me.cb2_LogFile.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(993, 638)
        Me.Controls.Add(Me.cb2_LogFile)
        Me.Controls.Add(Me.cb_LOGIX)
        Me.Controls.Add(Me.cb_SLC)
        Me.Controls.Add(Me.lbl_Build)
        Me.Controls.Add(Me.lbl_IP_Address)
        Me.Controls.Add(Me.cb_LogFile)
        Me.Controls.Add(Me.lbl_DB)
        Me.Controls.Add(Me.lbl_TotalTrans)
        Me.Controls.Add(Me.btnClearList)
        Me.Controls.Add(Me.btnShutdown)
        Me.Controls.Add(Me.btnListen)
        Me.Controls.Add(Me.listView)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "OES SLC AND LOGIX"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents btnClearList As System.Windows.Forms.Button
    Private WithEvents btnShutdown As System.Windows.Forms.Button
    Private WithEvents btnListen As System.Windows.Forms.Button
    Private WithEvents listView As System.Windows.Forms.ListView
    Friend WithEvents lbl_DB As System.Windows.Forms.Label
    Friend WithEvents cb_LogFile As System.Windows.Forms.CheckBox
    Friend WithEvents lbl_IP_Address As System.Windows.Forms.Label
    Friend WithEvents lbl_Build As System.Windows.Forms.Label
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Public WithEvents lbl_TotalTrans As System.Windows.Forms.Label
    Friend WithEvents cb_SLC As System.Windows.Forms.CheckBox
    Friend WithEvents cb_LOGIX As System.Windows.Forms.CheckBox
    Friend WithEvents cb2_LogFile As System.Windows.Forms.CheckBox

End Class
