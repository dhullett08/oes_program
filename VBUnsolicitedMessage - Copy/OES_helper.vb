Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Data
Imports System.Configuration
Public Class OES_helper
    Private conn As New OleDbConnection
    Public Sub New(ByVal ConnectionString As String)
        ' Dummy parameter is here simply to make the signature different
        conn.ConnectionString = ConnectionString
    End Sub

    Public Function ConnectionString() As String
        Return conn.ConnectionString.ToString
    End Function

    Public Function GetSysdate() As String
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Author:                       Jeff ElDridge
        ' Date:                         2012-03-27
        ' Schema:                       
        ' Table References:             
        ' Description:                  Returns the Sysdate
        ' Revisions:
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim Cmdstr As String = "Select sysdate from dual"

        Dim cmd As New OleDbCommand(Cmdstr, conn)
        Try
            conn.Open()
            Return cmd.ExecuteScalar
        Catch ex As OleDbException
            Throw New Exception(ex.Message)
        Finally
            conn.Close()
            If conn IsNot Nothing Then
                conn.Dispose()
                cmd.Dispose()
            End If
        End Try
    End Function

    Public Function GetBusinessId() As String
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Author:                       Jeff ElDridge
        ' Date:                         2012-03-27
        ' Schema:                       
        ' Table References:             
        ' Description:                  Returns the Business id
        ' Revisions:
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim Cmdstr As String = "select business_id from MANITDBA.MANIT_PARAMETERS"

        Dim cmd As New OleDbCommand(Cmdstr, conn)
        Try
            conn.Open()
            Return cmd.ExecuteScalar
        Catch ex As OleDbException
            Throw New Exception(ex.Message)
        Finally
            conn.Close()
            If conn IsNot Nothing Then
                conn.Dispose()
                cmd.Dispose()
            End If
        End Try
    End Function

    Public Function GetPlcModelSetupCount(ByVal P_CELL_ID As String, ByVal P_MODEL_ID As String) As Integer
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Author:                       Jeff ElDridge
        ' Date:                         2012-07-18
        ' Schema:                       
        ' Table References:             
        ' Description:                  Returns Count of PLC MODEL SETUP for Model \ Cell
        ' Revisions:
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim Cmdstr As String = "select nvl(count(*),0) from plc_model_setup " & _
                               "where cell_id = '" & P_CELL_ID & "' and model_id = '" & P_MODEL_ID & "'"

        Dim cmd As New OleDbCommand(Cmdstr, conn)
        Try
            conn.Open()
            Return cmd.ExecuteScalar
        Catch ex As OleDbException
            Throw New Exception(ex.Message)
        Finally
            conn.Close()
            If conn IsNot Nothing Then
                conn.Dispose()
                cmd.Dispose()
            End If
        End Try
    End Function

    Public Function GetLabelIDFromCell_ID(ByVal P_CELL_ID As String) As String
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Author:                       Jeff ElDridge
        ' Date:                         2012-08-03
        ' Schema:                       
        ' Table References:             
        ' Description:                  Returns the Label_id for setup Model from Cell_id
        ' Revisions:
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim Cmdstr As String = "select msc.LABEL_CODE_1 " & _
                               "from cell c, routing r, MANIT_STATION_CONFIGURATION msc " & _
                               "where c.cell_id = '" & P_CELL_ID & "' " & _
                               "and r.model_id = c.model_id " & _
                               "and r.operation_id = c.operation_id " & _
                               "and r.cell_id = c.cell_id " & _
                               "and R.CONFIGURATION_ID = msc.CONFIGURATION_ID"

        Dim cmd As New OleDbCommand(Cmdstr, conn)
        Try
            conn.Open()
            Return cmd.ExecuteScalar
        Catch ex As OleDbException
            Throw New Exception(ex.Message)
        Finally
            conn.Close()
            If conn IsNot Nothing Then
                conn.Dispose()
                cmd.Dispose()
            End If
        End Try
    End Function

    Public Sub InsertIntoTX_LOG(ByVal Transaction As String, ByVal Ethernet_ID As String, ByVal Configuration_ID As String, ByVal User_ID As String, ByVal Data As String, ByVal ManitVersion As String, ByVal Transaction_id As String)
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Author:                       Jeff Judkins
        ' Date:                         07-24-2007
        ' Schema:                       manitdba
        ' Table References:             TX_LOG     
        ' Description:                  
        ' Revisions:
        ' Notes:                        
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If Transaction.Length > 11 Then
            Transaction = Transaction.Substring(0, 11)
        End If
        If Ethernet_ID.Length > 12 Then
            Ethernet_ID = Ethernet_ID.Substring(0, 12)
        End If
        If Configuration_ID.Length > 255 Then
            Configuration_ID = Configuration_ID.Substring(0, 255)
        End If
        If User_ID.Length > 12 Then
            User_ID = User_ID.Substring(0, 12)
        End If
        If Data.Length > 255 Then
            Data = Data.Substring(0, 255)
        End If
        If Transaction_id.Length > 255 Then
            Transaction_id = Transaction_id.Substring(0, 255)
        End If


        Dim Cmdstr As String = "Insert into tx_log(log_id,transaction,ethernet_id,configuration_id,user_id,time_stamp,data,manit_version,TRANSACTION_ID) values (tx_seq.nextval,'" & Transaction & "','" & Ethernet_ID & "','" & Configuration_ID & "','" & User_ID & "',sysdate,'" & Data & "','" & ManitVersion & "','" & Transaction_id & "')"
        ''Dim cmd As New OleDbCommand(Cmdstr, conn)
        'Try
        '    conn.Open()
        '    cmd.ExecuteNonQuery()
        'Catch ex As OleDbException
        '    Throw New Exception("InsertIntoTX_LOG: " & ex.Message)
        'Finally
        '    conn.Close()
        '    ''conn.Dispose()
        'End Try



        Using cmd As New OleDbCommand(Cmdstr, conn)
            With cmd
                ' Do stuff with cn
                conn.Open()
                cmd.ExecuteNonQuery()
                conn.Close()
                If conn IsNot Nothing Then
                    conn.Dispose()
                    cmd.Dispose()
                End If
            End With
        End Using

    End Sub



End Class
