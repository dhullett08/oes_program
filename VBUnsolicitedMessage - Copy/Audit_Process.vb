Imports System.Data.OleDb

Public Class Audit_Process
    Private conn As New OleDbConnection


    Public Sub New(ByVal ConnectionString As String)
        conn.ConnectionString = ConnectionString
    End Sub

    Dim mPROCESS_INDICATOR As Integer 'IN
    Dim mCELL_ID As String          'IN
    Dim mITEM_ID As String      'IN
    Dim mOUTWORD0 As Integer      'OUT
    Dim mOUTWORD1 As Integer      'OUT
    Dim mOUTWORD2 As Integer      'OUT
    Dim mOUTWORD3 As Integer      'OUT
    Dim mOUTWORD4 As Integer      'OUT
    Dim mSTATUS_CODE As Integer   'OUT
    Dim mOUTWORD6 As Integer      'OUT
    Dim mOUTWORD7 As Integer      'OUT
    Dim mOUTWORD8 As Integer      'OUT
    Dim mOUTWORD9 As Integer      'OUT
    Dim mOUTWORD10 As Integer      'OUT
    Dim mOUTWORD11 As Integer      'OUT
    Dim mOUTWORD12 As Integer      'OUT
    Dim mOUTWORD13 As Integer      'OUT
    Dim mOUTWORD14 As Integer      'OUT
    Dim mOUTWORD15 As Integer      'OUT
    Dim mOUTWORD16 As Integer      'OUT



    Public Property PROCESS_INDICATOR()
        Get
            Return mPROCESS_INDICATOR
        End Get
        Set(ByVal value)
            mPROCESS_INDICATOR = value
        End Set
    End Property
    Public Property CELL_ID()
        Get
            Return mCELL_ID
        End Get
        Set(ByVal value)
            mCELL_ID = value
        End Set
    End Property
    Public Property ITEM_ID()
        Get
            Return mITEM_ID
        End Get
        Set(ByVal value)
            mITEM_ID = value
        End Set
    End Property
    Public Property OUTWORD0()
        Get
            Return mOUTWORD0
        End Get
        Set(ByVal value)
            mOUTWORD0 = value
        End Set
    End Property
    Public Property OUTWORD1()
        Get
            Return mOUTWORD1
        End Get
        Set(ByVal value)
            mOUTWORD1 = value
        End Set
    End Property
    Public Property OUTWORD2()
        Get
            Return mOUTWORD2
        End Get
        Set(ByVal value)
            mOUTWORD2 = value
        End Set
    End Property
    Public Property OUTWORD3()
        Get
            Return mOUTWORD3
        End Get
        Set(ByVal value)
            mOUTWORD3 = value
        End Set
    End Property
    Public Property OUTWORD4()
        Get
            Return mOUTWORD4
        End Get
        Set(ByVal value)
            mOUTWORD4 = value
        End Set
    End Property
    Public Property STATUS_CODE()
        Get
            Return mSTATUS_CODE
        End Get
        Set(ByVal value)
            mSTATUS_CODE = value
        End Set
    End Property
    Public Property OUTWORD6()
        Get
            Return mOUTWORD6
        End Get
        Set(ByVal value)
            mOUTWORD6 = value
        End Set
    End Property
    Public Property OUTWORD7()
        Get
            Return mOUTWORD7
        End Get
        Set(ByVal value)
            mOUTWORD7 = value
        End Set
    End Property
    Public Property OUTWORD8()
        Get
            Return mOUTWORD8
        End Get
        Set(ByVal value)
            mOUTWORD8 = value
        End Set
    End Property
    Public Property OUTWORD9()
        Get
            Return mOUTWORD9
        End Get
        Set(ByVal value)
            mOUTWORD9 = value
        End Set
    End Property
    Public Property OUTWORD10()
        Get
            Return mOUTWORD10
        End Get
        Set(ByVal value)
            mOUTWORD10 = value
        End Set
    End Property
    Public Property OUTWORD11()
        Get
            Return mOUTWORD11
        End Get
        Set(ByVal value)
            mOUTWORD11 = value
        End Set
    End Property
    Public Property OUTWORD12()
        Get
            Return mOUTWORD12
        End Get
        Set(ByVal value)
            mOUTWORD12 = value
        End Set
    End Property
    Public Property OUTWORD13()
        Get
            Return mOUTWORD13
        End Get
        Set(ByVal value)
            mOUTWORD13 = value
        End Set
    End Property
    Public Property OUTWORD14()
        Get
            Return mOUTWORD14
        End Get
        Set(ByVal value)
            mOUTWORD14 = value
        End Set
    End Property
    Public Property OUTWORD15()
        Get
            Return mOUTWORD15
        End Get
        Set(ByVal value)
            mOUTWORD15 = value
        End Set
    End Property
    Public Property OUTWORD16()
        Get
            Return mOUTWORD16
        End Get
        Set(ByVal value)
            mOUTWORD16 = value
        End Set
    End Property


    Public Function CallAuditProcess(ByVal AuditObject As Audit_Process) As String
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Author:                       Jeff ElDridge
        ' Date:                         08-02-2012
        ' Schema:                       
        ' Table References:                   
        ' Description:                  
        ' Revisions:
        ' Notes:                        
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim cmd As New OleDbCommand("arc.plc_audit", conn)
        cmd.CommandType = CommandType.StoredProcedure
        ' INWORDS
        Dim P_PROCESS_INDICATOR As New OleDbParameter("PROCESS_INDICATOR", OleDbType.VarChar, 40)
        P_PROCESS_INDICATOR.Direction = ParameterDirection.Input
        cmd.Parameters.Add(P_PROCESS_INDICATOR).Value = AuditObject.mPROCESS_INDICATOR

        Dim P_CELL_ID As New OleDbParameter("CELL_ID", OleDbType.VarChar, 40)
        P_CELL_ID.Direction = ParameterDirection.Input
        cmd.Parameters.Add(P_CELL_ID).Value = AuditObject.mCELL_ID

        Dim P_ITEM_ID As New OleDbParameter("ITEM_ID", OleDbType.VarChar, 40)
        P_ITEM_ID.Direction = ParameterDirection.Input
        cmd.Parameters.Add(P_ITEM_ID).Value = AuditObject.mITEM_ID


        ' OUTWORDS

        Dim OUTWORD0 As New OleDbParameter("OUTWORD0", OleDbType.VarNumeric)
        OUTWORD0.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(OUTWORD0)

        Dim OUTWORD1 As New OleDbParameter("OUTWORD1", OleDbType.VarNumeric)
        OUTWORD1.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(OUTWORD1)

        Dim OUTWORD2 As New OleDbParameter("OUTWORD2", OleDbType.VarNumeric)
        OUTWORD2.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(OUTWORD2)

        Dim OUTWORD3 As New OleDbParameter("OUTWORD3", OleDbType.VarNumeric)
        OUTWORD3.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(OUTWORD3)

        Dim OUTWORD4 As New OleDbParameter("OUTWORD4", OleDbType.VarNumeric)
        OUTWORD4.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(OUTWORD4)

        Dim P_STATUS_CODE As New OleDbParameter("STATUS_CODE", OleDbType.VarNumeric)
        P_STATUS_CODE.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_STATUS_CODE)

        Dim OUTWORD6 As New OleDbParameter("OUTWORD6", OleDbType.VarNumeric)
        OUTWORD6.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(OUTWORD6)

        Dim OUTWORD7 As New OleDbParameter("OUTWORD7", OleDbType.VarNumeric)
        OUTWORD7.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(OUTWORD7)

        Dim OUTWORD8 As New OleDbParameter("OUTWORD8", OleDbType.VarNumeric)
        OUTWORD8.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(OUTWORD8)

        Dim OUTWORD9 As New OleDbParameter("OUTWORD9", OleDbType.VarNumeric)
        OUTWORD9.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(OUTWORD9)

        Dim OUTWORD10 As New OleDbParameter("OUTWORD10", OleDbType.VarNumeric)
        OUTWORD10.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(OUTWORD10)

        Dim OUTWORD11 As New OleDbParameter("OUTWORD11", OleDbType.VarNumeric)
        OUTWORD11.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(OUTWORD11)

        Dim OUTWORD12 As New OleDbParameter("OUTWORD12", OleDbType.VarNumeric)
        OUTWORD12.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(OUTWORD12)

        Dim OUTWORD13 As New OleDbParameter("OUTWORD13", OleDbType.VarNumeric)
        OUTWORD13.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(OUTWORD13)

        Dim OUTWORD14 As New OleDbParameter("OUTWORD14", OleDbType.VarNumeric)
        OUTWORD14.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(OUTWORD14)

        Dim OUTWORD15 As New OleDbParameter("OUTWORD15", OleDbType.VarNumeric)
        OUTWORD15.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(OUTWORD15)

        Dim OUTWORD16 As New OleDbParameter("OUTWORD16", OleDbType.VarNumeric)
        OUTWORD16.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(OUTWORD16)
        
        Try
            conn.Open()
            cmd.ExecuteNonQuery()
            Dim ReturnString As String
            ReturnString = OUTWORD0.Value & "," & OUTWORD1.Value & "," & OUTWORD2.Value & "," & OUTWORD3.Value & "," & OUTWORD4.Value & "," & P_STATUS_CODE.Value & "," & OUTWORD6.Value & "," & OUTWORD7.Value & "," & OUTWORD7.Value & "," & OUTWORD8.Value & "," & OUTWORD9.Value & "," & OUTWORD10.Value & "," & OUTWORD11.Value & "," & OUTWORD12.Value & "," & OUTWORD13.Value & "," & OUTWORD14.Value & "," & OUTWORD15.Value & "," & OUTWORD16.Value
            Return ReturnString

        Catch ex As OleDbException
            Throw New Exception(ex.Message)
        Finally
            conn.Close()
        End Try
    End Function
End Class

