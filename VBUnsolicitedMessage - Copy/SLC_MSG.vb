Imports System.Net
Imports ABLink
Imports Logix
Imports System.Configuration
Imports System.Diagnostics
Imports System.IO
Imports System.Timers
Imports System.Threading
Imports System.Data.OleDb

Public Class SLC_MSG
    Public dat As OES_helper
    Public constr As String
    Dim WithEvents peerMsg As New Logix.PeerMessage()
    Dim senderIP, fromDataTable, stHIP As String
    Dim inDataArray(0 To 49) As Integer
    Dim outDataArray(0 To 49) As Integer
    Dim outBOM_DataArray(0 To 70) As Integer
    Dim P_Cell_ID, P_ITEM_ID, P_COMPONENT_ID, P_PERSON_ID, P_MODEL_ID, Result2, sBad, sResend, ProdIPLogix As String
    Dim SetUpIPLogix, LoginIPLogix, GetSerialIPLogix As String
    Dim P_PROCESS_INDICATOR, P_SUCCESS_INDICATOR, P_PROCESS_FAULT_CODE, P_TNX_ID, P_OPERATION_ID As Integer
    Dim P_VAL_1, P_VAL_2, P_VAL_3, P_VAL_4, P_VAL_5, P_VAL_6, P_VAL_7, P_VAL_8, P_VAL_9, P_VAL_10 As Integer
    Dim P_VAL_11, P_VAL_12, P_VAL_13, P_VAL_14, P_VAL_15, P_VAL_16, P_VAL_17, P_VAL_18, P_VAL_19, P_VAL_20 As Integer
    Dim P_VAL_21, P_VAL_22, P_VAL_23, P_VAL_24, P_VAL_25, P_VAL_26, P_VAL_27, P_VAL_28, P_STATUS_CODE, cN227, cN237 As Integer
    Dim cN197, cN207, cN217, cN247, z, P_ACCESS_ID_1, P_ACCESS_ID_2, P_ACCESS_ID_3, P_ACCESS_ID_4, P_ACCESS_ID_5, P_ACCESS_ID_6 As Integer
    Dim INWORD0, INWORD1, INWORD2, INWORD3, INWORD4, INWORD5, INWORD6, INWORD7, INWORD8, INWORD9, INWORD10, INWORD11 As Integer

    Public Sub MsgReceivedSLCT(ByVal arj As Object)
        'Lock object
        Monitor.Enter(arj)
        Dim argInput As Logix.MessageEventArgs = arj
        'Unlock object
        Monitor.Exit(arj)
        Try
            If Form1.cb_log = 1 Then
                Dim stMess As String = " Input MsgReceivedSLCT IP:" & argInput.IPSender & " Length:" & argInput.Length & " NAME:" & argInput.ItemName
                Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                Dim rDateH As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & "-" & Now.ToString("HH")
                Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & "_" & rDateH & "_MsgReceivedSLCT.txt"
                Dim sOutput As String = rDateF & " - " & stMess
                Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                objWriter.WriteLine(sOutput)

                If Not objWriter Is Nothing Then
                    objWriter.Close()
                    objWriter = Nothing
                End If

            End If

        Catch ex As Exception
            Dim stMess As String = " Input MsgReceivedSLCT IP:" & argInput.IPSender & " Length:" & argInput.Length & " NAME:" & argInput.ItemName
            Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.Hour.ToString.PadLeft(2, "0") & ":" & Now.Minute.ToString.PadLeft(2, "0") & ":" & Now.Second.ToString.PadLeft(2, "0")
            Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & rDateF & "_MsgReceivedSLCT.txt"
            Dim sOutput As String = rDateF & " - " & stMess & ex.Message
            Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
            objWriter.WriteLine(sOutput)
            objWriter.WriteLine(vbCrLf)
            If Not objWriter Is Nothing Then
                objWriter.Close()
                objWriter = Nothing
            End If
        End Try

        Try
            constr = ConfigurationManager.ConnectionStrings("DBConnection").ConnectionString
            ''Dim Production_Process As New Production_Process(constr)
            Dim Login_Process As New Login_Process(constr)
            Dim GetSerial_Process As New GetSerial_Process(constr)
            dat = New OES_helper(constr)
            Dim PLC As New ABLink.Controller()
            PLC.Timeout = 3000
            'SLC BLOCK
            'peerMsg.ItemName is the address that the message is sent to.  Store this value in fromDataTable to
            'determine the type of processor to send the data back to.
            Dim PMItemName As String = argInput.ItemName
            Dim PMIPSender As String = argInput.IPSender
            'REM data sent to message is populated in the peerMsg.Value.  The data from
            'peerMsg.Value is moved into dataArray.
            Dim dataArray As Array = argInput.Value
            'the peerMsg.Length is the length of the message sent from the plc.
            Dim myLength As Integer = argInput.Length
            Dim sInputT As String = Now.ToString("hh:mm:ss:fff")
            ''Dim sInput As String = ItemValuesV(dataArray, myLength)
            Dim S As String = ""

            Try
                Dim I As Integer
                If myLength = 1 Then
                    S = dataArray.ToString()
                Else
                    For I = 0 To dataArray.Length - 1
                        S = S + dataArray(I).ToString() + ","
                    Next
                End If
            Catch ex As Exception
                Dim stMess As String = "  - ITEMVALUESV Function -  "
                Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                Dim rDateH As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & "-" & Now.ToString("HH")
                Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & "_" & rDateH & "_SLC.txt"
                Dim sOutput As String = rDateF & stMess & ex.Message
                Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                objWriter.WriteLine(sOutput)
                objWriter.WriteLine(vbCrLf)
                If Not objWriter Is Nothing Then
                    objWriter.Close()
                    objWriter = Nothing
                    stMess = Nothing
                    rDateF = Nothing
                    rDateH = Nothing
                    FILE_NAME = Nothing
                    sOutput = Nothing
                End If
            End Try

            Dim sInput As String = S
            'stopwatch 
            Dim oWatch As New Stopwatch
            oWatch.Reset()
            oWatch.Start()

            Dim sIP, sIN As String
            Form1.xCount = Form1.xCount + 1

            Dim StartTime As DateTime = Now

            If PMItemName = "N197:0" Or PMItemName = "N207:0" Or PMItemName = "N217:0" Then
                '''''''''''''''''''''''''''Production SLC''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Dim ProductionObject As New Production_Process(dat.ConnectionString)
                sIP = PMIPSender & " - " & PMItemName
                sIN = "BLANK"
                Dim stTransaction As String = "PRODUCTION"
                'Dim sInput As String = ItemValues()
                'Dim tag(0 To 49) As ABLink.Tag
                Dim tag As ABLink.Tag
                Dim tagData(myLength - 1) As Int16
                'Dim x As Integer
                'Set IP address of PLC to send data
                'PLC.IPAddress = peerMsg.IPSender
                PLC.IPAddress = PMIPSender
                Dim ReturnString As String = String.Empty

                Try
                    If Form1.cb_log = 1 Then
                        Dim stMess As String = " Input from PLC on Production: "
                        Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                        Dim rDateH As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & "-" & Now.ToString("HH")
                        Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & "_" & rDateH & "_SLC.txt"
                        Dim sOutput As String = rDateF & " - " & PMIPSender & stMess & sInput
                        Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                        objWriter.WriteLine(sOutput)
                        If Not objWriter Is Nothing Then
                            objWriter.Close()
                            objWriter = Nothing
                        End If
                    End If
                Catch ex As Exception
                    Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.Hour.ToString.PadLeft(2, "0") & ":" & Now.Minute.ToString.PadLeft(2, "0") & ":" & Now.Second.ToString.PadLeft(2, "0")
                    Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & rDateF & "_SLC.txt"
                    Dim sOutput As String = rDateF & " - " & sIP & " - WITH PRODUCTION DATA ARRAY A SLC - " & ex.Message
                    Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                    objWriter.WriteLine(sOutput)
                    objWriter.WriteLine(vbCrLf)
                    If Not objWriter Is Nothing Then
                        objWriter.Close()
                        objWriter = Nothing
                    End If
                End Try

                Dim myCellID As String = ""
                Dim myItemID As String = ""
                Dim myComponentID As String = ""

                Try
                    Dim x, myVar1, myVar2 As Integer
                    For x = 0 To 4
                        If dataArray(x) <> 0 Then
                            myVar1 = Int(dataArray(x) / 256)
                            myVar2 = Int(dataArray(x) - (myVar1 * 256))
                            If myVar1 <> 0 Then myCellID = myCellID & Chr(myVar1)
                            If myVar2 <> 0 Then myCellID = myCellID & Chr(myVar2)
                        End If
                    Next
                    P_Cell_ID = myCellID
                Catch ex As Exception
                    Dim stMess As String = "  - CellIDV Function -  "
                    Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                    Dim rDateH As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & "-" & Now.ToString("HH")
                    Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & "_" & rDateH & "_SLC.txt"
                    Dim sOutput As String = rDateF & stMess & ex.Message
                    Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                    objWriter.WriteLine(sOutput)
                    objWriter.WriteLine(vbCrLf)
                    If Not objWriter Is Nothing Then
                        objWriter.Close()
                        objWriter = Nothing
                    End If
                End Try

                Try
                    Dim x, myVar1, myVar2 As Integer
                    For x = 5 To 10
                        If dataArray(x) <> 0 Then
                            myVar1 = Int(dataArray(x) / 256)
                            myVar2 = Int(dataArray(x) - (myVar1 * 256))
                            If myVar1 <> 0 Then myItemID = myItemID & Chr(myVar1)
                            If myVar2 <> 0 Then myItemID = myItemID & Chr(myVar2)
                        End If
                    Next
                    P_ITEM_ID = myItemID
                Catch ex As Exception
                    Dim stMess As String = "  - ItemIDV Function -  "
                    Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                    Dim rDateH As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & "-" & Now.ToString("HH")
                    Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & "_" & rDateH & "_SLC.txt"
                    Dim sOutput As String = rDateF & stMess & ex.Message
                    Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                    objWriter.WriteLine(sOutput)
                    objWriter.WriteLine(vbCrLf)
                    If Not objWriter Is Nothing Then
                        objWriter.Close()
                        objWriter = Nothing
                    End If
                End Try

                Try
                    Dim x, myVar1, myVar2 As Integer
                    x = 11
                    If dataArray(x) <> 0 Then
                        myVar1 = Int(dataArray(x) / 256)
                        myVar2 = Int(dataArray(x) - (myVar1 * 256))
                        If myVar1 <> 0 Then myComponentID = myComponentID & Chr(myVar1)
                        If myVar2 <> 0 Then myComponentID = myComponentID & Chr(myVar2)
                    End If
                    P_COMPONENT_ID = myComponentID
                Catch ex As Exception
                    Dim stMess As String = "  - ComponentIDV Function -  "
                    Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                    Dim rDateH As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & "-" & Now.ToString("HH")
                    Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & "_" & rDateH & "_SLC.txt"
                    Dim sOutput As String = rDateF & stMess & ex.Message
                    Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                    objWriter.WriteLine(sOutput)
                    objWriter.WriteLine(vbCrLf)
                    If Not objWriter Is Nothing Then
                        objWriter.Close()
                        objWriter = Nothing
                    End If
                End Try

                Try
                    ''P_Cell_ID = CellIDV(dataArray)
                    ''P_ITEM_ID = ItemIDV(dataArray)
                    ''P_COMPONENT_ID = ComponentIDV(dataArray)
                    P_PROCESS_INDICATOR = dataArray(18)
                    P_SUCCESS_INDICATOR = dataArray(19)
                    P_PROCESS_FAULT_CODE = dataArray(20)
                    P_STATUS_CODE = dataArray(21)
                    P_VAL_1 = dataArray(22)
                    P_VAL_2 = dataArray(23)
                    P_VAL_3 = dataArray(24)
                    P_VAL_4 = dataArray(25)
                    P_VAL_5 = dataArray(26)
                    P_VAL_6 = dataArray(27)
                    P_VAL_7 = dataArray(28)
                    P_VAL_8 = dataArray(29)
                    P_VAL_9 = dataArray(30)
                    P_VAL_10 = dataArray(31)
                    P_VAL_11 = dataArray(32)
                    P_VAL_12 = dataArray(33)
                    If myLength = 50 Then
                        P_VAL_13 = dataArray(34)
                        P_VAL_14 = dataArray(35)
                        P_VAL_15 = dataArray(36)
                        P_VAL_16 = dataArray(37)
                        P_VAL_17 = dataArray(38)
                        P_VAL_18 = dataArray(39)
                        P_VAL_19 = dataArray(40)
                        P_VAL_20 = dataArray(41)
                        P_VAL_21 = dataArray(42)
                        P_VAL_22 = dataArray(43)
                        P_VAL_23 = dataArray(44)
                        P_VAL_24 = dataArray(45)
                        P_VAL_25 = dataArray(46)
                        P_VAL_26 = dataArray(47)
                        P_VAL_27 = dataArray(48)
                        P_VAL_28 = dataArray(49)
                    End If
                    INWORD0 = dataArray(0)
                    INWORD1 = dataArray(1)
                    INWORD2 = dataArray(2)
                    INWORD3 = dataArray(3)
                    INWORD4 = dataArray(4)
                    INWORD5 = dataArray(5)
                    INWORD6 = dataArray(6)
                    INWORD7 = dataArray(7)
                    INWORD8 = dataArray(8)
                    INWORD9 = dataArray(9)
                    INWORD10 = dataArray(10)
                    INWORD11 = dataArray(11)
                Catch ex As Exception
                    Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.Hour.ToString.PadLeft(2, "0") & ":" & Now.Minute.ToString.PadLeft(2, "0") & ":" & Now.Second.ToString.PadLeft(2, "0")
                    Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & rDateF & "_SLC.txt"
                    Dim sOutput As String = rDateF & " - " & sIP & " - WITH PRODUCTION DATA ARRAY B SLC - " & ex.Message
                    Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                    objWriter.WriteLine(sOutput)
                    objWriter.WriteLine(vbCrLf)
                    If Not objWriter Is Nothing Then
                        objWriter.Close()
                        objWriter = Nothing
                    End If
                End Try

                Dim sGotData As String = oWatch.ElapsedMilliseconds.ToString
                stHIP = P_Cell_ID & "," & P_ITEM_ID & "," & P_COMPONENT_ID & "," & P_PROCESS_INDICATOR & "," & P_SUCCESS_INDICATOR & "," & P_PROCESS_FAULT_CODE

                Try
                    If Form1.cb_log = 1 Then
                        Dim stMess As String = " Input from PLC on Production Convert: "
                        Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                        Dim rDateH As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & "-" & Now.ToString("HH")
                        Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & "_" & rDateH & "_SLC.txt"
                        Dim sOutput As String = rDateF & " - " & PMIPSender & stMess & stHIP
                        Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                        objWriter.WriteLine(sOutput)
                        If Not objWriter Is Nothing Then
                            objWriter.Close()
                            objWriter = Nothing
                        End If
                    End If
                Catch ex As Exception
                    Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.Hour.ToString.PadLeft(2, "0") & ":" & Now.Minute.ToString.PadLeft(2, "0") & ":" & Now.Second.ToString.PadLeft(2, "0")
                    Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & rDateF & "_SLC.txt"
                    Dim sOutput As String = rDateF & " - " & sIP & " - PLC on Production Convert - " & ex.Message
                    Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                    objWriter.WriteLine(sOutput)
                    objWriter.WriteLine(vbCrLf)
                    If Not objWriter Is Nothing Then
                        objWriter.Close()
                        objWriter = Nothing
                    End If
                End Try

                sIN = PMIPSender & " - " & PMItemName

                'Try
                '    With ProductionObject
                '        ProductionObject.CELL_ID = P_Cell_ID
                '        ProductionObject.ITEM_ID = P_ITEM_ID
                '        ProductionObject.COMPONENT_ID = P_COMPONENT_ID
                '        ProductionObject.PROCESS_INDICATOR = P_PROCESS_INDICATOR
                '        ProductionObject.STATUS_CODE = P_STATUS_CODE
                '        ProductionObject.SUCCESS_INDICATOR = P_SUCCESS_INDICATOR
                '        ProductionObject.VAL_1 = P_VAL_1
                '        ProductionObject.VAL_2 = P_VAL_2
                '        ProductionObject.VAL_3 = P_VAL_3
                '        ProductionObject.VAL_4 = P_VAL_4
                '        ProductionObject.VAL_5 = P_VAL_5
                '        ProductionObject.VAL_6 = P_VAL_6
                '        ProductionObject.VAL_7 = P_VAL_7
                '        ProductionObject.VAL_8 = P_VAL_8
                '        ProductionObject.VAL_9 = P_VAL_9
                '        ProductionObject.VAL_10 = P_VAL_10
                '        ProductionObject.VAL_11 = P_VAL_11
                '        ProductionObject.VAL_12 = P_VAL_12
                '        If myLength = 50 Then
                '            ProductionObject.VAL_13 = P_VAL_13
                '            ProductionObject.VAL_14 = P_VAL_14
                '            ProductionObject.VAL_15 = P_VAL_15
                '            ProductionObject.VAL_16 = P_VAL_16
                '            ProductionObject.VAL_17 = P_VAL_17
                '            ProductionObject.VAL_18 = P_VAL_18
                '            ProductionObject.VAL_19 = P_VAL_19
                '            ProductionObject.VAL_20 = P_VAL_20
                '            ProductionObject.VAL_21 = P_VAL_21
                '            ProductionObject.VAL_22 = P_VAL_22
                '            ProductionObject.VAL_23 = P_VAL_23
                '            ProductionObject.VAL_24 = P_VAL_24
                '            ProductionObject.VAL_25 = P_VAL_25
                '            ProductionObject.VAL_26 = P_VAL_26
                '            ProductionObject.VAL_27 = P_VAL_27
                '            ProductionObject.VAL_28 = P_VAL_28
                '        End If
                '        ProductionObject.PROCESS_FAULT_CODE = P_PROCESS_FAULT_CODE
                '        ProductionObject.INWORD0 = INWORD0
                '        ProductionObject.INWORD1 = INWORD1
                '        ProductionObject.INWORD2 = INWORD2
                '        ProductionObject.INWORD3 = INWORD3
                '        ProductionObject.INWORD4 = INWORD4
                '        ProductionObject.INWORD5 = INWORD5
                '        ProductionObject.INWORD6 = INWORD6
                '        ProductionObject.INWORD7 = INWORD7
                '        ProductionObject.INWORD8 = INWORD8
                '        ProductionObject.INWORD9 = INWORD9
                '        ProductionObject.INWORD10 = INWORD10
                '        ProductionObject.INWORD11 = INWORD11
                '    End With
                'Catch ex As Exception
                '    Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.Hour.ToString.PadLeft(2, "0") & ":" & Now.Minute.ToString.PadLeft(2, "0") & ":" & Now.Second.ToString.PadLeft(2, "0")
                '    Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & rDateF & "_SLC.txt"
                '    Dim sOutput As String = rDateF & " - " & sIP & " - WITH PRODUCTION OBLECT SLC - " & ex.Message
                '    Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                '    objWriter.WriteLine(sOutput)
                '    objWriter.WriteLine(vbCrLf)
                '    objWriter.Close()
                '    End Try
                ''
                ''
                ''
                Try
                    Dim con_p As New OleDbConnection
                    con_p.ConnectionString = ConfigurationManager.ConnectionStrings("DBConnection").ConnectionString

                    Dim cmd As New OleDbCommand("arc.oes_prod_50", con_p)
                    cmd.CommandType = CommandType.StoredProcedure

                    Dim piCELL_ID As New OleDbParameter("CELL_ID", OleDbType.VarChar, 40)
                    piCELL_ID.Direction = ParameterDirection.Input
                    cmd.Parameters.Add(piCELL_ID).Value = P_Cell_ID

                    Dim piITEM_ID As New OleDbParameter("ITEM_ID", OleDbType.VarChar, 40)
                    piITEM_ID.Direction = ParameterDirection.Input
                    cmd.Parameters.Add(piITEM_ID).Value = P_ITEM_ID

                    Dim piCOMPONENT_ID As New OleDbParameter("COMPONENT_ID", OleDbType.VarChar, 40)
                    piCOMPONENT_ID.Direction = ParameterDirection.Input
                    cmd.Parameters.Add(piCOMPONENT_ID).Value = P_COMPONENT_ID

                    Dim piPROCESS_INDICATOR As New OleDbParameter("PROCESS_INDICATOR", OleDbType.VarNumeric)
                    piPROCESS_INDICATOR.Direction = ParameterDirection.Input
                    cmd.Parameters.Add(piPROCESS_INDICATOR).Value = P_PROCESS_INDICATOR

                    Dim piSUCCESS_INDICATOR As New OleDbParameter("SUCCESS_INDICATOR", OleDbType.VarNumeric)
                    piSUCCESS_INDICATOR.Direction = ParameterDirection.Input
                    cmd.Parameters.Add(piSUCCESS_INDICATOR).Value = P_SUCCESS_INDICATOR

                    Dim piPROCESS_FAULT_CODE As New OleDbParameter("PROCESS_FAULT_CODE", OleDbType.VarNumeric)
                    piPROCESS_FAULT_CODE.Direction = ParameterDirection.Input
                    cmd.Parameters.Add(piPROCESS_FAULT_CODE).Value = P_PROCESS_FAULT_CODE

                    Dim piVAL_1 As New OleDbParameter("VAL_1", OleDbType.VarNumeric)
                    piVAL_1.Direction = ParameterDirection.Input
                    cmd.Parameters.Add(piVAL_1).Value = P_VAL_1

                    Dim piVAL_2 As New OleDbParameter("VAL_2", OleDbType.VarNumeric)
                    piVAL_2.Direction = ParameterDirection.Input
                    cmd.Parameters.Add(piVAL_2).Value = P_VAL_2

                    Dim piVAL_3 As New OleDbParameter("VAL_3", OleDbType.VarNumeric)
                    piVAL_3.Direction = ParameterDirection.Input
                    cmd.Parameters.Add(piVAL_3).Value = P_VAL_3

                    Dim piVAL_4 As New OleDbParameter("VAL_4", OleDbType.VarNumeric)
                    piVAL_4.Direction = ParameterDirection.Input
                    cmd.Parameters.Add(piVAL_4).Value = P_VAL_4

                    Dim piVAL_5 As New OleDbParameter("VAL_5", OleDbType.VarNumeric)
                    piVAL_5.Direction = ParameterDirection.Input
                    cmd.Parameters.Add(piVAL_5).Value = P_VAL_5

                    Dim piVAL_6 As New OleDbParameter("VAL_6", OleDbType.VarNumeric)
                    piVAL_6.Direction = ParameterDirection.Input
                    cmd.Parameters.Add(piVAL_6).Value = P_VAL_6

                    Dim piVAL_7 As New OleDbParameter("VAL_7", OleDbType.VarNumeric)
                    piVAL_7.Direction = ParameterDirection.Input
                    cmd.Parameters.Add(piVAL_7).Value = P_VAL_7

                    Dim piVAL_8 As New OleDbParameter("VAL_8", OleDbType.VarNumeric)
                    piVAL_8.Direction = ParameterDirection.Input
                    cmd.Parameters.Add(piVAL_8).Value = P_VAL_8

                    Dim piVAL_9 As New OleDbParameter("VAL_9", OleDbType.VarNumeric)
                    piVAL_9.Direction = ParameterDirection.Input
                    cmd.Parameters.Add(piVAL_9).Value = P_VAL_9

                    Dim piVAL_10 As New OleDbParameter("VAL_10", OleDbType.VarNumeric)
                    piVAL_10.Direction = ParameterDirection.Input
                    cmd.Parameters.Add(piVAL_10).Value = P_VAL_10

                    Dim piVAL_11 As New OleDbParameter("VAL_11", OleDbType.VarNumeric)
                    piVAL_11.Direction = ParameterDirection.Input
                    cmd.Parameters.Add(piVAL_11).Value = P_VAL_11

                    Dim piVAL_12 As New OleDbParameter("VAL_12", OleDbType.VarNumeric)
                    piVAL_12.Direction = ParameterDirection.Input
                    cmd.Parameters.Add(piVAL_12).Value = P_VAL_12

                    Dim piVAL_13 As New OleDbParameter("VAL_13", OleDbType.VarNumeric)
                    piVAL_13.Direction = ParameterDirection.Input
                    cmd.Parameters.Add(piVAL_13).Value = P_VAL_13

                    Dim piVAL_14 As New OleDbParameter("VAL_14", OleDbType.VarNumeric)
                    piVAL_14.Direction = ParameterDirection.Input
                    cmd.Parameters.Add(piVAL_14).Value = P_VAL_14

                    Dim piVAL_15 As New OleDbParameter("VAL_15", OleDbType.VarNumeric)
                    piVAL_15.Direction = ParameterDirection.Input
                    cmd.Parameters.Add(piVAL_15).Value = P_VAL_15

                    Dim piVAL_16 As New OleDbParameter("VAL_16", OleDbType.VarNumeric)
                    piVAL_16.Direction = ParameterDirection.Input
                    cmd.Parameters.Add(piVAL_16).Value = P_VAL_16

                    Dim piVAL_17 As New OleDbParameter("VAL_17", OleDbType.VarNumeric)
                    piVAL_17.Direction = ParameterDirection.Input
                    cmd.Parameters.Add(piVAL_17).Value = P_VAL_17

                    Dim piVAL_18 As New OleDbParameter("VAL_18", OleDbType.VarNumeric)
                    piVAL_18.Direction = ParameterDirection.Input
                    cmd.Parameters.Add(piVAL_18).Value = P_VAL_18

                    Dim piVAL_19 As New OleDbParameter("VAL_19", OleDbType.VarNumeric)
                    piVAL_19.Direction = ParameterDirection.Input
                    cmd.Parameters.Add(piVAL_19).Value = P_VAL_19

                    Dim piVAL_20 As New OleDbParameter("VAL_20", OleDbType.VarNumeric)
                    piVAL_20.Direction = ParameterDirection.Input
                    cmd.Parameters.Add(piVAL_20).Value = P_VAL_20

                    Dim piVAL_21 As New OleDbParameter("VAL_21", OleDbType.VarNumeric)
                    piVAL_21.Direction = ParameterDirection.Input
                    cmd.Parameters.Add(piVAL_21).Value = P_VAL_21

                    Dim piVAL_22 As New OleDbParameter("VAL_22", OleDbType.VarNumeric)
                    piVAL_22.Direction = ParameterDirection.Input
                    cmd.Parameters.Add(piVAL_22).Value = P_VAL_22

                    Dim piVAL_23 As New OleDbParameter("VAL_23", OleDbType.VarNumeric)
                    piVAL_23.Direction = ParameterDirection.Input
                    cmd.Parameters.Add(piVAL_23).Value = P_VAL_23

                    Dim piVAL_24 As New OleDbParameter("VAL_24", OleDbType.VarNumeric)
                    piVAL_24.Direction = ParameterDirection.Input
                    cmd.Parameters.Add(piVAL_24).Value = P_VAL_24

                    Dim piVAL_25 As New OleDbParameter("VAL_25", OleDbType.VarNumeric)
                    piVAL_25.Direction = ParameterDirection.Input
                    cmd.Parameters.Add(piVAL_25).Value = P_VAL_25

                    Dim piVAL_26 As New OleDbParameter("VAL_26", OleDbType.VarNumeric)
                    piVAL_26.Direction = ParameterDirection.Input
                    cmd.Parameters.Add(piVAL_26).Value = P_VAL_26

                    Dim piVAL_27 As New OleDbParameter("VAL_27", OleDbType.VarNumeric)
                    piVAL_27.Direction = ParameterDirection.Input
                    cmd.Parameters.Add(piVAL_27).Value = P_VAL_27

                    Dim piVAL_28 As New OleDbParameter("VAL_28", OleDbType.VarNumeric)
                    piVAL_28.Direction = ParameterDirection.Input
                    cmd.Parameters.Add(piVAL_28).Value = P_VAL_28

                    Dim piINWORD0 As New OleDbParameter("INWORD0", OleDbType.VarNumeric)
                    piINWORD0.Direction = ParameterDirection.Input
                    cmd.Parameters.Add(piINWORD0).Value = INWORD0

                    Dim piINWORD1 As New OleDbParameter("INWORD1", OleDbType.VarNumeric)
                    piINWORD1.Direction = ParameterDirection.Input
                    cmd.Parameters.Add(piINWORD1).Value = INWORD1

                    Dim piINWORD2 As New OleDbParameter("INWORD2", OleDbType.VarNumeric)
                    piINWORD2.Direction = ParameterDirection.Input
                    cmd.Parameters.Add(piINWORD2).Value = INWORD2

                    Dim piINWORD3 As New OleDbParameter("INWORD3", OleDbType.VarNumeric)
                    piINWORD3.Direction = ParameterDirection.Input
                    cmd.Parameters.Add(piINWORD3).Value = INWORD3

                    Dim piINWORD4 As New OleDbParameter("INWORD4", OleDbType.VarNumeric)
                    piINWORD4.Direction = ParameterDirection.Input
                    cmd.Parameters.Add(piINWORD4).Value = INWORD4

                    Dim piINWORD5 As New OleDbParameter("INWORD5", OleDbType.VarNumeric)
                    piINWORD5.Direction = ParameterDirection.Input
                    cmd.Parameters.Add(piINWORD5).Value = INWORD5

                    Dim piINWORD6 As New OleDbParameter("INWORD6", OleDbType.VarNumeric)
                    piINWORD6.Direction = ParameterDirection.Input
                    cmd.Parameters.Add(piINWORD6).Value = INWORD6

                    Dim piINWORD7 As New OleDbParameter("INWORD7", OleDbType.VarNumeric)
                    piINWORD7.Direction = ParameterDirection.Input
                    cmd.Parameters.Add(piINWORD7).Value = INWORD7

                    Dim piINWORD8 As New OleDbParameter("INWORD8", OleDbType.VarNumeric)
                    piINWORD8.Direction = ParameterDirection.Input
                    cmd.Parameters.Add(piINWORD8).Value = INWORD8

                    Dim piINWORD9 As New OleDbParameter("INWORD9", OleDbType.VarNumeric)
                    piINWORD9.Direction = ParameterDirection.Input
                    cmd.Parameters.Add(piINWORD9).Value = INWORD9

                    Dim piINWORD10 As New OleDbParameter("INWORD10", OleDbType.VarNumeric)
                    piINWORD10.Direction = ParameterDirection.Input
                    cmd.Parameters.Add(piINWORD10).Value = INWORD10

                    Dim piINWORD11 As New OleDbParameter("INWORD11", OleDbType.VarNumeric)
                    piINWORD11.Direction = ParameterDirection.Input
                    cmd.Parameters.Add(piINWORD11).Value = INWORD11

                    ' OUTWORDS

                    Dim poOUTWORD18 As New OleDbParameter("OUTWORD18", OleDbType.VarNumeric)
                    poOUTWORD18.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(poOUTWORD18)

                    Dim poOUTWORD19 As New OleDbParameter("OUTWORD19", OleDbType.VarNumeric)
                    poOUTWORD19.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(poOUTWORD19)

                    Dim poOUTWORD20 As New OleDbParameter("OUTWORD20", OleDbType.VarNumeric)
                    poOUTWORD20.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(poOUTWORD20)

                    Dim poSTATUS_CODE As New OleDbParameter("STATUS_CODE", OleDbType.VarNumeric)
                    poSTATUS_CODE.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(poSTATUS_CODE)

                    Dim poOUTWORD22 As New OleDbParameter("OUTWORD22", OleDbType.VarNumeric)
                    poOUTWORD22.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(poOUTWORD22)

                    Dim poOUTWORD23 As New OleDbParameter("OUTWORD23", OleDbType.VarNumeric)
                    poOUTWORD23.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(poOUTWORD23)

                    Dim poOUTWORD24 As New OleDbParameter("OUTWORD24", OleDbType.VarNumeric)
                    poOUTWORD24.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(poOUTWORD24)

                    Dim poOUTWORD25 As New OleDbParameter("OUTWORD25", OleDbType.VarNumeric)
                    poOUTWORD25.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(poOUTWORD25)

                    Dim poOUTWORD26 As New OleDbParameter("OUTWORD26", OleDbType.VarNumeric)
                    poOUTWORD26.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(poOUTWORD26)

                    Dim poOUTWORD27 As New OleDbParameter("OUTWORD27", OleDbType.VarNumeric)
                    poOUTWORD27.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(poOUTWORD27)

                    Dim poOUTWORD28 As New OleDbParameter("OUTWORD28", OleDbType.VarNumeric)
                    poOUTWORD28.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(poOUTWORD28)

                    Dim poOUTWORD29 As New OleDbParameter("OUTWORD29", OleDbType.VarNumeric)
                    poOUTWORD29.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(poOUTWORD29)

                    Dim poOUTWORD30 As New OleDbParameter("OUTWORD30", OleDbType.VarNumeric)
                    poOUTWORD30.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(poOUTWORD30)

                    Dim poOUTWORD31 As New OleDbParameter("OUTWORD31", OleDbType.VarNumeric)
                    poOUTWORD31.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(poOUTWORD31)

                    Dim poOUTWORD32 As New OleDbParameter("OUTWORD32", OleDbType.VarNumeric)
                    poOUTWORD32.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(poOUTWORD32)

                    Dim poOUTWORD33 As New OleDbParameter("OUTWORD33", OleDbType.VarNumeric)
                    poOUTWORD33.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(poOUTWORD33)

                    Dim poOUTWORD34 As New OleDbParameter("OUTWORD34", OleDbType.VarNumeric)
                    poOUTWORD34.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(poOUTWORD34)

                    Dim poOUTWORD35 As New OleDbParameter("OUTWORD35", OleDbType.VarNumeric)
                    poOUTWORD35.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(poOUTWORD35)

                    Dim poOUTWORD36 As New OleDbParameter("OUTWORD36", OleDbType.VarNumeric)
                    poOUTWORD36.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(poOUTWORD36)

                    Dim poOUTWORD37 As New OleDbParameter("OUTWORD37", OleDbType.VarNumeric)
                    poOUTWORD37.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(poOUTWORD37)

                    Dim poOUTWORD38 As New OleDbParameter("OUTWORD38", OleDbType.VarNumeric)
                    poOUTWORD38.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(poOUTWORD38)

                    Dim poOUTWORD39 As New OleDbParameter("OUTWORD39", OleDbType.VarNumeric)
                    poOUTWORD39.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(poOUTWORD39)

                    Dim poOUTWORD40 As New OleDbParameter("OUTWORD40", OleDbType.VarNumeric)
                    poOUTWORD40.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(poOUTWORD40)

                    Dim poOUTWORD41 As New OleDbParameter("OUTWORD41", OleDbType.VarNumeric)
                    poOUTWORD41.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(poOUTWORD41)

                    Dim poOUTWORD42 As New OleDbParameter("OUTWORD42", OleDbType.VarNumeric)
                    poOUTWORD42.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(poOUTWORD42)

                    Dim poOUTWORD43 As New OleDbParameter("OUTWORD43", OleDbType.VarNumeric)
                    poOUTWORD43.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(poOUTWORD43)

                    Dim poOUTWORD44 As New OleDbParameter("OUTWORD44", OleDbType.VarNumeric)
                    poOUTWORD44.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(poOUTWORD44)

                    Dim poOUTWORD45 As New OleDbParameter("OUTWORD45", OleDbType.VarNumeric)
                    poOUTWORD45.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(poOUTWORD45)

                    Dim poOUTWORD46 As New OleDbParameter("OUTWORD46", OleDbType.VarNumeric)
                    poOUTWORD46.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(poOUTWORD46)

                    Dim poOUTWORD47 As New OleDbParameter("OUTWORD47", OleDbType.VarNumeric)
                    poOUTWORD47.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(poOUTWORD47)

                    Dim poOUTWORD48 As New OleDbParameter("OUTWORD48", OleDbType.VarNumeric)
                    poOUTWORD48.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(poOUTWORD48)

                    Dim poOUTWORD49 As New OleDbParameter("OUTWORD49", OleDbType.VarNumeric)
                    poOUTWORD49.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(poOUTWORD49)

                    Dim poOUTWORD0 As New OleDbParameter("OUTWORD0", OleDbType.VarNumeric)
                    poOUTWORD0.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(poOUTWORD0)

                    Dim poOUTWORD1 As New OleDbParameter("OUTWORD1", OleDbType.VarNumeric)
                    poOUTWORD1.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(poOUTWORD1)

                    Dim poOUTWORD2 As New OleDbParameter("OUTWORD2", OleDbType.VarNumeric)
                    poOUTWORD2.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(poOUTWORD2)

                    Dim poOUTWORD3 As New OleDbParameter("OUTWORD3", OleDbType.VarNumeric)
                    poOUTWORD3.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(poOUTWORD3)

                    Dim poOUTWORD4 As New OleDbParameter("OUTWORD4", OleDbType.VarNumeric)
                    poOUTWORD4.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(poOUTWORD4)

                    Dim poOUTWORD5 As New OleDbParameter("OUTWORD5", OleDbType.VarNumeric)
                    poOUTWORD5.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(poOUTWORD5)

                    Dim poOUTWORD6 As New OleDbParameter("OUTWORD6", OleDbType.VarNumeric)
                    poOUTWORD6.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(poOUTWORD6)

                    Dim poOUTWORD7 As New OleDbParameter("OUTWORD7", OleDbType.VarNumeric)
                    poOUTWORD7.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(poOUTWORD7)

                    Dim poOUTWORD8 As New OleDbParameter("OUTWORD8", OleDbType.VarNumeric)
                    poOUTWORD8.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(poOUTWORD8)

                    Dim poOUTWORD9 As New OleDbParameter("OUTWORD9", OleDbType.VarNumeric)
                    poOUTWORD9.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(poOUTWORD9)

                    Dim poOUTWORD10 As New OleDbParameter("OUTWORD10", OleDbType.VarNumeric)
                    poOUTWORD10.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(poOUTWORD10)

                    Dim poOUTWORD11 As New OleDbParameter("OUTWORD11", OleDbType.VarNumeric)
                    poOUTWORD11.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(poOUTWORD11)

                    Dim poOUTWORD13 As New OleDbParameter("OUTWORD13", OleDbType.VarNumeric)
                    poOUTWORD13.Direction = ParameterDirection.ReturnValue
                    cmd.Parameters.Add(poOUTWORD13)


                    Try
                        con_p.Open()
                        cmd.ExecuteNonQuery()
                        ReturnString = poOUTWORD0.Value.ToString & "," & poOUTWORD1.Value.ToString & "," & poOUTWORD2.Value.ToString & "," & poOUTWORD3.Value.ToString & "," & poOUTWORD4.Value.ToString & "," & poOUTWORD5.Value.ToString & "," & poOUTWORD6.Value.ToString & "," & poOUTWORD7.Value.ToString & "," & poOUTWORD8.Value.ToString & "," & poOUTWORD9.Value.ToString & "," & poOUTWORD10.Value.ToString & "," & poOUTWORD11.Value.ToString & "," & poOUTWORD13.Value.ToString & "," & poOUTWORD18.Value.ToString & "," & poOUTWORD19.Value.ToString & "," & poOUTWORD20.Value.ToString & "," & poSTATUS_CODE.Value.ToString & "," & poOUTWORD22.Value.ToString & "," & poOUTWORD23.Value.ToString & "," & poOUTWORD24.Value.ToString & "," & poOUTWORD25.Value.ToString & "," & poOUTWORD26.Value.ToString & "," & poOUTWORD27.Value.ToString & "," & poOUTWORD28.Value.ToString & "," & poOUTWORD29.Value.ToString & "," & poOUTWORD30.Value.ToString & "," & poOUTWORD31.Value.ToString & "," & poOUTWORD32.Value.ToString & "," & poOUTWORD33.Value.ToString & "," & poOUTWORD34.Value.ToString & "," & poOUTWORD35.Value.ToString & "," & poOUTWORD36.Value.ToString & "," & poOUTWORD37.Value.ToString & "," & poOUTWORD38.Value.ToString & "," & poOUTWORD39.Value.ToString & "," & poOUTWORD40.Value.ToString & "," & poOUTWORD41.Value.ToString & "," & poOUTWORD42.Value.ToString & "," & poOUTWORD43.Value.ToString & "," & poOUTWORD44.Value.ToString & "," & poOUTWORD45.Value.ToString & "," & poOUTWORD46.Value.ToString & "," & poOUTWORD47.Value.ToString & "," & poOUTWORD48.Value.ToString & "," & poOUTWORD49.Value.ToString
                    Catch ex As OleDbException
                        Throw New Exception(ex.Message)
                    Finally

                        ''
                        If con_p IsNot Nothing Then
                            con_p.Close()
                            con_p.Dispose()
                            cmd.Dispose()
                        End If
                        ''
                    End Try

                Catch ex As Exception
                End Try
                ''''''''''''''''''''

            Dim sSend_DB As String = oWatch.ElapsedMilliseconds.ToString
                ' Dim Result As String = ProductionObject.CallProductionProcess(ProductionObject)

            Dim sReturn_DB As String = oWatch.ElapsedMilliseconds.ToString

            Try
                If Form1.cb_log = 1 Then
                    Dim stMess As String = " Return from DB ProductionObject: "
                    Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                        Dim rDateH As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & "-" & Now.ToString("HH")
                    Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & "_" & rDateH & "_SLC.txt"
                        Dim sOutput As String = rDateF & " - " & PMIPSender & stMess & ReturnString
                    Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                    objWriter.WriteLine(sOutput)
                        If Not objWriter Is Nothing Then
                            objWriter.Close()
                            objWriter = Nothing
                            stMess = Nothing
                            rDateF = Nothing
                            rDateH = Nothing
                            sOutput = Nothing
                        End If
                End If

            Catch ex As Exception
                Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.Hour.ToString.PadLeft(2, "0") & ":" & Now.Minute.ToString.PadLeft(2, "0") & ":" & Now.Second.ToString.PadLeft(2, "0")
                Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & rDateF & "_SLC.txt"
                Dim sOutput As String = rDateF & " - " & sIP & " - Return from DB ProductionObject - " & ex.Message
                Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                objWriter.WriteLine(sOutput)
                objWriter.WriteLine(vbCrLf)
                    If Not objWriter Is Nothing Then
                        objWriter.Close()
                        objWriter = Nothing
                        rDateF = Nothing
                        FILE_NAME = Nothing
                        sOutput = Nothing
                    End If
            End Try



            If PMItemName = "N197:0" Then
                tag = New ABLink.Tag("N198:0")

                If myLength = 50 Then
                    tag.Length = 50
                Else
                    tag.Length = 34
                End If
            End If

            If PMItemName = "N207:0" Then
                tag = New ABLink.Tag("N208:0")

                If myLength = 50 Then
                    tag.Length = 50
                Else
                    tag.Length = 34
                End If

            End If

            If PMItemName = "N217:0" Then
                tag = New ABLink.Tag("N218:0")

                If myLength = 50 Then
                    tag.Length = 50
                Else
                    tag.Length = 34
                End If

            End If

            Try

                    Dim arrResult() As String = ReturnString.Split(",")

                'cell_id
                tagData(0) = Convert.ToInt16(arrResult.GetValue(0))
                tagData(1) = Convert.ToInt16(arrResult.GetValue(1))
                tagData(2) = Convert.ToInt16(arrResult.GetValue(2))
                tagData(3) = Convert.ToInt16(arrResult.GetValue(3))
                tagData(4) = Convert.ToInt16(arrResult.GetValue(4))
                'item_id
                tagData(5) = Convert.ToInt16(arrResult.GetValue(5))
                tagData(6) = Convert.ToInt16(arrResult.GetValue(6))
                tagData(7) = Convert.ToInt16(arrResult.GetValue(7))
                tagData(8) = Convert.ToInt16(arrResult.GetValue(8))
                tagData(9) = Convert.ToInt16(arrResult.GetValue(9))
                tagData(10) = Convert.ToInt16(arrResult.GetValue(10))
                'Generant Barcode
                tagData(11) = Convert.ToInt16(arrResult.GetValue(11))
                '12 is a counter word not returned
                tagData(12) = 0
                'Part Retry Status
                tagData(13) = Convert.ToInt16(arrResult.GetValue(12))
                tagData(14) = 0
                tagData(15) = 0
                tagData(16) = 0
                tagData(17) = 0
                'Transaction Request
                tagData(18) = Convert.ToInt16(arrResult.GetValue(13))
                'PASS \FAIL
                tagData(19) = Convert.ToInt16(arrResult.GetValue(14))
                'Process Failure Code
                tagData(20) = Convert.ToInt16(arrResult.GetValue(15))
                '21 is returned VERY LAST
                tagData(21) = Convert.ToInt16(arrResult.GetValue(16))
                tagData(22) = Convert.ToInt16(arrResult.GetValue(17))
                tagData(23) = Convert.ToInt16(arrResult.GetValue(18))
                tagData(24) = Convert.ToInt16(arrResult.GetValue(19))
                tagData(25) = Convert.ToInt16(arrResult.GetValue(20))
                tagData(26) = Convert.ToInt16(arrResult.GetValue(21))
                tagData(27) = Convert.ToInt16(arrResult.GetValue(22))
                tagData(28) = Convert.ToInt16(arrResult.GetValue(23))
                tagData(29) = Convert.ToInt16(arrResult.GetValue(24))
                tagData(30) = Convert.ToInt16(arrResult.GetValue(25))
                tagData(31) = Convert.ToInt16(arrResult.GetValue(26))
                tagData(32) = Convert.ToInt16(arrResult.GetValue(27))
                tagData(33) = Convert.ToInt16(arrResult.GetValue(28))

                If myLength = 50 Then
                    tagData(34) = Convert.ToInt16(arrResult.GetValue(29))
                    tagData(35) = Convert.ToInt16(arrResult.GetValue(30))
                    tagData(36) = Convert.ToInt16(arrResult.GetValue(31))
                    tagData(37) = Convert.ToInt16(arrResult.GetValue(32))
                    tagData(38) = Convert.ToInt16(arrResult.GetValue(33))
                    tagData(39) = Convert.ToInt16(arrResult.GetValue(34))
                    tagData(40) = Convert.ToInt16(arrResult.GetValue(35))
                    tagData(41) = Convert.ToInt16(arrResult.GetValue(36))
                    tagData(42) = Convert.ToInt16(arrResult.GetValue(37))
                    tagData(43) = Convert.ToInt16(arrResult.GetValue(38))
                    tagData(44) = Convert.ToInt16(arrResult.GetValue(39))
                    tagData(45) = Convert.ToInt16(arrResult.GetValue(40))
                    tagData(46) = Convert.ToInt16(arrResult.GetValue(41))
                    tagData(47) = Convert.ToInt16(arrResult.GetValue(42))
                    tagData(48) = Convert.ToInt16(arrResult.GetValue(43))
                    tagData(49) = Convert.ToInt16(arrResult.GetValue(44))
                End If

                Catch ex As Exception
                    Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.Hour.ToString.PadLeft(2, "0") & ":" & Now.Minute.ToString.PadLeft(2, "0") & ":" & Now.Second.ToString.PadLeft(2, "0")
                    Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & rDateF & "_SLC.txt"
                    Dim sOutput As String = rDateF & " - " & sIP & " - WITH PRODUCTION WRITE TO TAG ARRAY SLC - " & ex.Message
                    Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                    objWriter.WriteLine(sOutput)
                    objWriter.WriteLine(vbCrLf)
                    If Not objWriter Is Nothing Then
                        objWriter.Close()
                        objWriter = Nothing
                        rDateF = Nothing
                        FILE_NAME = Nothing
                        sOutput = Nothing
                    End If
                End Try


            sBad = ""
            sResend = ""
            tag.Value = tagData

            Try
                PLC.WriteTag(tag)
                ''Console.WriteLine(Now.ToString("hh:mm:ss:fff") & " PROD SLC: " & PLC.ErrorString)
                    Dim z As Integer = 0
                    ' Loop.
                    Do While tag.QualityCode <> 192 And z < 5
                        tag.Value = tagData
                        Thread.Sleep(50)
                        PLC.WriteTag(tag)
                        z = z + 1
                    Loop

                    'Console.WriteLine(Now.ToString("hh:mm:ss:fff") & " Cycle Count " & z & " " & PLC.ErrorString)

                    ''''''''''''''Polling error code
                'If tag.QualityCode = 192 Then

                '    If PMItemName = "N197:0" Then
                '        Dim newTag = New ABLink.Tag(PLC, "N198:21")
                '        PLC.ReadTag(newTag)
                '        Console.WriteLine(vbCrLf & Now.ToString("hh:mm:ss:fff") & " Polling data: " & newTag.Value & " - " & sIP & vbCrLf)
                '    End If

                '    If PMItemName = "N207:0" Then
                '        Dim newTag = New ABLink.Tag(PLC, "N208:21")
                '        PLC.ReadTag(newTag)
                '        Console.WriteLine(vbCrLf & Now.ToString("hh:mm:ss:fff") & " Polling data: " & newTag.Value & " - " & sIP & vbCrLf)
                '    End If

                '    If PMItemName = "N217:0" Then
                '        Dim newTag = New ABLink.Tag(PLC, "N218:21")
                '        PLC.ReadTag(newTag)
                '        Console.WriteLine(vbCrLf & Now.ToString("hh:mm:ss:fff") & " Polling data: " & newTag.Value & " - " & sIP & vbCrLf)
                '    End If

                'End If

            Catch ex As Exception
                Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.Hour.ToString.PadLeft(2, "0") & ":" & Now.Minute.ToString.PadLeft(2, "0") & ":" & Now.Second.ToString.PadLeft(2, "0")
                Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & "_" & rDateF & ".txt"
                Dim sOutput As String = rDateF & " - WRITE TAG PRODUCTION SLC - " & ex.Message
                Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                objWriter.WriteLine(sOutput)
                objWriter.WriteLine(vbCrLf)
                    If Not objWriter Is Nothing Then
                        objWriter.Close()
                        objWriter = Nothing
                        rDateF = Nothing
                        FILE_NAME = Nothing
                        sOutput = Nothing
                    End If
            End Try


            sBad = " PLC Version: " & PLC.Version & " PLC CPUType: " & PLC.CPUType & " PLC DriverType: " & PLC.DriverType & " PLC Timeout:" & PLC.Timeout & " PLC ErrorCode:" & PLC.ErrorCode & " PLC IsConnected:" & PLC.IsConnected & " Tag 0 QualityCode: " & tag.QualityCode
            Dim sReturntoPLC As String = oWatch.ElapsedMilliseconds.ToString

            If Form1.cb_log = 1 Then
                Try
                    Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                        Dim sOutput As String = rDateF & " - " & sIP & vbCrLf & " Input from PLC: " & sInput & vbCrLf & " Result from DB: " & ReturnString & vbCrLf & "    " & stHIP & vbCrLf & "    Got Data: " & (sGotData / 1000) & " Send to DB: " & (sSend_DB / 1000) & " Return from DB: " & (sReturn_DB / 1000) & " Last Word: " & (sReturntoPLC / 1000)
                    sInput = sInput & vbCrLf
                    Dim ScreenOutput As String = rDateF & " - " & sIP & " - " & P_Cell_ID & " Got Data: " & (sGotData / 1000) & " Send to DB: " & (sSend_DB / 1000) & " Return from DB: " & (sReturn_DB / 1000) & " Last Word: " & (sReturntoPLC / 1000) & " QualityCode: " & tag.QualityCode
                    Dim stMess As String = " End of Production "
                        Dim rDateH As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & "-" & Now.ToString("HH")
                    Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & "_" & rDateH & "_SLC.txt"
                    Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                    objWriter.WriteLine(ScreenOutput & " - " & stMess)
                        If Not objWriter Is Nothing Then
                            objWriter.Close()
                            objWriter = Nothing
                            rDateF = Nothing
                            FILE_NAME = Nothing
                            sOutput = Nothing
                            ScreenOutput = Nothing
                        End If

                    Dim LogTimeOutput As String = rDateF & "," & sIP & ",Got Data," & (sGotData / 1000) & ",Send to DB:," & (sSend_DB / 1000) & ",Return from DB," & (sReturn_DB / 1000) & ",Last Word," & (sReturntoPLC / 1000) & ",QualityCode," & tag.QualityCode
                    Form1.LogTimingArray.Add(LogTimeOutput)
                        rDateF = Nothing
                        sOutput = Nothing
                        sInput = Nothing
                        ScreenOutput = Nothing
                        stMess = Nothing
                        rDateH = Nothing
                        FILE_NAME = Nothing
                Catch ex As Exception
                    Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                    Thread.Sleep(50)
                    Dim ScreenOutput As String = rDateF & " - " & sIP & " - " & P_Cell_ID & " Got Data: " & (sGotData / 1000) & " Send to DB: " & (sSend_DB / 1000) & " Return from DB: " & (sReturn_DB / 1000) & " Last Word: " & (sReturntoPLC / 1000) & " QualityCode: " & tag.QualityCode
                        Form1.ScreenList.Add(ScreenOutput)
                        rDateF = Nothing
                        ScreenOutput = Nothing
                End Try
            End If

                If Form1.cb_log2 = 1 Then
                    Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                    Dim ScreenOutput As String = rDateF & " - " & sIP & " - " & P_Cell_ID & " Got Data: " & (sGotData / 1000) & " Send to DB: " & (sSend_DB / 1000) & " Return from DB: " & (sReturn_DB / 1000) & " Last Word: " & (sReturntoPLC / 1000) & " QualityCode: " & tag.QualityCode
                    Form1.ScreenList.Add(ScreenOutput)
                    rDateF = Nothing
                    ScreenOutput = Nothing
                End If
            '''''''''''''''
            ''insert into tx
                Try
                    Dim dat_TX As OES_helper
                    dat_TX = New OES_helper(constr)
                    Dim stB As String = Form1.stBuild.ToString & " - " & PMIPSender & " - " & System.Net.Dns.GetHostName()
                    Dim sOutputtx As String = P_Cell_ID & ",Got Data," & (sGotData / 1000) & ",Send to DB," & (sSend_DB / 1000) & ",Return from DB," & (sReturn_DB / 1000) & ",Last Word," & (sReturntoPLC / 1000) & ",QualityCode," & tag.QualityCode
                    Dim t As New Thread(DirectCast(Sub() dat_TX.InsertIntoTX_LOG(stTransaction, "OES", stB, "OES", sOutputtx, "OES", PMItemName), ThreadStart))
                    t.Start()
                Catch ex As Exception
                    Dim stMess As String = " End of Production "
                    Dim rDateH As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & "-" & Now.ToString("HH")
                    Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & "_" & rDateH & "_SLC.txt"
                    Dim rDateF2 As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                    Dim sOutput2 As String = rDateF2 & " - WRITE TO TX LOG - " & stTransaction & " - " & ex.Message
                    Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                    objWriter.WriteLine(sOutput2)
                    objWriter.WriteLine(vbCrLf)
                    If Not objWriter Is Nothing Then
                        objWriter.Close()
                        objWriter = Nothing
                        stMess = Nothing
                        rDateF2 = Nothing
                        rDateH = Nothing
                        FILE_NAME = Nothing
                        sOutput2 = Nothing
                    End If
                End Try
                ''''''''''''''''''''''
                PLC.Disconnect()

                ''Console.WriteLine("Result SLC: " & Now.ToString("hh:mm:ss:fff") & " " & Result)
                ''Console.WriteLine("PLC ERRROR String: " & PLC.ErrorCode & " Tag 0 QualityCode: " & tag.QualityCode)
            End If

        If PMItemName = "N227:0" Then
            ' Login
            Dim LoginObject As New Login_Process(dat.ConnectionString)
            Dim stTransaction As String = "LOGIN"
            'Dim tagGroup As New ABLink.TagGroup
            Dim tag(0 To 33) As ABLink.Tag
            Dim tagData(33) As Int16
            Dim x As Integer
            'Set IP address of PLC to send data
            PLC.IPAddress = PMIPSender
            sBad = ""
            sResend = ""

            sIP = PMIPSender & " - " & PMItemName
            sIN = "BLANK"
            'Dim sInput As String = ItemValuesV(dataArray, myLength)

            Dim sGotData As String = oWatch.ElapsedMilliseconds.ToString

            P_Cell_ID = CellIDV(dataArray)
            P_PERSON_ID = PersonIDV(dataArray)
            P_TNX_ID = dataArray(18)

            Try
                With LoginObject
                    LoginObject.CELL_ID = P_Cell_ID
                    LoginObject.EMPLOYEE_ID = P_PERSON_ID
                    LoginObject.TNX_ID = P_TNX_ID
                End With
            Catch ex As Exception
                Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & "_" & rDateF & "_SLC.txt"
                Dim sOutput As String = rDateF & " - WITH LOGIN OBJECT SLC - " & ex.Message
                Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                objWriter.WriteLine(sOutput)
                objWriter.WriteLine(vbCrLf)
                    If Not objWriter Is Nothing Then
                        objWriter.Close()
                        objWriter = Nothing
                    End If
            End Try

            Dim sSend_DB As String = oWatch.ElapsedMilliseconds.ToString
            Dim Result As String = LoginObject.CallLoginProcess(LoginObject)
            Dim sReturn_DB As String = oWatch.ElapsedMilliseconds.ToString


            If PMItemName = "N227:0" Then
                For x = 0 To 33
                    tag(x) = New ABLink.Tag("N228:" & x)
                Next
            End If

            Dim arrResult() As String
            arrResult = Result.Split(",")
            tag(21).Value = arrResult.GetValue(0)
            tag(20).Value = arrResult.GetValue(1)


                PLC.WriteTag(tag(20))
                PLC.WriteTag(tag(21))

                Dim z As Integer = 0
                ' Loop.
                Do While tag(20).QualityCode <> 192 And z < 5
                    tag(20).Value = arrResult.GetValue(0)
                    tag(21).Value = arrResult.GetValue(1)
                    Thread.Sleep(50)
                    PLC.WriteTag(tag(20))
                    PLC.WriteTag(tag(21))
                    z = z + 1
                Loop

            sBad = " Tag 20 QualityCode: " & tag(20).QualityCode

                'If tag(20).QualityCode <> 192 Then
                '    tag(20).Value = arrResult.GetValue(0)
                '    tag(21).Value = arrResult.GetValue(1)
                '    PLC.WriteTag(tag(20))
                '    PLC.WriteTag(tag(21))
                '    sResend = "Resend " & " Tag 20 QualityCode: " & tag(20).QualityCode
                'End If

                Dim sReturntoPLC As String = oWatch.ElapsedMilliseconds.ToString


                sIN = PMIPSender & " - " & PMItemName
            sIP = PMIPSender & " - " & PMItemName

            Form1.lbl_TotalTrans.Text = "Total Transactions: " & Form1.xCount
            If Form1.cb_log = 1 Then
                Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & "_SLC.txt"
                Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                Dim sOutput As String = rDateF & " - " & sIP & vbCrLf & " Input from PLC: " & sInput & vbCrLf & " DB return " & Result & vbCrLf & "    Login " & P_Cell_ID & " " & P_PERSON_ID & "  " & P_TNX_ID & vbCrLf & "    Got Data: " & (sGotData / 1000) & " Send to DB: " & (sSend_DB / 1000) & " Return from DB: " & (sReturn_DB / 1000) & " Return to PLC: " & (sReturntoPLC / 1000)

                Dim ScreenOutput As String = rDateF & " - " & sIP & " - " & P_Cell_ID & " Got Data: " & (sGotData / 1000) & " Send to DB: " & (sSend_DB / 1000) & " Return from DB: " & (sReturn_DB / 1000) & " Last Word: " & (sReturntoPLC / 1000) & " QualityCode: " & tag(20).QualityCode
                Form1.ScreenList.Add(ScreenOutput)
                Dim LogTimeOutput As String = rDateF & "," & sIP & ",Got Data," & (sGotData / 1000) & ",Send to DB:," & (sSend_DB / 1000) & ",Return from DB," & (sReturn_DB / 1000) & ",Last Word," & (sReturntoPLC / 1000) & ",QualityCode," & tag(20).QualityCode
                Form1.LogTimingArray.Add(LogTimeOutput)
                End If

                If Form1.cb_log2 = 1 Then
                    Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                    Dim ScreenOutput As String = rDateF & " - " & sIP & " - " & P_Cell_ID & " Got Data: " & (sGotData / 1000) & " Send to DB: " & (sSend_DB / 1000) & " Return from DB: " & (sReturn_DB / 1000) & " Last Word: " & (sReturntoPLC / 1000) & " QualityCode: " & tag(20).QualityCode
                    Form1.ScreenList.Add(ScreenOutput)
                End If

            '''''''''''''''
            ''insert into tx
                Try
                    Dim dat_TX As OES_helper
                    dat_TX = New OES_helper(constr)
                    Dim stB As String = Form1.stBuild.ToString & " - " & PMIPSender & " - " & System.Net.Dns.GetHostName()
                    Dim sOutputtx As String = P_Cell_ID & ",Got Data," & (sGotData / 1000) & ",Send to DB," & (sSend_DB / 1000) & ",Return from DB," & (sReturn_DB / 1000) & ",Last Word," & (sReturntoPLC / 1000) & ",QualityCode," & tag(20).QualityCode
                    Dim t As New Thread(DirectCast(Sub() dat_TX.InsertIntoTX_LOG(stTransaction, "OES", stB, "OES", sOutputtx, "OES", PMItemName), ThreadStart))
                    t.Start()
                Catch ex As Exception

                    Dim stMess As String = " End of Production "
                    Dim rDateH As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & "-" & Now.ToString("HH")
                    Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & "_" & rDateH & "_SLC.txt"
                    Dim rDateF2 As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                    Dim sOutput2 As String = rDateF2 & " - WRITE TO TX LOG - " & stTransaction & " - " & ex.Message
                    Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                    objWriter.WriteLine(sOutput2)
                    objWriter.WriteLine(vbCrLf)
                    objWriter.Close()
                End Try
            ''''''''''''''''''''''


            PLC.Disconnect()
            ''Console.WriteLine("Result SLC: " & Now.ToString("hh:mm:ss:fff") & " " & Result)
            ''Console.WriteLine("PLC ERRROR String: " & PLC.ErrorCode & " Tag 20 QualityCode: " & tag(20).QualityCode)

        End If

        ' GET SERIAL
        If PMItemName = "N247:20" Then
            'GetSerialIP = peerMsg.IPSender
            Dim stTransaction As String = "GET_SERIAL"
            Dim oWatch2 As New Stopwatch
            oWatch2.Start()
            Dim GetSerialObject As New GetSerial_Process(dat.ConnectionString)

            Dim tag As ABLink.Tag
            'Dim x As Integer
            Dim Result, sReturn_DB, sReturntoPLC, sErr As String
            Dim arrResult() As String
            'Set IP address of PLC to send data
            PLC.IPAddress = PMIPSender
            'Convert 
            P_Cell_ID = CellIDV(dataArray)

            sIP = PMIPSender & " - " & PMItemName
            sIN = "BLANK"
            'Dim sInput As String = ItemValuesV(dataArray, myLength)

            Dim sGotData As String = oWatch2.ElapsedMilliseconds.ToString

            Try
                With GetSerialObject
                    GetSerialObject.CELL_ID = P_Cell_ID
                End With
            Catch ex As Exception
                Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & rDateF & "_SLC.txt"

                Dim sOutput As String = rDateF & " - WITH GET SERIAL OBJECT SLC - " & ex.Message
                Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                objWriter.WriteLine(sOutput)
                objWriter.WriteLine(vbCrLf)
                    If Not objWriter Is Nothing Then
                        objWriter.Close()
                        objWriter = Nothing
                    End If
            End Try

            Dim sSend_DB As String = oWatch2.ElapsedMilliseconds.ToString

            tag = New ABLink.Tag("N247:0", 11)

            Try
                Result = GetSerialObject.CallGetSerialProcess(GetSerialObject)
            Catch ex As Exception
                sErr = ex.Message
                Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & rDateF & "_SLC.txt"
                Dim sOutput As String = rDateF & "  VB Error Get Serial " & sErr & vbCrLf
                Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                objWriter.WriteLine(sOutput)
                objWriter.WriteLine(vbCrLf)
                    If Not objWriter Is Nothing Then
                        objWriter.Close()
                        objWriter = Nothing
                    End If
            End Try

            sReturn_DB = oWatch2.ElapsedMilliseconds.ToString
            arrResult = Result.Split(",")

            Dim PlcData(10) As System.Int16

            PlcData(0) = Convert.ToInt16(arrResult.GetValue(0))
            PlcData(1) = Convert.ToInt16(arrResult.GetValue(1))
            PlcData(2) = Convert.ToInt16(arrResult.GetValue(2))
            PlcData(3) = Convert.ToInt16(arrResult.GetValue(3))
            PlcData(4) = Convert.ToInt16(arrResult.GetValue(4))
            PlcData(5) = Convert.ToInt16(arrResult.GetValue(5))
            PlcData(6) = Convert.ToInt16(arrResult.GetValue(6))
            PlcData(7) = Convert.ToInt16(arrResult.GetValue(7))
            PlcData(8) = Convert.ToInt16(arrResult.GetValue(8))
            PlcData(9) = Convert.ToInt16(arrResult.GetValue(9))
            PlcData(10) = Convert.ToInt16(arrResult.GetValue(10))

            tag.Value = PlcData
            sResend = ""

            Try
                PLC.WriteTag(tag)
                ''Console.WriteLine(Now.ToString("hh:mm:ss:fff") & " PROD: " & PLC.ErrorString)


                    Dim z As Integer = 0
                    ' Loop.
                    Do While tag.QualityCode <> 192 And z < 5
                        tag.Value = PlcData
                        Thread.Sleep(50)
                        PLC.WriteTag(tag)
                        z = z + 1
                    Loop

                    'If tag.QualityCode <> 192 Then
                    '    tag.Value = PlcData
                    '    Dim i As Integer
                    '    For i = 1 To 4000
                    '    Next
                    '    PLC.WriteTag(tag)
                    '    ''Console.WriteLine(Now.ToString("hh:mm:ss:fff") & " PROD REWRITE 1: " & PLC.ErrorString)
                    'End If

                'If tag.QualityCode <> 192 Then
                '    Console.WriteLine(Now.ToString("hh:mm:ss:fff") & " **********ERROR**************")
                'End If

            Catch ex As Exception
                sErr = ex.Message
                Dim rDate As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.Hour.ToString.PadLeft(2, "0") & ":" & Now.Minute.ToString.PadLeft(2, "0") & ":" & Now.Second.ToString.PadLeft(2, "0")
                Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & rDate & "_SLC.txt"
                Dim sOutput As String = rDate & " VB Error Get Serial " & sErr
                Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                objWriter.WriteLine(sOutput)
                objWriter.WriteLine(vbCrLf)
                    If Not objWriter Is Nothing Then
                        objWriter.Close()
                        objWriter = Nothing
                    End If
            End Try

            sBad = " Error string = " & PLC.ErrorString & " PLC Version: " & PLC.Version & " PLC CPUType: " & PLC.CPUType & " PLC DriverType: " & PLC.DriverType & " PLC Timeout: " & PLC.Timeout & " Tag 0 QualityCode: " & tag.QualityCode
            sReturntoPLC = oWatch2.ElapsedMilliseconds.ToString

                sIN = PMIPSender & " - " & PMItemName
            sIP = PMIPSender & " - " & PMItemName

            Form1.lbl_TotalTrans.Text = "Total Transactions: " & Form1.xCount
            If Form1.cb_log = 1 Then
                Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & "_SLC.txt"
                Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                Dim sOutput As String = rDateF & " - " & sIP & vbCrLf & " Input from PLC: " & sInput & vbCrLf & " DB return " & Result & vbCrLf & "   Get Serial " & P_Cell_ID & vbCrLf & "   Got Data: " & (sGotData \ 1000) & " Send to DB: " & (sSend_DB / 1000) & " Return from DB: " & (sReturn_DB / 1000) & " Return to PLC: " & (sReturntoPLC / 1000)
                    Dim LogTimeOutput As String = rDateF & "," & sIP & ",Got Data," & (sGotData / 1000) & ",Send to DB:," & (sSend_DB / 1000) & ",Return from DB," & (sReturn_DB / 1000) & ",Last Word," & (sReturntoPLC / 1000) & ",QualityCode," & tag.QualityCode
                Form1.LogTimingArray.Add(LogTimeOutput)
            End If

                If Form1.cb_log2 = 1 Then
                    Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                    Dim ScreenOutput As String = rDateF & " - " & sIP & " - " & P_Cell_ID & " Got Data: " & (sGotData / 1000) & " Send to DB: " & (sSend_DB / 1000) & " Return from DB: " & (sReturn_DB / 1000) & " Last Word: " & (sReturntoPLC / 1000) & " QualityCode: " & tag.QualityCode
                    Form1.ScreenList.Add(ScreenOutput)
                End If

                '''''''''''''''
            ''insert into tx
                Try
                    Dim dat_TX As OES_helper
                    dat_TX = New OES_helper(constr)
                    Dim stB As String = Form1.stBuild.ToString & " - " & PMIPSender & " - " & System.Net.Dns.GetHostName()
                    Dim sOutputtx As String = P_Cell_ID & ",Got Data," & (sGotData / 1000) & ",Send to DB," & (sSend_DB / 1000) & ",Return from DB," & (sReturn_DB / 1000) & ",Last Word," & (sReturntoPLC / 1000) & ",QualityCode," & tag.QualityCode
                    Dim t As New Thread(DirectCast(Sub() dat_TX.InsertIntoTX_LOG(stTransaction, "OES", stB, "OES", sOutputtx, "OES", PMItemName), ThreadStart))
                    t.Start()
                Catch ex As Exception

                    Dim stMess As String = " End of Production "
                    Dim rDateH As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & "-" & Now.ToString("HH")
                    Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & "_" & rDateH & "_SLC.txt"
                    Dim rDateF2 As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                    Dim sOutput2 As String = rDateF2 & " - WRITE TO TX LOG - " & stTransaction & " - " & ex.Message
                    Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                    objWriter.WriteLine(sOutput2)
                    objWriter.WriteLine(vbCrLf)
                    objWriter.Close()
                End Try
            ''''''''''''''''''''''

            oWatch2.Stop()
            PLC.Disconnect()
            ''Console.WriteLine("Result SLC: " & Now.ToString("hh:mm:ss:fff") & " " & Result)
            ''Console.WriteLine("PLC ERRROR String: " & PLC.ErrorCode & " Tag 0 QualityCode: " & tag.QualityCode)
        End If

        If PMItemName = "N237:0" Then
            ' Setup
            Dim SetupObject As New Setup_Process(dat.ConnectionString)
            Dim PLCModelObject As New PLC_PushDown_Process(dat.ConnectionString)

            Dim tag As ABLink.Tag
            Dim Tag2 As ABLink.Tag
            Dim tagData(69) As Int16
            Dim tagData2(33) As Int16

            Dim stTransaction As String = "SETUP"

            tag = New ABLink.Tag("N238:0")
            Tag2 = New ABLink.Tag("N241:0")
            tag.Name = "N238:0"
            tag.Length = 70
            Tag2.Name = "N241:0"
            Tag2.Length = 34

            'Dim x, x1, x2 As Integer
            'Set IP address of PLC to send data
            PLC.IPAddress = PMIPSender

            'Dim sInput As String = ItemValuesV(dataArray, myLength)
            'DEBUG SETUP
            'If Form1.cb_log = 1 Then
            '    Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & "_SLC.txt"
            '    Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
            '    Dim sOutput As String = rDateF & " - " & PMIPSender & " Input from PLC on SETUP: " & sInput
            '    Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
            '    objWriter.WriteLine(sOutput)
            '    objWriter.Close()
            'End If
            ''Set file name with the hour

            If Form1.cb_log = 1 Then
                Dim stMess As String = " Input from PLC on Setup: "
                Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                    Dim rDateH As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & "-" & Now.ToString("HH")
                Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & "_" & rDateH & "_SLC.txt"
                Dim sOutput As String = rDateF & " - " & PMIPSender & stMess & sInput
                Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                objWriter.WriteLine(sOutput)
                    If Not objWriter Is Nothing Then
                        objWriter.Close()
                        objWriter = Nothing
                    End If
            End If

            P_Cell_ID = CellIDV(dataArray)
            P_MODEL_ID = ModelIDV(dataArray)
            P_TNX_ID = dataArray(18)
            P_OPERATION_ID = OperationIDV(dataArray)
            P_COMPONENT_ID = BOM_CompIDV(dataArray)

            If Form1.cb_log = 1 Then
                Dim stMess As String = " Input from PLC on Setup: "
                Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                    Dim rDateH As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & "-" & Now.ToString("HH")
                Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & "_" & rDateH & "_SLC.txt"
                Dim sOutput As String = rDateF & " - " & PMIPSender & stMess & P_Cell_ID & "," & P_MODEL_ID & "," & P_TNX_ID & "," & P_OPERATION_ID & "," & P_COMPONENT_ID
                Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                objWriter.WriteLine(sOutput)
                    If Not objWriter Is Nothing Then
                        objWriter.Close()
                        objWriter = Nothing
                    End If
            End If

            Dim sGotData As String = oWatch.ElapsedMilliseconds.ToString

            Try
                With SetupObject
                    SetupObject.CELL_ID = P_Cell_ID
                    SetupObject.MODEL_ID = P_MODEL_ID
                    SetupObject.OPERATION = P_OPERATION_ID
                    SetupObject.TRANSACTION_ID = P_TNX_ID
                    SetupObject.ACCESS_ID = dataArray(16)
                    SetupObject.ITEM_ID = P_COMPONENT_ID
                End With
            Catch ex As Exception
                'Throw New Exception(ex.Message & " SETUP OBJECT")
                Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & rDateF & "_SLC.txt"

                Dim sOutput As String = rDateF & " - SETUP OBJECT - " & ex.Message
                Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                objWriter.WriteLine(sOutput)
                objWriter.WriteLine(vbCrLf)
                    If Not objWriter Is Nothing Then
                        objWriter.Close()
                        objWriter = Nothing
                    End If
            End Try

            Dim cPlcModelSetupCount As Integer = dat.GetPlcModelSetupCount(P_Cell_ID, P_MODEL_ID)

            If (P_TNX_ID = 4 And cPlcModelSetupCount = 1) Then
                'If (P_TNX_ID = 4) Then
                Try
                    With PLCModelObject
                        PLCModelObject.CELL_ID = P_Cell_ID
                        PLCModelObject.Model_ID = P_MODEL_ID
                    End With
                Catch ex As Exception
                    'Throw New Exception(ex.Message & " PLC MODEL SETUP OBJECT")
                    Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                    Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & rDateF & "_SLC.txt"
                    Dim sOutput As String = rDateF & " - PLC MODEL SETUP OBJECT - " & ex.Message
                    Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                    objWriter.WriteLine(sOutput)
                    objWriter.WriteLine(vbCrLf)
                        If Not objWriter Is Nothing Then
                            objWriter.Close()
                            objWriter = Nothing
                        End If
                End Try

                'DEBUG SETUP
                'If Form1.cb_log = 1 Then
                '    Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & "_SLC.txt"
                '    Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                '    Dim sOutput As String = rDateF & " - " & PMIPSender & " PLC MODEL SETUP to DB"
                '    Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                '    objWriter.WriteLine(sOutput)
                '    objWriter.Close()


                'End If
            End If

            Dim sSend_DB As String = oWatch.ElapsedMilliseconds.ToString
            Dim Result As String = SetupObject.CallSetupProcess(SetupObject)

            'DEBUG SETUP
            If Form1.cb_log = 1 Then
                Dim stMess As String = " Return from DB on Setup: "
                Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                    Dim rDateH As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & "-" & Now.ToString("HH")
                Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & "_" & rDateH & "_SLC.txt"
                Dim sOutput As String = rDateF & " - " & PMIPSender & stMess & Result
                Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                objWriter.WriteLine(sOutput)
                    If Not objWriter Is Nothing Then
                        objWriter.Close()
                        objWriter = Nothing
                    End If
            End If

            sIP = PMIPSender & " - " & PMItemName
            sIN = "BLANK"

            If (P_TNX_ID = 4 And cPlcModelSetupCount = 1) Then
                Try
                    Result2 = PLCModelObject.CallPLC_PushDownProcess(PLCModelObject)
                Catch ex As Exception
                    'Throw New Exception(ex.Message & " PLC MODEL SETUP RETURN")
                    Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                    Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & rDateF & "_SLC.txt"

                    Dim sOutput As String = rDateF & " - PLC MODEL SETUP RETURN - " & ex.Message
                    Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                    objWriter.WriteLine(sOutput)
                    objWriter.WriteLine(vbCrLf)
                        If Not objWriter Is Nothing Then
                            objWriter.Close()
                            objWriter = Nothing
                        End If
                End Try
                'DEBUG SETUP
                '    If Form1.cb_log = 1 Then
                'Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & "_SLC.txt"
                'Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                'Dim sOutput As String = rDateF & " - " & PMIPSender & " PLC MODEL SETUP RETURN FROM DB: " & Result2
                'Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                'objWriter.WriteLine(sOutput)
                'objWriter.Close()
                'End If

            End If

            Dim sReturn_DB As String = oWatch.ElapsedMilliseconds.ToString

            Dim arrResult() As String
            Dim arrResult2() As String
            arrResult = Result.Split(",")
            If (P_TNX_ID = 4 And cPlcModelSetupCount = 1) Then
                'If P_TNX_ID = 4 Then
                arrResult2 = Result2.Split(",")
            End If

            tagData(0) = Convert.ToInt16(arrResult.GetValue(10))
            tagData(1) = Convert.ToInt16(arrResult.GetValue(0))
            tagData(2) = Convert.ToInt16(arrResult.GetValue(1))
            tagData(3) = Convert.ToInt16(arrResult.GetValue(2))
            tagData(4) = Convert.ToInt16(arrResult.GetValue(3))
            tagData(5) = Convert.ToInt16(arrResult.GetValue(4))
            tagData(6) = Convert.ToInt16(arrResult.GetValue(5))
            tagData(7) = Convert.ToInt16(arrResult.GetValue(6))
            tagData(8) = Convert.ToInt16(arrResult.GetValue(7))
            tagData(9) = Convert.ToInt16(arrResult.GetValue(8))
            tagData(10) = Convert.ToInt16(arrResult.GetValue(9))
            tagData(11) = Convert.ToInt16(arrResult.GetValue(21))
            tagData(12) = Convert.ToInt16(arrResult.GetValue(11))
            tagData(13) = Convert.ToInt16(arrResult.GetValue(12))
            tagData(14) = Convert.ToInt16(arrResult.GetValue(13))
            tagData(15) = Convert.ToInt16(arrResult.GetValue(14))
            tagData(16) = Convert.ToInt16(arrResult.GetValue(15))
            tagData(17) = Convert.ToInt16(arrResult.GetValue(16))
            tagData(18) = Convert.ToInt16(arrResult.GetValue(17))
            tagData(19) = Convert.ToInt16(arrResult.GetValue(18))
            tagData(20) = Convert.ToInt16(arrResult.GetValue(19))
            tagData(21) = Convert.ToInt16(arrResult.GetValue(20))
            tagData(22) = Convert.ToInt16(arrResult.GetValue(32))
            tagData(23) = Convert.ToInt16(arrResult.GetValue(22))
            tagData(24) = Convert.ToInt16(arrResult.GetValue(23))
            tagData(25) = Convert.ToInt16(arrResult.GetValue(24))
            tagData(26) = Convert.ToInt16(arrResult.GetValue(25))
            tagData(27) = Convert.ToInt16(arrResult.GetValue(26))
            tagData(28) = Convert.ToInt16(arrResult.GetValue(27))
            tagData(29) = Convert.ToInt16(arrResult.GetValue(28))
            tagData(30) = Convert.ToInt16(arrResult.GetValue(29))
            tagData(31) = Convert.ToInt16(arrResult.GetValue(30))
            tagData(32) = Convert.ToInt16(arrResult.GetValue(31))
            tagData(33) = Convert.ToInt16(arrResult.GetValue(43))
            tagData(34) = Convert.ToInt16(arrResult.GetValue(33))
            tagData(35) = Convert.ToInt16(arrResult.GetValue(34))
            tagData(36) = Convert.ToInt16(arrResult.GetValue(35))
            tagData(37) = Convert.ToInt16(arrResult.GetValue(36))
            tagData(38) = Convert.ToInt16(arrResult.GetValue(37))
            tagData(39) = Convert.ToInt16(arrResult.GetValue(38))
            tagData(40) = Convert.ToInt16(arrResult.GetValue(39))
            tagData(41) = Convert.ToInt16(arrResult.GetValue(40))
            tagData(42) = Convert.ToInt16(arrResult.GetValue(41))
            tagData(43) = Convert.ToInt16(arrResult.GetValue(42))
            tagData(44) = Convert.ToInt16(arrResult.GetValue(54))
            tagData(45) = Convert.ToInt16(arrResult.GetValue(44))
            tagData(46) = Convert.ToInt16(arrResult.GetValue(45))
            tagData(47) = Convert.ToInt16(arrResult.GetValue(46))
            tagData(48) = Convert.ToInt16(arrResult.GetValue(47))
            tagData(49) = Convert.ToInt16(arrResult.GetValue(48))
            tagData(50) = Convert.ToInt16(arrResult.GetValue(49))
            tagData(51) = Convert.ToInt16(arrResult.GetValue(50))
            tagData(52) = Convert.ToInt16(arrResult.GetValue(51))
            tagData(53) = Convert.ToInt16(arrResult.GetValue(52))
            tagData(54) = Convert.ToInt16(arrResult.GetValue(53))
            tagData(55) = Convert.ToInt16(arrResult.GetValue(65))
            tagData(56) = Convert.ToInt16(arrResult.GetValue(55))
            tagData(57) = Convert.ToInt16(arrResult.GetValue(56))
            tagData(58) = Convert.ToInt16(arrResult.GetValue(57))
            tagData(59) = Convert.ToInt16(arrResult.GetValue(58))
            tagData(60) = Convert.ToInt16(arrResult.GetValue(59))
            tagData(61) = Convert.ToInt16(arrResult.GetValue(60))
            tagData(62) = Convert.ToInt16(arrResult.GetValue(61))
            tagData(63) = Convert.ToInt16(arrResult.GetValue(62))
            tagData(64) = Convert.ToInt16(arrResult.GetValue(63))
            tagData(65) = Convert.ToInt16(arrResult.GetValue(66))
            tagData(66) = Convert.ToInt16(arrResult.GetValue(67))
            tagData(67) = Convert.ToInt16(arrResult.GetValue(68))
            tagData(68) = Convert.ToInt16(arrResult.GetValue(69))
            tagData(69) = Convert.ToInt16(arrResult.GetValue(64))

            'DEBUG SETUP
            'If Form1.cb_log = 1 Then
            'Try
            'Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & "_SLC.txt"
            'Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
            'Dim sOutput As String = rDateF & " - " & PMIPSender & " SETUP RETURN INTO TAG ARRAY"
            'Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
            'objWriter.WriteLine(sOutput)
            'objWriter.Close()
            'Catch ex As Exception

            'End Try

            'End If

            ' PLC setup
            If (P_TNX_ID = 4 And cPlcModelSetupCount = 1) Then
                'If P_TNX_ID = 4 Then
                tagData2(0) = Convert.ToInt16(arrResult2.GetValue(0))
                tagData2(1) = Convert.ToInt16(arrResult2.GetValue(1))
                tagData2(2) = Convert.ToInt16(arrResult2.GetValue(2))
                tagData2(3) = Convert.ToInt16(arrResult2.GetValue(3))
                tagData2(4) = Convert.ToInt16(arrResult2.GetValue(4))
                tagData2(5) = Convert.ToInt16(arrResult2.GetValue(5))
                tagData2(6) = Convert.ToInt16(arrResult2.GetValue(6))
                tagData2(7) = Convert.ToInt16(arrResult2.GetValue(7))
                tagData2(8) = Convert.ToInt16(arrResult2.GetValue(8))
                tagData2(9) = Convert.ToInt16(arrResult2.GetValue(9))
                tagData2(10) = Convert.ToInt16(arrResult2.GetValue(10))
                tagData2(11) = Convert.ToInt16(arrResult2.GetValue(11))
                tagData2(12) = Convert.ToInt16(arrResult2.GetValue(12))
                tagData2(13) = Convert.ToInt16(arrResult2.GetValue(13))
                tagData2(14) = Convert.ToInt16(arrResult2.GetValue(14))
                tagData2(15) = Convert.ToInt16(arrResult2.GetValue(15))
                tagData2(16) = Convert.ToInt16(arrResult2.GetValue(16))
                tagData2(17) = Convert.ToInt16(arrResult2.GetValue(17))
                tagData2(18) = Convert.ToInt16(arrResult2.GetValue(18))
                tagData2(19) = Convert.ToInt16(arrResult2.GetValue(19))
                tagData2(20) = Convert.ToInt16(arrResult2.GetValue(20))
                tagData2(21) = Convert.ToInt16(arrResult2.GetValue(21))
                tagData2(22) = Convert.ToInt16(arrResult2.GetValue(22))
                tagData2(23) = Convert.ToInt16(arrResult2.GetValue(23))
                tagData2(24) = Convert.ToInt16(arrResult2.GetValue(24))
                tagData2(25) = Convert.ToInt16(arrResult2.GetValue(25))
                tagData2(26) = Convert.ToInt16(arrResult2.GetValue(26))
                tagData2(27) = Convert.ToInt16(arrResult2.GetValue(27))
                tagData2(28) = Convert.ToInt16(arrResult2.GetValue(28))
                tagData2(29) = Convert.ToInt16(arrResult2.GetValue(29))
                tagData2(30) = Convert.ToInt16(arrResult2.GetValue(30))
                tagData2(31) = Convert.ToInt16(arrResult2.GetValue(31))
                tagData2(32) = Convert.ToInt16(arrResult2.GetValue(32))
                tagData2(33) = Convert.ToInt16(arrResult2.GetValue(33))
            End If

            'DEBUG SETUP
            '   If Form1.cb_log = 1 Then
            'Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & "_SLC.txt"
            'Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
            'Dim sOutput As String = rDateF & " - " & PMIPSender & " PLC MODEL SETUP TO ARRAY"
            'Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
            'objWriter.WriteLine(sOutput)
            'objWriter.Close()
            'End If

            tag.Value = tagData
            If (P_TNX_ID = 4 And cPlcModelSetupCount = 1) Then
                Tag2.Value = tagData2
            End If

            sBad = ""
            Try
                PLC.WriteTag(tag)
                ''Console.WriteLine(Now.ToString("hh:mm:ss:fff") & " SETUP WRITE: " & PLC.ErrorString)
                'PLC MODEL SETUP Return
                If (P_TNX_ID = 4 And cPlcModelSetupCount = 1) Then
                    'If P_TNX_ID = 4 Then
                    PLC.WriteTag(Tag2)
                End If

                    Dim z As Integer = 0
                    ' Loop.
                    Do While tag.QualityCode <> 192 And z < 5
                        tag.Value = tagData
                        Thread.Sleep(50)
                        PLC.WriteTag(tag)
                        z = z + 1
                    Loop


                    'If tag.QualityCode <> 192 Then
                    '    tag.Value = tagData
                    '    Dim i As Integer
                    '    For i = 1 To 4000
                    '    Next
                    '    PLC.WriteTag(tag)
                    '    ''Console.WriteLine(Now.ToString("hh:mm:ss:fff") & " SETUP REWRITE 1: " & PLC.ErrorString)
                    'End If


                    Dim y As Integer = 0
                    ' Loop.
                    Do While Tag2.QualityCode <> 192 And y < 5
                        Tag2.Value = tagData2
                        Thread.Sleep(50)
                        PLC.WriteTag(Tag2)
                        z = z + 1
                    Loop

                    'If Tag2.QualityCode <> 192 Then
                    '    Tag2.Value = tagData2
                    '    Dim i As Integer
                    '    For i = 1 To 4000
                    '    Next
                    '    PLC.WriteTag(Tag2)
                    '    ''Console.WriteLine(Now.ToString("hh:mm:ss:fff") & " PLC SETUP REWRITE 1: " & PLC.ErrorString)
                    'End If

                'If tag.QualityCode <> 192 Then
                '    Console.WriteLine(Now.ToString("hh:mm:ss:fff") & " **********SETUP ERROR**************")
                'End If
                'If Tag2.QualityCode <> 192 Then
                '    Console.WriteLine(Now.ToString("hh:mm:ss:fff") & " **********SETUP ERROR**************")
                'End If

            Catch ex As Exception
                'Throw New Exception(ex.Message & " PLC SETUP WRITE TO PLC")
                Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & rDateF & "_SLC.txt"

                Dim sOutput As String = rDateF & " - PLC SETUP WRITE TO PLC - " & ex.Message
                Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                objWriter.WriteLine(sOutput)
                objWriter.WriteLine(vbCrLf)
                    If Not objWriter Is Nothing Then
                        objWriter.Close()
                        objWriter = Nothing
                    End If
            End Try

            'DEBUG SETUP
            'If Form1.cb_log = 1 Then
            'Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & "_SLC.txt"
            'Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
            'Dim sOutput As String = rDateF & " - " & PMIPSender & " FIRE RETURN TO PLC"
            'Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
            'objWriter.WriteLine(sOutput)
            'objWriter.Close()
            'End If

            Dim sReturntoPLC As String = oWatch.ElapsedMilliseconds.ToString


                sIN = PMIPSender & " - " & PMItemName
            sIP = PMIPSender & " - " & PMItemName

            Form1.lbl_TotalTrans.Text = "Total Transactions: " & Form1.xCount

            If Form1.cb_log = 1 Then
                Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & "_SLC.txt"
                Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                Dim sOutput As String = rDateF & " - " & sIP & vbCrLf & " Input from PLC: " & sInput & vbCrLf & " DB return: " & Result & vbCrLf & "   SETUP " & P_Cell_ID & "," & P_MODEL_ID & "," & P_TNX_ID & "," & P_OPERATION_ID & "," & P_COMPONENT_ID & vbCrLf & "   Got Data: " & (sGotData \ 1000) & " Send to DB: " & (sSend_DB / 1000) & " Return from DB: " & (sReturn_DB / 1000) & " Return to PLC: " & (sReturntoPLC / 1000) & " Tag 0 QualityCode: " & tag.QualityCode
                    Dim LogTimeOutput As String = rDateF & "," & sIP & ",Got Data," & (sGotData / 1000) & ",Send to DB:," & (sSend_DB / 1000) & ",Return from DB," & (sReturn_DB / 1000) & ",Last Word," & (sReturntoPLC / 1000) & ",QualityCode," & tag.QualityCode
                Form1.LogTimingArray.Add(LogTimeOutput)

            End If

                If Form1.cb_log2 = 1 Then
                    Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                    Dim ScreenOutput As String = rDateF & " - " & sIP & " - " & P_Cell_ID & " Got Data: " & (sGotData / 1000) & " Send to DB: " & (sSend_DB / 1000) & " Return from DB: " & (sReturn_DB / 1000) & " Last Word: " & (sReturntoPLC / 1000) & " QualityCode: " & tag.QualityCode
                    Form1.ScreenList.Add(ScreenOutput)
                    ScreenOutput = String.Empty
                End If
            '''''''''''''''
            ''insert into tx
                Try
                    Dim dat_TX As OES_helper
                    dat_TX = New OES_helper(constr)
                    Dim stB As String = Form1.stBuild.ToString & " - " & PMIPSender & " - " & System.Net.Dns.GetHostName()
                    Dim sOutputtx As String = P_Cell_ID & ",Got Data," & (sGotData / 1000) & ",Send to DB," & (sSend_DB / 1000) & ",Return from DB," & (sReturn_DB / 1000) & ",Last Word," & (sReturntoPLC / 1000) & ",QualityCode," & tag.QualityCode
                    Dim t As New Thread(DirectCast(Sub() dat_TX.InsertIntoTX_LOG(stTransaction, "OES", stB, "OES", sOutputtx, "OES", PMItemName), ThreadStart))
                    t.Start()
                Catch ex As Exception

                    Dim stMess As String = " End of Production "
                    Dim rDateH As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & "-" & Now.ToString("HH")
                    Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & "_" & rDateH & "_SLC.txt"
                    Dim rDateF2 As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                    Dim sOutput2 As String = rDateF2 & " - WRITE TO TX LOG - " & stTransaction & " - " & ex.Message
                    Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                    objWriter.WriteLine(sOutput2)
                    objWriter.WriteLine(vbCrLf)
                    objWriter.Close()
                End Try
            ''''''''''''''''''''''

            PLC.Disconnect()
            ''Console.WriteLine("Result SLC: " & Now.ToString("hh:mm:ss:fff") & " " & Result)
            ''Console.WriteLine("PLC ERRROR String: " & PLC.ErrorCode & " Tag 0 QualityCode: " & tag.QualityCode)
        End If
        ''Console.WriteLine(Form1.xCount)

        oWatch.Reset()

        Catch ex As Exception

            Dim stMess As String = " - PROGRAM END - "
            Dim rDateH As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & "-" & Now.ToString("HH")
            Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & "_" & rDateH & "_SLC.txt"
            Dim rDateF2 As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
            Dim sOutput2 As String = rDateF2 & stMess & ex.Message
            Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
            objWriter.WriteLine(sOutput2)
            objWriter.WriteLine(vbCrLf)
            objWriter.Close()

            stMess = String.Empty
            rDateH = String.Empty
            FILE_NAME = String.Empty
            rDateF2 = String.Empty
            sOutput2 = String.Empty
            Result2 = String.Empty
            stHIP = String.Empty
        End Try

    End Sub

    Private Function ItemValuesV(ByVal x As Array, ByVal y As Integer) As String
        Try
            Dim S As String
            S = ""
            Dim I As Integer
            If y = 1 Then
                S = x.ToString()
            Else
                For I = 0 To x.Length - 1
                    S = S + x(I).ToString() + ","
                Next
            End If
            Return S

        Catch ex As Exception

            Dim stMess As String = "  - ITEMVALUESV Function -  "
            Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
            Dim rDateH As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & "-" & Now.ToString("HH")
            Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & "_" & rDateH & "_SLC.txt"
            Dim sOutput As String = rDateF & stMess & ex.Message
            Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
            objWriter.WriteLine(sOutput)
            objWriter.WriteLine(vbCrLf)
            If Not objWriter Is Nothing Then
                objWriter.Close()
                objWriter = Nothing
            End If

        End Try
    End Function
    Private Function CellIDV(ByVal y As Array) As String
        Try
            Dim x, myVar1, myVar2 As Integer
            Dim myCellID As String
            myCellID = ""
            For x = 0 To 4
                If y(x) <> 0 Then
                    myVar1 = Int(y(x) / 256)
                    myVar2 = Int(y(x) - (myVar1 * 256))
                    If myVar1 <> 0 Then myCellID = myCellID & Chr(myVar1)
                    If myVar2 <> 0 Then myCellID = myCellID & Chr(myVar2)
                End If
            Next
            Return myCellID
        Catch ex As Exception
            Dim stMess As String = "  - CellIDV Function -  "
            Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
            Dim rDateH As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & "-" & Now.ToString("HH")
            Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & "_" & rDateH & "_SLC.txt"
            Dim sOutput As String = rDateF & stMess & ex.Message
            Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
            objWriter.WriteLine(sOutput)
            objWriter.WriteLine(vbCrLf)
            If Not objWriter Is Nothing Then
                objWriter.Close()
                objWriter = Nothing
            End If
        End Try
    End Function
    Private Function ItemIDV(ByVal y As Array) As String
        Try
            Dim x, myVar1, myVar2 As Integer
            Dim myItemID As String
            myItemID = ""
            For x = 5 To 10
                If y(x) <> 0 Then
                    myVar1 = Int(y(x) / 256)
                    myVar2 = Int(y(x) - (myVar1 * 256))
                    If myVar1 <> 0 Then myItemID = myItemID & Chr(myVar1)
                    If myVar2 <> 0 Then myItemID = myItemID & Chr(myVar2)
                End If
            Next
            Return myItemID
        Catch ex As Exception
            Dim stMess As String = "  - ItemIDV Function -  "
            Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
            Dim rDateH As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & "-" & Now.ToString("HH")
            Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & "_" & rDateH & "_SLC.txt"
            Dim sOutput As String = rDateF & stMess & ex.Message
            Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
            objWriter.WriteLine(sOutput)
            objWriter.WriteLine(vbCrLf)
            If Not objWriter Is Nothing Then
                objWriter.Close()
                objWriter = Nothing
            End If
        End Try
    End Function

    Private Function ComponentIDV(ByVal y As Array) As String
        Try
            Dim x, myVar1, myVar2 As Integer
            Dim myComponentID As String
            myComponentID = ""
            x = 11
            If y(x) <> 0 Then
                myVar1 = Int(y(x) / 256)
                myVar2 = Int(y(x) - (myVar1 * 256))
                If myVar1 <> 0 Then myComponentID = myComponentID & Chr(myVar1)
                If myVar2 <> 0 Then myComponentID = myComponentID & Chr(myVar2)
            End If
            Return myComponentID
        Catch ex As Exception
            Dim stMess As String = "  - ComponentIDV Function -  "
            Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
            Dim rDateH As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & "-" & Now.ToString("HH")
            Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & "_" & rDateH & "_SLC.txt"
            Dim sOutput As String = rDateF & stMess & ex.Message
            Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
            objWriter.WriteLine(sOutput)
            objWriter.WriteLine(vbCrLf)
            If Not objWriter Is Nothing Then
                objWriter.Close()
                objWriter = Nothing
            End If
        End Try
    End Function


    Private Function PersonIDV(ByVal y As Array) As String
        Try
            Dim x, myVar1, myVar2 As Integer
            Dim myPersonID As String
            myPersonID = ""
            For x = 5 To 7
                If y(x) <> 0 Then
                    myVar1 = Int(y(x) / 256)
                    myVar2 = Int(y(x) - (myVar1 * 256))
                    If myVar1 <> 0 Then myPersonID = myPersonID & Chr(myVar1)
                    If myVar2 <> 0 Then myPersonID = myPersonID & Chr(myVar2)
                End If
            Next
            Return myPersonID
        Catch ex As Exception
            Dim stMess As String = "  - PersonIDV Function -  "
            Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
            Dim rDateH As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & "-" & Now.ToString("HH")
            Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & "_" & rDateH & "_SLC.txt"
            Dim sOutput As String = rDateF & stMess & ex.Message
            Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
            objWriter.WriteLine(sOutput)
            objWriter.WriteLine(vbCrLf)
            If Not objWriter Is Nothing Then
                objWriter.Close()
                objWriter = Nothing
            End If
        End Try
    End Function

    Private Function ModelIDV(ByVal y As Array) As String
        Try
            Dim x, myVar1, myVar2 As Integer
            Dim myModelID As String
            myModelID = ""
            For x = 22 To 29
                If y(x) <> 0 Then
                    myVar1 = Int(y(x) / 256)
                    myVar2 = Int(y(x) - (myVar1 * 256))
                    If myVar1 <> 0 Then myModelID = myModelID & Chr(myVar1)
                    If myVar2 <> 0 Then myModelID = myModelID & Chr(myVar2)
                End If
            Next
            Return myModelID
        Catch ex As Exception
            Dim stMess As String = "  - ModelIDV Function -  "
            Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
            Dim rDateH As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & "-" & Now.ToString("HH")
            Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & "_" & rDateH & "_SLC.txt"
            Dim sOutput As String = rDateF & stMess & ex.Message
            Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
            objWriter.WriteLine(sOutput)
            objWriter.WriteLine(vbCrLf)
            If Not objWriter Is Nothing Then
                objWriter.Close()
                objWriter = Nothing
            End If
        End Try
    End Function

    Private Function OperationIDV(ByVal y As Array) As String
        Try
            Dim x, myVar1, myVar2 As Integer
            Dim myOperationID As String
            myOperationID = ""
            For x = 30 To 31
                If y(x) <> 0 Then
                    myVar1 = Int(y(x) / 256)
                    myVar2 = Int(y(x) - (myVar1 * 256))
                    If myVar1 <> 0 Then myOperationID = myOperationID & Chr(myVar1)
                    If myVar2 <> 0 Then myOperationID = myOperationID & Chr(myVar2)
                End If
            Next
            Return myOperationID
        Catch ex As Exception
            Dim stMess As String = "  - OperationIDV Function -  "
            Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
            Dim rDateH As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & "-" & Now.ToString("HH")
            Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & "_" & rDateH & "_SLC.txt"
            Dim sOutput As String = rDateF & stMess & ex.Message
            Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
            objWriter.WriteLine(sOutput)
            objWriter.WriteLine(vbCrLf)
            If Not objWriter Is Nothing Then
                objWriter.Close()
                objWriter = Nothing
            End If
        End Try
    End Function

    Private Function BOM_CompIDV(ByVal y As Array) As String
        Try
            Dim x, myVar1, myVar2 As Integer
            Dim myBOM_CompID As String = ""
            ''myBOM_CompID = ""
            For x = 5 To 11
                If y(x) <> 0 Then
                    myVar1 = Int(y(x) / 256)
                    myVar2 = Int(y(x) - (myVar1 * 256))
                    If myVar1 <> 0 Then myBOM_CompID = myBOM_CompID & Chr(myVar1)
                    If myVar2 <> 0 Then myBOM_CompID = myBOM_CompID & Chr(myVar2)
                End If
            Next
            Return myBOM_CompID
        Catch ex As Exception
            Dim stMess As String = "  - BOM_CompIDV Function -  "
            Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
            Dim rDateH As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & "-" & Now.ToString("HH")
            Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & "_" & rDateH & "_SLC.txt"
            Dim sOutput As String = rDateF & stMess & ex.Message
            Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
            objWriter.WriteLine(sOutput)
            objWriter.WriteLine(vbCrLf)
            If Not objWriter Is Nothing Then
                objWriter.Close()
                objWriter = Nothing
            End If
        End Try
    End Function

End Class
