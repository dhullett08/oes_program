Imports System.Data.OleDb

Public Class GetSerial_Process
    Private conn As New OleDbConnection


    Public Sub New(ByVal ConnectionString As String)
        conn.ConnectionString = ConnectionString
    End Sub

    Dim mCELL_ID As String          'IN
    Dim P_SN_WORD1, P_SN_WORD2, P_SN_WORD3, P_SN_WORD4, P_SN_WORD5, P_SN_WORD6, P_SN_WORD7, P_SN_WORD8, P_SN_WORD9, P_SN_WORD10, P_ACKNOWLEDGE As Integer 'OUT
    Public Property SN_ACK()
        Get
            Return P_ACKNOWLEDGE
        End Get
        Set(ByVal value)
            P_ACKNOWLEDGE = value
        End Set
    End Property
    Public Property CELL_ID()
        Get
            Return mCELL_ID
        End Get
        Set(ByVal value)
            mCELL_ID = value
        End Set
    End Property
    Public Property SN_WORD1()
        Get
            Return P_SN_WORD1
        End Get
        Set(ByVal value)
            P_SN_WORD1 = value
        End Set
    End Property
    Public Property SN_WORD2()
        Get
            Return P_SN_WORD2
        End Get
        Set(ByVal value)
            P_SN_WORD2 = value
        End Set
    End Property
    Public Property SN_WORD3()
        Get
            Return P_SN_WORD3
        End Get
        Set(ByVal value)
            P_SN_WORD3 = value
        End Set
    End Property
    Public Property SN_WORD4()
        Get
            Return P_SN_WORD4
        End Get
        Set(ByVal value)
            P_SN_WORD4 = value
        End Set
    End Property
    Public Property SN_WORD5()
        Get
            Return P_SN_WORD5
        End Get
        Set(ByVal value)
            P_SN_WORD5 = value
        End Set
    End Property
    Public Property SN_WORD6()
        Get
            Return P_SN_WORD6
        End Get
        Set(ByVal value)
            P_SN_WORD6 = value
        End Set
    End Property
    Public Property SN_WORD7()
        Get
            Return P_SN_WORD7
        End Get
        Set(ByVal value)
            P_SN_WORD7 = value
        End Set
    End Property
    Public Property SN_WORD8()
        Get
            Return P_SN_WORD8
        End Get
        Set(ByVal value)
            P_SN_WORD8 = value
        End Set
    End Property
    Public Property SN_WORD9()
        Get
            Return P_SN_WORD9
        End Get
        Set(ByVal value)
            P_SN_WORD9 = value
        End Set
    End Property
    Public Property SN_WORD10()
        Get
            Return P_SN_WORD10
        End Get
        Set(ByVal value)
            P_SN_WORD10 = value
        End Set
    End Property


    Public Function CallGetSerialProcess(ByVal GetSerialObject As GetSerial_Process) As String
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Author:                       Jeff ElDridge
        ' Date:                         2012-04-09
        ' Schema:                       
        ' Table References:                   
        ' Description:                  
        ' Revisions:
        ' Notes:                        
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim cmd As New OleDbCommand("ARC.OES_GET_SERIAL", conn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim P_CELL_ID As New OleDbParameter("CELL_ID", OleDbType.VarChar, 40)
        P_CELL_ID.Direction = ParameterDirection.Input
        cmd.Parameters.Add(P_CELL_ID).Value = GetSerialObject.mCELL_ID


        ' OUTWORDS
        Dim P_ACKNOWLEDGE As New OleDbParameter("SN_ACK", OleDbType.VarNumeric)
        P_ACKNOWLEDGE.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_ACKNOWLEDGE)

        Dim P_SN_WORD1 As New OleDbParameter("SN_WORD1", OleDbType.VarNumeric)
        P_SN_WORD1.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_SN_WORD1)

        Dim P_SN_WORD2 As New OleDbParameter("SN_WORD2", OleDbType.VarNumeric)
        P_SN_WORD2.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_SN_WORD2)

        Dim P_SN_WORD3 As New OleDbParameter("SN_WORD3", OleDbType.VarNumeric)
        P_SN_WORD3.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_SN_WORD3)

        Dim P_SN_WORD4 As New OleDbParameter("SN_WORD4", OleDbType.VarNumeric)
        P_SN_WORD4.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_SN_WORD4)

        Dim P_SN_WORD5 As New OleDbParameter("SN_WORD5", OleDbType.VarNumeric)
        P_SN_WORD5.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_SN_WORD5)

        Dim P_SN_WORD6 As New OleDbParameter("SN_WORD6", OleDbType.VarNumeric)
        P_SN_WORD6.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_SN_WORD6)

        Dim P_SN_WORD7 As New OleDbParameter("SN_WORD7", OleDbType.VarNumeric)
        P_SN_WORD7.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_SN_WORD7)

        Dim P_SN_WORD8 As New OleDbParameter("SN_WORD8", OleDbType.VarNumeric)
        P_SN_WORD8.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_SN_WORD8)

        Dim P_SN_WORD9 As New OleDbParameter("SN_WORD9", OleDbType.VarNumeric)
        P_SN_WORD9.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_SN_WORD9)

        Dim P_SN_WORD10 As New OleDbParameter("SN_WORD10", OleDbType.VarNumeric)
        P_SN_WORD10.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_SN_WORD10)

        Try
            conn.Open()
            cmd.ExecuteNonQuery()
            Dim ReturnString As String
            ReturnString = P_SN_WORD1.Value.ToString & "," & P_SN_WORD2.Value.ToString & "," & P_SN_WORD3.Value.ToString & "," & P_SN_WORD4.Value.ToString & "," & P_SN_WORD5.Value.ToString & "," & P_SN_WORD6.Value.ToString & "," & P_SN_WORD7.Value.ToString & "," & P_SN_WORD8.Value.ToString & "," & P_SN_WORD9.Value.ToString & "," & P_SN_WORD10.Value.ToString & "," & P_ACKNOWLEDGE.Value.ToString
            Return ReturnString
        Catch ex As OleDbException
            Throw New Exception(ex.Message)
        Finally
            conn.Close()
            If conn IsNot Nothing Then
                conn.Dispose()
                cmd.Dispose()
            End If
        End Try
    End Function
End Class


