Imports System.Data.OleDb

Public Class PLC_PushDown_Process
    Private conn As New OleDbConnection


    Public Sub New(ByVal ConnectionString As String)
        conn.ConnectionString = ConnectionString
    End Sub

    Dim mCELL_ID As String     'IN
    Dim mModel_ID As String    'IN
    Dim mVal_1 As Integer      'OUT
    Dim mVal_2 As Integer      'OUT
    Dim mVal_3 As Integer      'OUT
    Dim mVal_4 As Integer      'OUT
    Dim mVal_5 As Integer      'OUT
    Dim mVal_6 As Integer      'OUT
    Dim mVal_7 As Integer      'OUT
    Dim mVal_8 As Integer      'OUT
    Dim mVal_9 As Integer      'OUT
    Dim mVal_10 As Integer     'OUT
    Dim mVal_11 As Integer     'OUT
    Dim mVal_12 As Integer     'OUT
    Dim mVal_13 As Integer     'OUT
    Dim mVal_14 As Integer     'OUT
    Dim mVal_15 As Integer     'OUT
    Dim mVal_16 As Integer     'OUT
    Dim mVal_17 As Integer     'OUT
    Dim mVal_18 As Integer     'OUT
    Dim mVal_19 As Integer     'OUT
    Dim mVal_20 As Integer     'OUT
    Dim mVal_21 As Integer     'OUT
    Dim mVal_22 As Integer     'OUT
    Dim mVal_23 As Integer     'OUT
    Dim mVal_24 As Integer     'OUT
    Dim mVal_25 As Integer     'OUT
    Dim mVal_26 As Integer     'OUT
    Dim mVal_27 As Integer     'OUT
    Dim mVal_28 As Integer     'OUT
    Dim mVal_29 As Integer     'OUT
    Dim mVal_30 As Integer     'OUT
    Dim mVal_31 As Integer     'OUT
    Dim mVal_32 As Integer     'OUT
    Dim mVal_33 As Integer     'OUT
    Dim mVal_34 As Integer     'OUT

    Public Property CELL_ID()
        Get
            Return mCELL_ID
        End Get
        Set(ByVal value)
            mCELL_ID = value
        End Set
    End Property
    Public Property Model_ID()
        Get
            Return mModel_ID
        End Get
        Set(ByVal value)
            mModel_ID = value
        End Set
    End Property
    Public Property P_VAL_1()
        Get
            Return mVal_1
        End Get
        Set(ByVal value)
            mVal_1 = value
        End Set
    End Property
    Public Property P_VAL_2()
        Get
            Return mVal_2
        End Get
        Set(ByVal value)
            mVal_2 = value
        End Set
    End Property
    Public Property P_VAL_3()
        Get
            Return mVal_3
        End Get
        Set(ByVal value)
            mVal_3 = value
        End Set
    End Property
    Public Property P_VAL_4()
        Get
            Return mVal_4
        End Get
        Set(ByVal value)
            mVal_4 = value
        End Set
    End Property
    Public Property P_VAL_5()
        Get
            Return mVal_5
        End Get
        Set(ByVal value)
            mVal_5 = value
        End Set
    End Property
    Public Property P_VAL_6()
        Get
            Return mVal_6
        End Get
        Set(ByVal value)
            mVal_6 = value
        End Set
    End Property
    Public Property P_VAL_7()
        Get
            Return mVal_7
        End Get
        Set(ByVal value)
            mVal_7 = value
        End Set
    End Property
    Public Property P_VAL_8()
        Get
            Return mVal_8
        End Get
        Set(ByVal value)
            mVal_8 = value
        End Set
    End Property
    Public Property P_VAL_9()
        Get
            Return mVal_9
        End Get
        Set(ByVal value)
            mVal_9 = value
        End Set
    End Property
    Public Property P_VAL_10()
        Get
            Return mVal_10
        End Get
        Set(ByVal value)
            mVal_10 = value
        End Set
    End Property
    Public Property P_VAL_11()
        Get
            Return mVal_11
        End Get
        Set(ByVal value)
            mVal_11 = value
        End Set
    End Property
    Public Property P_VAL_12()
        Get
            Return mVal_12
        End Get
        Set(ByVal value)
            mVal_12 = value
        End Set
    End Property
    Public Property P_VAL_13()
        Get
            Return mVal_13
        End Get
        Set(ByVal value)
            mVal_13 = value
        End Set
    End Property
    Public Property P_VAL_14()
        Get
            Return mVal_14
        End Get
        Set(ByVal value)
            mVal_14 = value
        End Set
    End Property
    Public Property P_VAL_15()
        Get
            Return mVal_15
        End Get
        Set(ByVal value)
            mVal_15 = value
        End Set
    End Property
    Public Property P_VAL_16()
        Get
            Return mVal_16
        End Get
        Set(ByVal value)
            mVal_16 = value
        End Set
    End Property
    Public Property P_VAL_17()
        Get
            Return mVal_17
        End Get
        Set(ByVal value)
            mVal_17 = value
        End Set
    End Property
    Public Property P_VAL_18()
        Get
            Return mVal_18
        End Get
        Set(ByVal value)
            mVal_18 = value
        End Set
    End Property
    Public Property P_VAL_19()
        Get
            Return mVal_19
        End Get
        Set(ByVal value)
            mVal_19 = value
        End Set
    End Property
    Public Property P_VAL_20()
        Get
            Return mVal_20
        End Get
        Set(ByVal value)
            mVal_20 = value
        End Set
    End Property
    Public Property P_VAL_21()
        Get
            Return mVal_21
        End Get
        Set(ByVal value)
            mVal_21 = value
        End Set
    End Property
    Public Property P_VAL_22()
        Get
            Return mVal_22
        End Get
        Set(ByVal value)
            mVal_22 = value
        End Set
    End Property
    Public Property P_VAL_23()
        Get
            Return mVal_23
        End Get
        Set(ByVal value)
            mVal_23 = value
        End Set
    End Property
    Public Property P_VAL_24()
        Get
            Return mVal_24
        End Get
        Set(ByVal value)
            mVal_24 = value
        End Set
    End Property
    Public Property P_VAL_25()
        Get
            Return mVal_25
        End Get
        Set(ByVal value)
            mVal_25 = value
        End Set
    End Property
    Public Property P_VAL_26()
        Get
            Return mVal_26
        End Get
        Set(ByVal value)
            mVal_26 = value
        End Set
    End Property
    Public Property P_VAL_27()
        Get
            Return mVal_27
        End Get
        Set(ByVal value)
            mVal_27 = value
        End Set
    End Property
    Public Property P_VAL_28()
        Get
            Return mVal_28
        End Get
        Set(ByVal value)
            mVal_28 = value
        End Set
    End Property
    Public Property P_VAL_29()
        Get
            Return mVal_29
        End Get
        Set(ByVal value)
            mVal_29 = value
        End Set
    End Property
    Public Property P_VAL_30()
        Get
            Return mVal_30
        End Get
        Set(ByVal value)
            mVal_30 = value
        End Set
    End Property
    Public Property P_VAL_31()
        Get
            Return mVal_31
        End Get
        Set(ByVal value)
            mVal_31 = value
        End Set
    End Property
    Public Property P_VAL_32()
        Get
            Return mVal_32
        End Get
        Set(ByVal value)
            mVal_32 = value
        End Set
    End Property
    Public Property P_VAL_33()
        Get
            Return mVal_33
        End Get
        Set(ByVal value)
            mVal_33 = value
        End Set
    End Property
    Public Property P_VAL_34()
        Get
            Return mVal_34
        End Get
        Set(ByVal value)
            mVal_34 = value
        End Set
    End Property
    Public Function CallPLC_PushDownProcess(ByVal PLC_PushDownObject As PLC_PushDown_Process) As String
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Author:                       Jeff ElDridge
        ' Date:                         3012-04-09
        ' Schema:                       
        ' Table References:                   
        ' Description:                  
        ' Revisions:
        ' Notes:                        
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim cmd As New OleDbCommand("arc.oes_PLC_SETUP_VALUES", conn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim P_CELL_ID As New OleDbParameter("CELL_ID", OleDbType.VarChar, 40)
        P_CELL_ID.Direction = ParameterDirection.Input
        cmd.Parameters.Add(P_CELL_ID).Value = PLC_PushDownObject.mCELL_ID

        Dim P_Model_ID As New OleDbParameter("Model_ID", OleDbType.VarChar, 40)
        P_Model_ID.Direction = ParameterDirection.Input
        cmd.Parameters.Add(P_Model_ID).Value = PLC_PushDownObject.mModel_ID


        ' OUTWORDS

        Dim P_VAL_1 As New OleDbParameter("VAL_1", OleDbType.VarNumeric)
        P_VAL_1.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_1)

        Dim P_VAL_2 As New OleDbParameter("VAL_2", OleDbType.VarNumeric)
        P_VAL_2.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_2)

        Dim P_VAL_3 As New OleDbParameter("VAL_3", OleDbType.VarNumeric)
        P_VAL_3.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_3)

        Dim P_VAL_4 As New OleDbParameter("VAL_4", OleDbType.VarNumeric)
        P_VAL_4.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_4)

        Dim P_VAL_5 As New OleDbParameter("VAL_5", OleDbType.VarNumeric)
        P_VAL_5.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_5)

        Dim P_VAL_6 As New OleDbParameter("VAL_6", OleDbType.VarNumeric)
        P_VAL_6.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_6)

        Dim P_VAL_7 As New OleDbParameter("VAL_7", OleDbType.VarNumeric)
        P_VAL_7.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_7)

        Dim P_VAL_8 As New OleDbParameter("VAL_8", OleDbType.VarNumeric)
        P_VAL_8.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_8)

        Dim P_VAL_9 As New OleDbParameter("VAL_9", OleDbType.VarNumeric)
        P_VAL_9.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_9)

        Dim P_VAL_10 As New OleDbParameter("VAL_10", OleDbType.VarNumeric)
        P_VAL_10.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_10)

        Dim P_VAL_11 As New OleDbParameter("VAL_11", OleDbType.VarNumeric)
        P_VAL_11.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_11)

        Dim P_VAL_12 As New OleDbParameter("VAL_12", OleDbType.VarNumeric)
        P_VAL_12.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_12)

        Dim P_VAL_13 As New OleDbParameter("VAL_13", OleDbType.VarNumeric)
        P_VAL_13.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_13)

        Dim P_VAL_14 As New OleDbParameter("VAL_14", OleDbType.VarNumeric)
        P_VAL_14.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_14)

        Dim P_VAL_15 As New OleDbParameter("VAL_15", OleDbType.VarNumeric)
        P_VAL_15.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_15)

        Dim P_VAL_16 As New OleDbParameter("VAL_16", OleDbType.VarNumeric)
        P_VAL_16.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_16)

        Dim P_VAL_17 As New OleDbParameter("VAL_17", OleDbType.VarNumeric)
        P_VAL_17.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_17)

        Dim P_VAL_18 As New OleDbParameter("VAL_18", OleDbType.VarNumeric)
        P_VAL_18.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_18)

        Dim P_VAL_19 As New OleDbParameter("VAL_19", OleDbType.VarNumeric)
        P_VAL_19.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_19)

        Dim P_VAL_20 As New OleDbParameter("VAL_20", OleDbType.VarNumeric)
        P_VAL_20.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_20)

        Dim P_VAL_21 As New OleDbParameter("VAL_21", OleDbType.VarNumeric)
        P_VAL_21.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_21)

        Dim P_VAL_22 As New OleDbParameter("VAL_22", OleDbType.VarNumeric)
        P_VAL_22.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_22)

        Dim P_VAL_23 As New OleDbParameter("VAL_23", OleDbType.VarNumeric)
        P_VAL_23.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_23)

        Dim P_VAL_24 As New OleDbParameter("VAL_24", OleDbType.VarNumeric)
        P_VAL_24.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_24)

        Dim P_VAL_25 As New OleDbParameter("VAL_25", OleDbType.VarNumeric)
        P_VAL_25.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_25)

        Dim P_VAL_26 As New OleDbParameter("VAL_26", OleDbType.VarNumeric)
        P_VAL_26.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_26)

        Dim P_VAL_27 As New OleDbParameter("VAL_27", OleDbType.VarNumeric)
        P_VAL_27.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_27)

        Dim P_VAL_28 As New OleDbParameter("VAL_28", OleDbType.VarNumeric)
        P_VAL_28.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_28)

        Dim P_VAL_29 As New OleDbParameter("VAL_29", OleDbType.VarNumeric)
        P_VAL_29.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_29)

        Dim P_VAL_30 As New OleDbParameter("VAL_30", OleDbType.VarNumeric)
        P_VAL_30.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_30)

        Dim P_VAL_31 As New OleDbParameter("VAL_31", OleDbType.VarNumeric)
        P_VAL_31.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_31)

        Dim P_VAL_32 As New OleDbParameter("VAL_32", OleDbType.VarNumeric)
        P_VAL_32.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_32)

        Dim P_VAL_33 As New OleDbParameter("VAL_33", OleDbType.VarNumeric)
        P_VAL_33.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_33)

        Dim P_VAL_34 As New OleDbParameter("VAL_34", OleDbType.VarNumeric)
        P_VAL_34.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_34)

        Try
            conn.Open()
            cmd.ExecuteNonQuery()
            Dim ReturnString As String
            ReturnString = P_VAL_1.Value.ToString & "," & P_VAL_2.Value.ToString & "," & P_VAL_3.Value.ToString & "," & P_VAL_4.Value.ToString & "," & P_VAL_5.Value.ToString & "," & P_VAL_6.Value.ToString & "," & P_VAL_7.Value.ToString & "," & P_VAL_8.Value.ToString & "," & P_VAL_9.Value.ToString & "," & P_VAL_10.Value.ToString & "," & P_VAL_11.Value.ToString & "," & P_VAL_12.Value.ToString & "," & P_VAL_13.Value.ToString & "," & P_VAL_14.Value.ToString & "," & P_VAL_15.Value.ToString & "," & P_VAL_16.Value.ToString & "," & P_VAL_17.Value.ToString & "," & P_VAL_18.Value.ToString & "," & P_VAL_19.Value.ToString & "," & P_VAL_20.Value.ToString & "," & P_VAL_21.Value.ToString & "," & P_VAL_22.Value.ToString & "," & P_VAL_23.Value.ToString & "," & P_VAL_24.Value.ToString & "," & P_VAL_25.Value.ToString & "," & P_VAL_26.Value.ToString & "," & P_VAL_27.Value.ToString & "," & P_VAL_28.Value.ToString & "," & P_VAL_29.Value.ToString & "," & P_VAL_30.Value.ToString & "," & P_VAL_31.Value.ToString & "," & P_VAL_32.Value.ToString & "," & P_VAL_33.Value.ToString & "," & P_VAL_34.Value.ToString
            Return ReturnString

        Catch ex As OleDbException
            Throw New Exception(ex.Message)
        Finally
            conn.Close()
        End Try
    End Function
End Class

