Imports System.Net
Imports ABLink
Imports Logix
Imports System.Configuration
Imports System.Diagnostics
Imports System.IO
Imports System.Timers
Imports System.Threading

Public Class LOGIX_MSG
    Public dat As OES_helper
    Public constr As String
    Dim WithEvents peerMsg As New Logix.PeerMessage()
    Dim senderIP, fromDataTable, stHIP As String
    Dim inDataArray(0 To 49) As Integer
    Dim outDataArray(0 To 49) As Integer
    Dim outBOM_DataArray(0 To 70) As Integer
    Dim P_Cell_ID, P_ITEM_ID, P_COMPONENT_ID, P_PERSON_ID, P_MODEL_ID, Result2, sBad, sResend, ProdIPLogix As String
    Dim SetUpIPLogix, LoginIPLogix, GetSerialIPLogix As String
    Dim P_PROCESS_INDICATOR, P_SUCCESS_INDICATOR, P_PROCESS_FAULT_CODE, P_TNX_ID, P_OPERATION_ID As Integer
    Dim P_VAL_1, P_VAL_2, P_VAL_3, P_VAL_4, P_VAL_5, P_VAL_6, P_VAL_7, P_VAL_8, P_VAL_9, P_VAL_10 As Integer
    Dim P_VAL_11, P_VAL_12, P_VAL_13, P_VAL_14, P_VAL_15, P_VAL_16, P_VAL_17, P_VAL_18, P_VAL_19, P_VAL_20 As Integer
    Dim P_VAL_21, P_VAL_22, P_VAL_23, P_VAL_24, P_VAL_25, P_VAL_26, P_VAL_27, P_VAL_28, P_STATUS_CODE, cN227, cN237 As Integer
    Dim cN197, cN207, cN217, cN247, z, P_ACCESS_ID_1, P_ACCESS_ID_2, P_ACCESS_ID_3, P_ACCESS_ID_4, P_ACCESS_ID_5, P_ACCESS_ID_6 As Integer
    Dim INWORD0, INWORD1, INWORD2, INWORD3, INWORD4, INWORD5, INWORD6, INWORD7, INWORD8, INWORD9, INWORD10, INWORD11 As Integer

    Public Sub MsgReceivedlogixT(ByVal arj2 As Object)
        'LOGIX BLOCK
        'REM data sent to message is populated in the peerMsg.Value.  The data from
        'peerMsg.Value is moved into dataArray.
        Monitor.Enter(arj2)
        Dim argInput2 As Logix.MessageEventArgs = arj2
        'Unlock object
        Monitor.Exit(arj2)

        Dim PLC As New Logix.Controller()
        PLC.Timeout = 3000

        constr = ConfigurationManager.ConnectionStrings("DBConnection").ConnectionString
        Dim Production_Process As New Production_Process(constr)
        Dim Login_Process As New Login_Process(constr)
        Dim GetSerial_Process As New GetSerial_Process(constr)
        dat = New OES_helper(constr)

        Dim dataArray2 As Array = argInput2.Value
        Dim myLength2 As Integer = argInput2.Length
        Dim PMItemName2 As String = argInput2.ItemName
        Dim PMIPSender2 As String = argInput2.IPSender

        Dim sInput As String = ItemValuesV2(dataArray2, myLength2)
        Dim sInputT As String = Now.ToString("hh:mm:ss:fff")
        ' Console.WriteLine("Input Logix " & sInputT & " " & sInput)

        'stopwatch 
        Dim oWatch As New Stopwatch
        oWatch.Reset()
        oWatch.Start()


        'peerMsg.ItemName is the address that the message is sent to.  Store this value in fromDataTable to
        'determine the type of processor to send the data back to.

        'the peerMsg.Length is the length of the message sent from the plc.
        'The data is moved into the P_data depending on length of message.
        Dim msgItem As ListViewItem
        Dim sIP, sIN, sItemName As String
        Form1.xCount = Form1.xCount + 1

        Dim StartTime As DateTime = Now

        ''PRODUCTION LOGIC
        If PMItemName2 = "N197[0]" Or PMItemName2 = "N207[0]" Or PMItemName2 = "N217[0]" Then
            ProdIPLogix = PMIPSender2
            Dim ProductionObject As New Production_Process(dat.ConnectionString)

            Dim tag As Logix.Tag
            Dim tagData(myLength2 - 1) As Int16
            'Set IP address of PLC to send data
            PLC.IPAddress = ProdIPLogix

            sIP = ProdIPLogix & " - " & PMItemName2
            sIN = "BLANK"
            sItemName = PMItemName2

            P_Cell_ID = CellID2V(dataArray2)
            P_ITEM_ID = ItemID2V(dataArray2)
            P_COMPONENT_ID = ComponentID2V(dataArray2)
            P_PROCESS_INDICATOR = dataArray2(18)
            P_SUCCESS_INDICATOR = dataArray2(19)
            P_PROCESS_FAULT_CODE = dataArray2(20)
            P_STATUS_CODE = dataArray2(21)
            P_VAL_1 = dataArray2(22)
            P_VAL_2 = dataArray2(23)
            P_VAL_3 = dataArray2(24)
            P_VAL_4 = dataArray2(25)
            P_VAL_5 = dataArray2(26)
            P_VAL_6 = dataArray2(27)
            P_VAL_7 = dataArray2(28)
            P_VAL_8 = dataArray2(29)
            P_VAL_9 = dataArray2(30)
            P_VAL_10 = dataArray2(31)
            P_VAL_11 = dataArray2(32)
            P_VAL_12 = dataArray2(33)
            If myLength2 = 50 Then
                P_VAL_13 = dataArray2(34)
                P_VAL_14 = dataArray2(35)
                P_VAL_15 = dataArray2(36)
                P_VAL_16 = dataArray2(37)
                P_VAL_17 = dataArray2(38)
                P_VAL_18 = dataArray2(39)
                P_VAL_19 = dataArray2(40)
                P_VAL_20 = dataArray2(41)
                P_VAL_21 = dataArray2(42)
                P_VAL_22 = dataArray2(43)
                P_VAL_23 = dataArray2(44)
                P_VAL_24 = dataArray2(45)
                P_VAL_25 = dataArray2(46)
                P_VAL_26 = dataArray2(47)
                P_VAL_27 = dataArray2(48)
                P_VAL_28 = dataArray2(49)
            End If
            INWORD0 = dataArray2(0)
            INWORD1 = dataArray2(1)
            INWORD2 = dataArray2(2)
            INWORD3 = dataArray2(3)
            INWORD4 = dataArray2(4)
            INWORD5 = dataArray2(5)
            INWORD6 = dataArray2(6)
            INWORD7 = dataArray2(7)
            INWORD8 = dataArray2(8)
            INWORD9 = dataArray2(9)
            INWORD10 = dataArray2(10)
            INWORD11 = dataArray2(11)

            Dim sGotData As String = oWatch.ElapsedMilliseconds.ToString
            stHIP = P_Cell_ID & "," & P_ITEM_ID & "," & P_COMPONENT_ID & "," & P_PROCESS_INDICATOR & "," & P_SUCCESS_INDICATOR & "," & P_PROCESS_FAULT_CODE

            If PMItemName2 = "N197[0]" Then
                cN197 = cN197 + 1
                sIN = PMIPSender2 & " - " & PMItemName2 & " - " & cN197
            End If

            If PMItemName2 = "N207[0]" Then
                cN207 = cN207 + 1
                sIN = PMIPSender2 & " - " & PMItemName2 & " - " & cN207
            End If

            If PMItemName2 = "N217[0]" Then
                cN217 = cN217 + 1
                sIN = PMIPSender2 & " - " & PMItemName2 & " - " & cN217
            End If

            With ProductionObject
                ProductionObject.CELL_ID = P_Cell_ID
                ProductionObject.ITEM_ID = P_ITEM_ID
                ProductionObject.COMPONENT_ID = P_COMPONENT_ID
                ProductionObject.PROCESS_INDICATOR = P_PROCESS_INDICATOR
                ProductionObject.STATUS_CODE = P_STATUS_CODE
                ProductionObject.SUCCESS_INDICATOR = P_SUCCESS_INDICATOR
                ProductionObject.VAL_1 = P_VAL_1
                ProductionObject.VAL_2 = P_VAL_2
                ProductionObject.VAL_3 = P_VAL_3
                ProductionObject.VAL_4 = P_VAL_4
                ProductionObject.VAL_5 = P_VAL_5
                ProductionObject.VAL_6 = P_VAL_6
                ProductionObject.VAL_7 = P_VAL_7
                ProductionObject.VAL_8 = P_VAL_8
                ProductionObject.VAL_9 = P_VAL_9
                ProductionObject.VAL_10 = P_VAL_10
                ProductionObject.VAL_11 = P_VAL_11
                ProductionObject.VAL_12 = P_VAL_12
                If myLength2 = 50 Then
                    ProductionObject.VAL_13 = P_VAL_13
                    ProductionObject.VAL_14 = P_VAL_14
                    ProductionObject.VAL_14 = P_VAL_14
                    ProductionObject.VAL_15 = P_VAL_15
                    ProductionObject.VAL_16 = P_VAL_16
                    ProductionObject.VAL_17 = P_VAL_17
                    ProductionObject.VAL_18 = P_VAL_18
                    ProductionObject.VAL_19 = P_VAL_19
                    ProductionObject.VAL_20 = P_VAL_20
                    ProductionObject.VAL_21 = P_VAL_21
                    ProductionObject.VAL_22 = P_VAL_22
                    ProductionObject.VAL_23 = P_VAL_23
                    ProductionObject.VAL_24 = P_VAL_24
                    ProductionObject.VAL_25 = P_VAL_25
                    ProductionObject.VAL_26 = P_VAL_26
                    ProductionObject.VAL_27 = P_VAL_27
                    ProductionObject.VAL_28 = P_VAL_28
                End If
                ProductionObject.PROCESS_FAULT_CODE = P_PROCESS_FAULT_CODE
                ProductionObject.INWORD0 = INWORD0
                ProductionObject.INWORD1 = INWORD1
                ProductionObject.INWORD2 = INWORD2
                ProductionObject.INWORD3 = INWORD3
                ProductionObject.INWORD4 = INWORD4
                ProductionObject.INWORD5 = INWORD5
                ProductionObject.INWORD6 = INWORD6
                ProductionObject.INWORD7 = INWORD7
                ProductionObject.INWORD8 = INWORD8
                ProductionObject.INWORD9 = INWORD9
                ProductionObject.INWORD10 = INWORD10
                ProductionObject.INWORD11 = INWORD11
            End With

            Dim sSend_DB As String = oWatch.ElapsedMilliseconds.ToString
            Dim Result As String
            Try
                Result = ProductionObject.CallProductionProcess(ProductionObject)
            Catch ex As Exception
                Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & ".txt"
                Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                Dim sOutput As String = rDateF & " - CALL PRODUCTION PROCESS LOGIX - " & ex.Message
                Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                objWriter.WriteLine(sOutput)
                objWriter.WriteLine(vbCrLf)
                objWriter.Close()
            End Try

            Dim sReturn_DB As String = oWatch.ElapsedMilliseconds.ToString

            If Form1.cb_LogFile.CheckState = 1 Then
                msgItem.SubItems.Add(P_Cell_ID & " - " & Result)
            End If

            If PMItemName2 = "N197[0]" Then
                tag = New Logix.Tag("N198[0]")
                If myLength2 = 50 Then
                    tag.Length = 50
                Else
                    tag.Length = 34
                End If
            End If


            If PMItemName2 = "N207[0]" Then
                tag = New Logix.Tag("N208[0]")

                If myLength2 = 50 Then
                    tag.Length = 50
                Else
                    tag.Length = 34
                End If
            End If

            If PMItemName2 = "N217[0]" Then
                tag = New Logix.Tag("N218[0]")

                If myLength2 = 50 Then
                    tag.Length = 50
                Else
                    tag.Length = 34
                End If
            End If

                Dim arrResult() As String
                arrResult = Result.Split(",")
                'Console.WriteLine("Result Logix: " & Now.ToString("hh:mm:ss:fff") & " " & Result)
                'cell_id
                tagData(0) = Convert.ToInt16(arrResult.GetValue(0))
                tagData(1) = Convert.ToInt16(arrResult.GetValue(1))
                tagData(2) = Convert.ToInt16(arrResult.GetValue(2))
                tagData(3) = Convert.ToInt16(arrResult.GetValue(3))
                tagData(4) = Convert.ToInt16(arrResult.GetValue(4))
                'item_id
                tagData(5) = Convert.ToInt16(arrResult.GetValue(5))
                tagData(6) = Convert.ToInt16(arrResult.GetValue(6))
                tagData(7) = Convert.ToInt16(arrResult.GetValue(7))
                tagData(8) = Convert.ToInt16(arrResult.GetValue(8))
                tagData(9) = Convert.ToInt16(arrResult.GetValue(9))
                tagData(10) = Convert.ToInt16(arrResult.GetValue(10))
                'Generant Barcode
                tagData(11) = Convert.ToInt16(arrResult.GetValue(11))
                '12 is a counter word not returned
                tagData(12) = 0
                'Part Retry Status
                tagData(13) = Convert.ToInt16(arrResult.GetValue(12))
                tagData(14) = 0
                tagData(15) = 0
                tagData(16) = 0
                tagData(17) = 0
                'Transaction Request
                tagData(18) = Convert.ToInt16(arrResult.GetValue(13))
                'PASS \FAIL
                tagData(19) = Convert.ToInt16(arrResult.GetValue(14))
                'Process Failure Code
                tagData(20) = Convert.ToInt16(arrResult.GetValue(15))
                '21 is returned VERY LAST
                tagData(21) = Convert.ToInt16(arrResult.GetValue(16))
                tagData(22) = Convert.ToInt16(arrResult.GetValue(17))
                tagData(23) = Convert.ToInt16(arrResult.GetValue(18))
                tagData(24) = Convert.ToInt16(arrResult.GetValue(19))
                tagData(25) = Convert.ToInt16(arrResult.GetValue(20))
                tagData(26) = Convert.ToInt16(arrResult.GetValue(21))
                tagData(27) = Convert.ToInt16(arrResult.GetValue(22))
                tagData(28) = Convert.ToInt16(arrResult.GetValue(23))
                tagData(29) = Convert.ToInt16(arrResult.GetValue(24))
                tagData(30) = Convert.ToInt16(arrResult.GetValue(25))
                tagData(31) = Convert.ToInt16(arrResult.GetValue(26))
                tagData(32) = Convert.ToInt16(arrResult.GetValue(27))
                tagData(33) = Convert.ToInt16(arrResult.GetValue(28))
            If myLength2 = 50 Then
                tagData(34) = Convert.ToInt16(arrResult.GetValue(29))
                tagData(35) = Convert.ToInt16(arrResult.GetValue(30))
                tagData(36) = Convert.ToInt16(arrResult.GetValue(31))
                tagData(37) = Convert.ToInt16(arrResult.GetValue(32))
                tagData(38) = Convert.ToInt16(arrResult.GetValue(33))
                tagData(39) = Convert.ToInt16(arrResult.GetValue(34))
                tagData(40) = Convert.ToInt16(arrResult.GetValue(35))
                tagData(41) = Convert.ToInt16(arrResult.GetValue(36))
                tagData(42) = Convert.ToInt16(arrResult.GetValue(37))
                tagData(43) = Convert.ToInt16(arrResult.GetValue(38))
                tagData(44) = Convert.ToInt16(arrResult.GetValue(39))
                tagData(45) = Convert.ToInt16(arrResult.GetValue(40))
                tagData(46) = Convert.ToInt16(arrResult.GetValue(41))
                tagData(47) = Convert.ToInt16(arrResult.GetValue(42))
                tagData(48) = Convert.ToInt16(arrResult.GetValue(43))
                tagData(49) = Convert.ToInt16(arrResult.GetValue(44))
            End If

                sBad = ""
                sResend = ""
                tag.Value = tagData
                'Console.WriteLine(tagData)

            Try
                PLC.WriteTag(tag)
                Console.WriteLine(Now.ToString("hh:mm:ss:fff") & " PROD LOGIX: " & myLength2 & " - " & tag.QualityCode)

                If tag.QualityCode <> 192 Then
                    tag.Value = tagData
                    Dim i As Integer
                    For i = 1 To 4000
                    Next
                    PLC.WriteTag(tag)
                    Console.WriteLine(Now.ToString("hh:mm:ss:fff") & " PROD LOGIX REWRITE 1: " & tag.QualityCode)
                End If

                If tag.QualityCode <> 192 Then
                    tag.Value = tagData
                    Dim i As Integer
                    For i = 1 To 4000
                    Next
                    PLC.WriteTag(tag)
                    Console.WriteLine(Now.ToString("hh:mm:ss:fff") & " PROD LOGIX REWRITE 2: " & tag.QualityCode)
                End If

                If tag.QualityCode <> 192 Then
                    tag.Value = tagData
                    Dim i As Integer
                    For i = 1 To 4000
                    Next
                    PLC.WriteTag(tag)
                    Console.WriteLine(Now.ToString("hh:mm:ss:fff") & " PROD LOGIX REWRITE 3: " & tag.QualityCode)
                End If

                If tag.QualityCode <> 192 Then
                    tag.Value = tagData
                    Dim i As Integer
                    For i = 1 To 4000
                    Next
                    PLC.WriteTag(tag)
                    Console.WriteLine(Now.ToString("hh:mm:ss:fff") & " PROD LOGIX REWRITE 4: " & tag.QualityCode)
                End If

                If tag.QualityCode <> 192 Then
                    Console.WriteLine(Now.ToString("hh:mm:ss:fff") & " **********PROD LOGIX ERROR**************")
                End If
            Catch ex As Exception
                Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & ".txt"
                Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                Dim sOutput As String = rDateF & " - WRITE TAG PRODUCTION LOGIX - " & ex.Message
                Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                objWriter.WriteLine(sOutput)
                objWriter.WriteLine(vbCrLf)
                objWriter.Close()
            End Try

            sBad = " PLC Version: " & PLC.Version & " PLC Timeout:" & PLC.Timeout & " PLC ErrorCode:" & PLC.ErrorCode & " PLC IsConnected:" & PLC.IsConnected & " Tag 0 QualityCode: " & tag.QualityCode
                Dim sReturntoPLC As String = oWatch.ElapsedMilliseconds.ToString

                If Form1.cb_LogFile.CheckState = 1 Then
                    msgItem.SubItems.Add("Got Data: " & (sGotData) & " Send to DB: " & (sSend_DB) & " Return from DB: " & (sReturn_DB) & " Last Word: " & (sReturntoPLC))
                End If

                'write to text file
                Form1.lbl_TotalTrans.Text = "Total Transactions: " & Form1.xCount
                If Form1.cb_log = 1 Then
                    Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & ".txt"
                    Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                    Dim sOutput As String = rDateF & " - " & sIP & vbCrLf & " Input from PLC: " & sInput & vbCrLf & " Result from DB: " & Result & vbCrLf & "    " & stHIP & vbCrLf & "    Got Data: " & (sGotData / 1000) & " Send to DB: " & (sSend_DB / 1000) & " Return from DB: " & (sReturn_DB / 1000) & " Last Word: " & (sReturntoPLC / 1000)
                sInput = sInput & vbCrLf

                Dim ScreenOutput As String = rDateF & " - " & sIP & " Got Data: " & (sGotData / 1000) & " Send to DB: " & (sSend_DB / 1000) & " Return from DB: " & (sReturn_DB / 1000) & " Last Word: " & (sReturntoPLC / 1000)
                Form1.ScreenList.Add(ScreenOutput)

                    Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                    objWriter.WriteLine(sOutput)
                    objWriter.WriteLine(sBad)
                    objWriter.WriteLine(vbCrLf)
                    If sResend <> "" Then
                        objWriter.WriteLine(sResend)
                        objWriter.WriteLine(vbCrLf)
                    End If
                    objWriter.Close()
                End If
            PLC.Disconnect()
            Console.WriteLine(P_Cell_ID & " LOGIX PLC ErrorCode: " & PLC.ErrorCode & " PLC IsConnected: " & PLC.IsConnected & " Tag 0 QualityCode: " & tag.QualityCode)

            End If

            If PMItemName2 = "N227[0]" Then
                ' Login Logix
                Dim LoginObject As New Login_Process(dat.ConnectionString)
                LoginIPLogix = PMIPSender2

                'Dim x As Integer
                'Set IP address of PLC to send data
                PLC.IPAddress = LoginIPLogix
                sBad = ""
                sResend = ""

                sIP = LoginIPLogix & " - " & PMItemName2
                sIN = "BLANK"
                sItemName = PMItemName2

                Dim sGotData As String = oWatch.ElapsedMilliseconds.ToString

                P_Cell_ID = CellID2V(dataArray2)
                P_PERSON_ID = PersonID2V(dataArray2)
                P_TNX_ID = dataArray2(18)

                Try
                    With LoginObject
                        LoginObject.CELL_ID = P_Cell_ID
                        LoginObject.EMPLOYEE_ID = P_PERSON_ID
                        LoginObject.TNX_ID = P_TNX_ID
                    End With
                Catch ex As Exception
                    Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & ".txt"
                    Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                    Dim sOutput As String = rDateF & " - LOGIN OBJECT LOGIX - " & ex.Message
                    Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                    objWriter.WriteLine(sOutput)
                    objWriter.WriteLine(vbCrLf)
                    objWriter.Close()
                End Try

                Dim sSend_DB As String = oWatch.ElapsedMilliseconds.ToString
                Dim Result As String = LoginObject.CallLoginProcess(LoginObject)
                Dim sReturn_DB As String = oWatch.ElapsedMilliseconds.ToString

                'Tag = New Logix.Tag("N228[0]", 33)
                'Dim Tag As Logix.Tag
                Dim tag As Logix.Tag
                Dim tagData(33) As Int16
                tag = New Logix.Tag("N228[0]")
                tag.Length = 33
                Dim x As Integer
                For x = 0 To 33
                    tagData(x) = 0
                Next

                Dim arrResult() As String

                Try
                    arrResult = Result.Split(",")
                    tagData(21) = Convert.ToInt16(arrResult.GetValue(0))
                    tagData(20) = Convert.ToInt16(arrResult.GetValue(1))
                Catch ex As Exception

                End Try

                tag.Value = tagData

                Try

                PLC.WriteTag(tag)

                If tag.QualityCode <> 192 Then
                    tag.Value = tagData
                    Dim i As Integer
                    For i = 1 To 4000
                    Next
                    PLC.WriteTag(tag)
                    Console.WriteLine(Now.ToString("hh:mm:ss:fff") & " LOGIN LOGIX REWRITE 1: " & tag.QualityCode)
                End If

                If tag.QualityCode <> 192 Then
                    tag.Value = tagData
                    Dim i As Integer
                    For i = 1 To 4000
                    Next
                    PLC.WriteTag(tag)
                    Console.WriteLine(Now.ToString("hh:mm:ss:fff") & " LOGIN LOGIX REWRITE 2: " & tag.QualityCode)
                End If

                If tag.QualityCode <> 192 Then
                    tag.Value = tagData
                    Dim i As Integer
                    For i = 1 To 4000
                    Next
                    PLC.WriteTag(tag)
                    Console.WriteLine(Now.ToString("hh:mm:ss:fff") & " LOGIN LOGIX REWRITE 3: " & tag.QualityCode)
                End If


                sResend = "RESEND"

            Catch ex As Exception
                Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & ".txt"
                Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                Dim sOutput As String = rDateF & " - WRITE TAG LOGIN-OFF LOGIX - " & ex.Message
                Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                objWriter.WriteLine(sOutput)
                objWriter.WriteLine(vbCrLf)
                objWriter.Close()
            End Try
            'Console.WriteLine("Result Logix: " & Now.ToString("hh:mm:ss:fff") & " " & Result)
            ''''cut code here

            Dim sReturntoPLC As String = oWatch.ElapsedMilliseconds.ToString

            cN227 = cN227 + 1
            sIN = PMIPSender2 & " - " & PMItemName2 & " - " & cN227

            Form1.lbl_TotalTrans.Text = "Total Transactions: " & Form1.xCount
            If Form1.cb_LogFile.CheckState = 1 Then
                Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & ".txt"
                Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                Dim sOutput As String = rDateF & " - " & sIP & vbCrLf & " Input from PLC: " & sInput & vbCrLf & " DB return " & Result & vbCrLf & "    Login " & P_Cell_ID & " " & P_PERSON_ID & "  " & P_TNX_ID & vbCrLf & "    Got Data: " & (sGotData / 1000) & " Send to DB: " & (sSend_DB / 1000) & " Return from DB: " & (sReturn_DB / 1000) & " Return to PLC: " & (sReturntoPLC / 1000)

                Dim ScreenOutput As String = rDateF & " - " & sIP & " Got Data: " & (sGotData / 1000) & " Send to DB: " & (sSend_DB / 1000) & " Return from DB: " & (sReturn_DB / 1000) & " Last Word: " & (sReturntoPLC / 1000)
                Form1.ScreenList.Add(ScreenOutput)

                Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                objWriter.WriteLine(sOutput)
                objWriter.WriteLine(sBad & vbCrLf)
                If sResend <> "" Then
                    objWriter.WriteLine(sResend & vbCrLf)
                End If
                objWriter.Close()
            End If
            PLC.Disconnect()
        End If

        If PMItemName2 = "N247[20]" Then
            ' GET SERIAL
            Dim oWatch2 As New Stopwatch
            oWatch2.Start()
            Dim GetSerialObject As New GetSerial_Process(dat.ConnectionString)

            Dim tag As Logix.Tag
            'Dim x As Integer
            Dim Result, sReturn_DB, sReturntoPLC, sErr As String
            Dim arrResult() As String
            'Set IP address of PLC to send data
            GetSerialIPLogix = PMIPSender2
            PLC.IPAddress = GetSerialIPLogix
            'Convert 
            P_Cell_ID = CellID2V(dataArray2)

            sIP = PMIPSender2 & " - " & PMItemName2
            sIN = "BLANK"
            sItemName = PMItemName2

            Dim sGotData As String = oWatch2.ElapsedMilliseconds.ToString

            Try
                With GetSerialObject
                    GetSerialObject.CELL_ID = P_Cell_ID
                End With
            Catch ex As Exception
                Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & ".txt"
                'Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.Hour.ToString.PadLeft(2, "0") & ":" & Now.Minute.ToString.PadLeft(2, "0") & ":" & Now.Second.ToString.PadLeft(2, "0")
                Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                Dim sOutput As String = rDateF & " - CALL GET SERIAL OBJECT LOGIX - " & ex.Message
                Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                objWriter.WriteLine(sOutput)
                objWriter.WriteLine(vbCrLf)
                objWriter.Close()
            End Try

            'DEBUG GET SERIAL
            If Form1.cb_LogFile.CheckState = 1 Then
                Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & ".txt"
                Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                Dim sOutput As String = rDateF & " - " & PMIPSender2 & " GET SERIAL OBJECT GET SERIAL"
                Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                objWriter.WriteLine(sOutput)
                objWriter.Close()
            End If

            Dim sSend_DB As String = oWatch2.ElapsedMilliseconds.ToString

            tag = New Logix.Tag("N247[0]")
            tag.Length = 11

            Try
                Result = GetSerialObject.CallGetSerialProcess(GetSerialObject)
            Catch ex As Exception
                sErr = ex.Message
                Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & ".txt"
                Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                Dim sOutput As String = rDateF & "   VB Error Get Serial " & sErr & vbCrLf
                Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                objWriter.WriteLine(sOutput)
                objWriter.WriteLine(vbCrLf)
                objWriter.Close()
            End Try

            'DEBUG GET SERIAL
            If Form1.cb_LogFile.CheckState = 1 Then
                Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & ".txt"
                Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                Dim sOutput As String = rDateF & " - " & PMIPSender2 & " CallGetSerialProcess GET SERIAL"
                Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                objWriter.WriteLine(sOutput)
                objWriter.Close()
            End If

            sReturn_DB = oWatch2.ElapsedMilliseconds.ToString
            arrResult = Result.Split(",")

            Dim PlcData(10) As System.Int16

            PlcData(0) = Convert.ToInt16(arrResult.GetValue(0))
            PlcData(1) = Convert.ToInt16(arrResult.GetValue(1))
            PlcData(2) = Convert.ToInt16(arrResult.GetValue(2))
            PlcData(3) = Convert.ToInt16(arrResult.GetValue(3))
            PlcData(4) = Convert.ToInt16(arrResult.GetValue(4))
            PlcData(5) = Convert.ToInt16(arrResult.GetValue(5))
            PlcData(6) = Convert.ToInt16(arrResult.GetValue(6))
            PlcData(7) = Convert.ToInt16(arrResult.GetValue(7))
            PlcData(8) = Convert.ToInt16(arrResult.GetValue(8))
            PlcData(9) = Convert.ToInt16(arrResult.GetValue(9))
            PlcData(10) = Convert.ToInt16(arrResult.GetValue(10))

            tag.Value = PlcData
            'tag.Length = PlcData.Length
            sResend = ""


            Try
                PLC.WriteTag(tag)
                
                If tag.QualityCode <> 192 Then
                    tag.Value = PlcData
                    Dim i As Integer
                    For i = 1 To 4000
                    Next
                    PLC.WriteTag(tag)
                    Console.WriteLine(Now.ToString("hh:mm:ss:fff") & " LOGIN LOGIX REWRITE 1: " & tag.QualityCode)
                End If

                If tag.QualityCode <> 192 Then
                    tag.Value = PlcData
                    Dim i As Integer
                    For i = 1 To 4000
                    Next
                    PLC.WriteTag(tag)
                    Console.WriteLine(Now.ToString("hh:mm:ss:fff") & " LOGIN LOGIX REWRITE 2: " & tag.QualityCode)
                End If

                If tag.QualityCode <> 192 Then
                    tag.Value = PlcData
                    Dim i As Integer
                    For i = 1 To 4000
                    Next
                    PLC.WriteTag(tag)
                    Console.WriteLine(Now.ToString("hh:mm:ss:fff") & " LOGIN LOGIX REWRITE 3: " & tag.QualityCode)
                End If

            Catch ex As Exception
                sErr = ex.Message
                Dim rDate As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.Hour.ToString.PadLeft(2, "0") & ":" & Now.Minute.ToString.PadLeft(2, "0") & ":" & Now.Second.ToString.PadLeft(2, "0")
                Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & ".txt"
                Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                Dim sOutput As String = rDate & " VB Error Get Serial " & sErr
                Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                objWriter.WriteLine(sOutput)
                objWriter.WriteLine(vbCrLf)
                objWriter.Close()
            End Try

            ' Console.WriteLine("Result Logix: " & Now.ToString("hh:mm:ss:fff") & " " & Result)

            'DEBUG GET SERIAL
            If Form1.cb_log = 1 Then
                Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & ".txt"
                Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                Dim sOutput As String = rDateF & " - " & PMIPSender2 & " WRITE TAG GET SERIAL"
                Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                objWriter.WriteLine(sOutput)
                objWriter.Close()
            End If

            sBad = " Error string = " & PLC.ErrorString & " PLC Version: " & PLC.Version & " PLC Timeout: " & PLC.Timeout & " Tag 0 QualityCode: " & tag.QualityCode
            sReturntoPLC = oWatch2.ElapsedMilliseconds.ToString
            cN247 = cN247 + 1
            sIN = GetSerialIPLogix & " - " & PMItemName2 & " - " & cN247
            sIP = GetSerialIPLogix & " - " & PMItemName2

            Form1.lbl_TotalTrans.Text = "Total Transactions: " & Form1.xCount
            If Form1.cb_log = 1 Then
                Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & ".txt"
                Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                Dim sOutput As String = rDateF & " - " & sIP & vbCrLf & " Input from PLC: " & sInput & vbCrLf & " DB return " & Result & vbCrLf & "   Get Serial " & P_Cell_ID & vbCrLf & "   Got Data: " & (sGotData \ 1000) & " Send to DB: " & (sSend_DB / 1000) & " Return from DB: " & (sReturn_DB / 1000) & " Return to PLC: " & (sReturntoPLC / 1000)

                Dim ScreenOutput As String = rDateF & " - " & sIP & " Got Data: " & (sGotData / 1000) & " Send to DB: " & (sSend_DB / 1000) & " Return from DB: " & (sReturn_DB / 1000) & " Last Word: " & (sReturntoPLC / 1000)
                Form1.ScreenList.Add(ScreenOutput)

                Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                objWriter.WriteLine(sOutput)
                objWriter.WriteLine(sBad)
                objWriter.WriteLine(sResend)
                objWriter.WriteLine(vbCrLf)
                objWriter.Close()
            End If
            oWatch2.Stop()
            PLC.Disconnect()
            Console.WriteLine(P_Cell_ID & " PLC ErrorCode: " & PLC.ErrorCode & " PLC IsConnected: " & PLC.IsConnected & " Tag 0 QualityCode: " & tag.QualityCode)
        End If

        If PMItemName2 = "N237[0]" Then
            ' Setup
            Dim SetupObject As New Setup_Process(dat.ConnectionString)
            Dim PLCModelObject As New PLC_PushDown_Process(dat.ConnectionString)
            SetUpIPLogix = PMIPSender2

            'Full 70
            Dim tag As Logix.Tag
            Dim Tag2 As Logix.Tag
            Dim tagData(69) As Int16
            Dim tagData2(33) As Int16

            tag = New Logix.Tag("N238[0]")
            tag.Length = 70

            Tag2 = New Logix.Tag("N241[0]")
            Tag2.Length = 34

            'Set IP address of PLC to send data
            PLC.IPAddress = SetUpIPLogix

            P_Cell_ID = CellID2V(dataArray2)
            P_MODEL_ID = ModelID2V(dataArray2)
            P_TNX_ID = dataArray2(18)
            P_OPERATION_ID = OperationID2V(dataArray2)
            P_COMPONENT_ID = BOM_CompID2V(dataArray2)

            Dim sGotData As String = oWatch.ElapsedMilliseconds.ToString

            Try
                With SetupObject
                    SetupObject.CELL_ID = P_Cell_ID
                    SetupObject.MODEL_ID = P_MODEL_ID
                    SetupObject.OPERATION = P_OPERATION_ID
                    SetupObject.TRANSACTION_ID = P_TNX_ID
                    SetupObject.ACCESS_ID = dataArray2(16)
                    SetupObject.ITEM_ID = P_COMPONENT_ID
                End With
            Catch ex As Exception
                Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & ".txt"
                Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                Dim sOutput As String = rDateF & " - SETUP OBJECT LOGIX - " & ex.Message
                Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                objWriter.WriteLine(sOutput)
                objWriter.WriteLine(vbCrLf)
                objWriter.Close()
            End Try

            Dim cPlcModelSetupCount As Integer = dat.GetPlcModelSetupCount(P_Cell_ID, P_MODEL_ID)

            Try
                If (P_TNX_ID = 4 And cPlcModelSetupCount = 1) Then
                    With PLCModelObject
                        PLCModelObject.CELL_ID = P_Cell_ID
                        PLCModelObject.Model_ID = P_MODEL_ID
                    End With
                End If
            Catch ex As Exception
                Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & ".txt"
                Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                Dim sOutput As String = rDateF & " - PLC MODEL SETUP LOGIX - " & ex.Message
                Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                objWriter.WriteLine(sOutput)
                objWriter.WriteLine(vbCrLf)
                objWriter.Close()
            End Try

            Dim sSend_DB As String = oWatch.ElapsedMilliseconds.ToString
            Dim Result As String = SetupObject.CallSetupProcess(SetupObject)

            sIP = PMIPSender2 & " - " & PMItemName2
            sIN = "BLANK"
            sItemName = PMItemName2

            Try
                If (P_TNX_ID = 4 And cPlcModelSetupCount = 1) Then
                    Result2 = PLCModelObject.CallPLC_PushDownProcess(PLCModelObject)
                End If
            Catch ex As Exception
                Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & ".txt"
                Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                Dim sOutput As String = rDateF & " - PLC MODEL SETUP VB LOGIX - " & ex.Message
                Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                objWriter.WriteLine(sOutput)
                objWriter.WriteLine(vbCrLf)
                objWriter.Close()
            End Try

            Dim sReturn_DB As String = oWatch.ElapsedMilliseconds.ToString

            Dim arrResult() As String
            Dim arrResult2() As String

            If (P_TNX_ID = 4 And cPlcModelSetupCount = 1) Then
                arrResult2 = Result2.Split(",")
            End If

            arrResult = Result.Split(",")

            tagData(0) = Convert.ToInt16(arrResult.GetValue(10))
            tagData(1) = Convert.ToInt16(arrResult.GetValue(0))
            tagData(2) = Convert.ToInt16(arrResult.GetValue(1))
            tagData(3) = Convert.ToInt16(arrResult.GetValue(2))
            tagData(4) = Convert.ToInt16(arrResult.GetValue(3))
            tagData(5) = Convert.ToInt16(arrResult.GetValue(4))
            tagData(6) = Convert.ToInt16(arrResult.GetValue(5))
            tagData(7) = Convert.ToInt16(arrResult.GetValue(6))
            tagData(8) = Convert.ToInt16(arrResult.GetValue(7))
            tagData(9) = Convert.ToInt16(arrResult.GetValue(8))
            tagData(10) = Convert.ToInt16(arrResult.GetValue(9))
            tagData(11) = Convert.ToInt16(arrResult.GetValue(21))
            tagData(12) = Convert.ToInt16(arrResult.GetValue(11))
            tagData(13) = Convert.ToInt16(arrResult.GetValue(12))
            tagData(14) = Convert.ToInt16(arrResult.GetValue(13))
            tagData(15) = Convert.ToInt16(arrResult.GetValue(14))
            tagData(16) = Convert.ToInt16(arrResult.GetValue(15))
            tagData(17) = Convert.ToInt16(arrResult.GetValue(16))
            tagData(18) = Convert.ToInt16(arrResult.GetValue(17))
            tagData(19) = Convert.ToInt16(arrResult.GetValue(18))
            tagData(20) = Convert.ToInt16(arrResult.GetValue(19))
            tagData(21) = Convert.ToInt16(arrResult.GetValue(20))
            tagData(22) = Convert.ToInt16(arrResult.GetValue(32))
            tagData(23) = Convert.ToInt16(arrResult.GetValue(22))
            tagData(24) = Convert.ToInt16(arrResult.GetValue(23))
            tagData(25) = Convert.ToInt16(arrResult.GetValue(24))
            tagData(26) = Convert.ToInt16(arrResult.GetValue(25))
            tagData(27) = Convert.ToInt16(arrResult.GetValue(26))
            tagData(28) = Convert.ToInt16(arrResult.GetValue(27))
            tagData(29) = Convert.ToInt16(arrResult.GetValue(28))
            tagData(30) = Convert.ToInt16(arrResult.GetValue(29))
            tagData(31) = Convert.ToInt16(arrResult.GetValue(30))
            tagData(32) = Convert.ToInt16(arrResult.GetValue(31))
            tagData(33) = Convert.ToInt16(arrResult.GetValue(43))
            tagData(34) = Convert.ToInt16(arrResult.GetValue(33))
            tagData(35) = Convert.ToInt16(arrResult.GetValue(34))
            tagData(36) = Convert.ToInt16(arrResult.GetValue(35))
            tagData(37) = Convert.ToInt16(arrResult.GetValue(36))
            tagData(38) = Convert.ToInt16(arrResult.GetValue(37))
            tagData(39) = Convert.ToInt16(arrResult.GetValue(38))
            tagData(40) = Convert.ToInt16(arrResult.GetValue(39))
            tagData(41) = Convert.ToInt16(arrResult.GetValue(40))
            tagData(42) = Convert.ToInt16(arrResult.GetValue(41))
            tagData(43) = Convert.ToInt16(arrResult.GetValue(42))
            tagData(44) = Convert.ToInt16(arrResult.GetValue(54))
            tagData(45) = Convert.ToInt16(arrResult.GetValue(44))
            tagData(46) = Convert.ToInt16(arrResult.GetValue(45))
            tagData(47) = Convert.ToInt16(arrResult.GetValue(46))
            tagData(48) = Convert.ToInt16(arrResult.GetValue(47))
            tagData(49) = Convert.ToInt16(arrResult.GetValue(48))
            tagData(50) = Convert.ToInt16(arrResult.GetValue(49))
            tagData(51) = Convert.ToInt16(arrResult.GetValue(50))
            tagData(52) = Convert.ToInt16(arrResult.GetValue(51))
            tagData(53) = Convert.ToInt16(arrResult.GetValue(52))
            tagData(54) = Convert.ToInt16(arrResult.GetValue(53))
            tagData(55) = Convert.ToInt16(arrResult.GetValue(65))
            tagData(56) = Convert.ToInt16(arrResult.GetValue(55))
            tagData(57) = Convert.ToInt16(arrResult.GetValue(56))
            tagData(58) = Convert.ToInt16(arrResult.GetValue(57))
            tagData(59) = Convert.ToInt16(arrResult.GetValue(58))
            tagData(60) = Convert.ToInt16(arrResult.GetValue(59))
            tagData(61) = Convert.ToInt16(arrResult.GetValue(60))
            tagData(62) = Convert.ToInt16(arrResult.GetValue(61))
            tagData(63) = Convert.ToInt16(arrResult.GetValue(62))
            tagData(64) = Convert.ToInt16(arrResult.GetValue(63))
            tagData(65) = Convert.ToInt16(arrResult.GetValue(66))
            tagData(66) = Convert.ToInt16(arrResult.GetValue(67))
            tagData(67) = Convert.ToInt16(arrResult.GetValue(68))
            tagData(68) = Convert.ToInt16(arrResult.GetValue(69))
            tagData(69) = Convert.ToInt16(arrResult.GetValue(64))

            ' PLC setup
            If (P_TNX_ID = 4 And cPlcModelSetupCount = 1) Then
                tagData2(0) = Convert.ToInt16(arrResult2.GetValue(0))
                tagData2(1) = Convert.ToInt16(arrResult2.GetValue(1))
                tagData2(2) = Convert.ToInt16(arrResult2.GetValue(2))
                tagData2(3) = Convert.ToInt16(arrResult2.GetValue(3))
                tagData2(4) = Convert.ToInt16(arrResult2.GetValue(4))
                tagData2(5) = Convert.ToInt16(arrResult2.GetValue(5))
                tagData2(6) = Convert.ToInt16(arrResult2.GetValue(6))
                tagData2(7) = Convert.ToInt16(arrResult2.GetValue(7))
                tagData2(8) = Convert.ToInt16(arrResult2.GetValue(8))
                tagData2(9) = Convert.ToInt16(arrResult2.GetValue(9))
                tagData2(10) = Convert.ToInt16(arrResult2.GetValue(10))
                tagData2(11) = Convert.ToInt16(arrResult2.GetValue(11))
                tagData2(12) = Convert.ToInt16(arrResult2.GetValue(12))
                tagData2(13) = Convert.ToInt16(arrResult2.GetValue(13))
                tagData2(14) = Convert.ToInt16(arrResult2.GetValue(14))
                tagData2(15) = Convert.ToInt16(arrResult2.GetValue(15))
                tagData2(16) = Convert.ToInt16(arrResult2.GetValue(16))
                tagData2(17) = Convert.ToInt16(arrResult2.GetValue(17))
                tagData2(18) = Convert.ToInt16(arrResult2.GetValue(18))
                tagData2(19) = Convert.ToInt16(arrResult2.GetValue(19))
                tagData2(20) = Convert.ToInt16(arrResult2.GetValue(20))
                tagData2(21) = Convert.ToInt16(arrResult2.GetValue(21))
                tagData2(22) = Convert.ToInt16(arrResult2.GetValue(22))
                tagData2(23) = Convert.ToInt16(arrResult2.GetValue(23))
                tagData2(24) = Convert.ToInt16(arrResult2.GetValue(24))
                tagData2(25) = Convert.ToInt16(arrResult2.GetValue(25))
                tagData2(26) = Convert.ToInt16(arrResult2.GetValue(26))
                tagData2(27) = Convert.ToInt16(arrResult2.GetValue(27))
                tagData2(28) = Convert.ToInt16(arrResult2.GetValue(28))
                tagData2(29) = Convert.ToInt16(arrResult2.GetValue(29))
                tagData2(30) = Convert.ToInt16(arrResult2.GetValue(30))
                tagData2(31) = Convert.ToInt16(arrResult2.GetValue(31))
                tagData2(32) = Convert.ToInt16(arrResult2.GetValue(32))
                tagData2(33) = Convert.ToInt16(arrResult2.GetValue(33))

                Tag2.Value = tagData2
            End If

            tag.Value = tagData

            sBad = ""
            PLC.Connect()
            If PLC.IsConnected = False Then
                Dim i As Integer
                For i = 1 To 500
                Next
                PLC.Connect()
                sBad = "ReConnect 1 " & PLC.IsConnected
            End If

            If PLC.IsConnected = False Then
                Dim i As Integer
                For i = 1 To 500
                Next
                PLC.Connect()
                sBad = sBad & " ReConnect 2 " & PLC.IsConnected
            End If

            If PLC.IsConnected = True Then
                Try
                    PLC.WriteTag(tag)
                    'PLC MODEL SETUP Return
                    Console.WriteLine(P_Cell_ID & " PLC ErrorCode: " & PLC.ErrorCode & " PLC IsConnected: " & PLC.IsConnected & " Tag 0 QualityCode: " & tag.QualityCode)
                    If (P_TNX_ID = 4 And cPlcModelSetupCount = 1) Then
                        PLC.WriteTag(Tag2)
                    End If
                Catch ex As Exception
                    Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & ".txt"
                    Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                    Dim sOutput As String = rDateF & " - WRITE TAG PLC MODEL SETUP LOGIX - " & ex.Message
                    Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                    objWriter.WriteLine(sOutput)
                    objWriter.WriteLine(vbCrLf)
                    objWriter.Close()
                End Try
            End If

            Dim sReturntoPLC As String = oWatch.ElapsedMilliseconds.ToString

            'Console.WriteLine("Result Logix: " & Now.ToString("hh:mm:ss:fff") & " " & Result)

            cN237 = cN237 + 1
            sIN = PMIPSender2 & " - " & PMItemName2 & " - " & cN237
            sIP = PMIPSender2 & " - " & PMItemName2

            Form1.lbl_TotalTrans.Text = "Total Transactions: " & Form1.xCount
            If Form1.cb_log = 1 Then
                Dim FILE_NAME As String = ConfigurationManager.AppSettings("OES_LOG") & ".txt"
                Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.ToString("hh:mm:ss:fff")
                Dim sOutput As String = rDateF & " - " & sIP & vbCrLf & " Input from PLC: " & sInput & vbCrLf & " DB return: " & Result & vbCrLf & "   SETUP " & P_Cell_ID & vbCrLf & "   Got Data: " & (sGotData \ 1000) & " Send to DB: " & (sSend_DB / 1000) & " Return from DB: " & (sReturn_DB / 1000) & " Return to PLC: " & (sReturntoPLC / 1000) & " Tag 0 QualityCode: " & tag.QualityCode

                Dim ScreenOutput As String = rDateF & " - " & sIP & " Got Data: " & (sGotData / 1000) & " Send to DB: " & (sSend_DB / 1000) & " Return from DB: " & (sReturn_DB / 1000) & " Last Word: " & (sReturntoPLC / 1000)
                Form1.ScreenList.Add(ScreenOutput)

                Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                objWriter.WriteLine(sOutput)
                If sBad <> "" Then
                    objWriter.WriteLine(sBad)
                End If
                objWriter.WriteLine(vbCrLf)
                objWriter.Close()
                sBad = ""
            End If
            PLC.Disconnect()

        End If
    End Sub
    Private Function ItemValuesV2(ByVal x As Array, ByVal y As Integer) As String
        Dim S As String
        S = ""
        Dim I As Integer
        If y = 1 Then
            S = x.ToString()
        Else
            For I = 0 To x.Length - 1
                S = S + x(I).ToString() + ","
            Next
        End If
        Return S
    End Function
    Private Function CellID2V(ByVal y As Array) As String
        Dim x, myVar1, myVar2 As Integer
        Dim myCellID As String
        myCellID = ""
        For x = 0 To 4
            If y(x) <> 0 Then
                myVar1 = Int(y(x) / 256)
                myVar2 = Int(y(x) - (myVar1 * 256))
                If myVar1 <> 0 Then myCellID = myCellID & Chr(myVar1)
                If myVar2 <> 0 Then myCellID = myCellID & Chr(myVar2)
            End If
        Next
        Return myCellID
    End Function

    Private Function ItemID2V(ByVal y As Array) As String
        Dim x, myVar1, myVar2 As Integer
        Dim myItemID As String
        myItemID = ""
        For x = 5 To 10
            If y(x) <> 0 Then
                myVar1 = Int(y(x) / 256)
                myVar2 = Int(y(x) - (myVar1 * 256))
                If myVar1 <> 0 Then myItemID = myItemID & Chr(myVar1)
                If myVar2 <> 0 Then myItemID = myItemID & Chr(myVar2)
            End If
        Next
        Return myItemID
    End Function

    Private Function ComponentID2V(ByVal y As Array) As String
        Dim x, myVar1, myVar2 As Integer
        Dim myComponentID As String
        myComponentID = ""
        x = 11
        If y(x) <> 0 Then
            myVar1 = Int(y(x) / 256)
            myVar2 = Int(y(x) - (myVar1 * 256))
            If myVar1 <> 0 Then myComponentID = myComponentID & Chr(myVar1)
            If myVar2 <> 0 Then myComponentID = myComponentID & Chr(myVar2)
        End If
        Return myComponentID
    End Function

    Private Function PersonID2V(ByVal y As Array) As String
        Dim x, myVar1, myVar2 As Integer
        Dim myPersonID As String
        myPersonID = ""
        For x = 5 To 7
            If y(x) <> 0 Then
                myVar1 = Int(y(x) / 256)
                myVar2 = Int(y(x) - (myVar1 * 256))
                If myVar1 <> 0 Then myPersonID = myPersonID & Chr(myVar1)
                If myVar2 <> 0 Then myPersonID = myPersonID & Chr(myVar2)
            End If
        Next
        Return myPersonID
    End Function

    Private Function ModelID2V(ByVal y As Array) As String
        Dim x, myVar1, myVar2 As Integer
        Dim myModelID As String
        myModelID = ""
        For x = 22 To 29
            If y(x) <> 0 Then
                myVar1 = Int(y(x) / 256)
                myVar2 = Int(y(x) - (myVar1 * 256))
                If myVar1 <> 0 Then myModelID = myModelID & Chr(myVar1)
                If myVar2 <> 0 Then myModelID = myModelID & Chr(myVar2)
            End If
        Next
        Return myModelID
    End Function

    Private Function OperationID2V(ByVal y As Array) As String

        Dim x, myVar1, myVar2 As Integer
        Dim myOperationID As String
        myOperationID = ""
        For x = 30 To 31
            If y(x) <> 0 Then
                myVar1 = Int(y(x) / 256)
                myVar2 = Int(y(x) - (myVar1 * 256))
                If myVar1 <> 0 Then myOperationID = myOperationID & Chr(myVar1)
                If myVar2 <> 0 Then myOperationID = myOperationID & Chr(myVar2)
            End If
        Next
        Return myOperationID
    End Function

    Private Function BOM_CompID2V(ByVal y As Array) As String

        Dim x, myVar1, myVar2 As Integer
        Dim myBOM_CompID As String

        myBOM_CompID = ""
        For x = 5 To 11
            If y(x) <> 0 Then
                myVar1 = Int(y(x) / 256)
                myVar2 = Int(y(x) - (myVar1 * 256))
                If myVar1 <> 0 Then myBOM_CompID = myBOM_CompID & Chr(myVar1)
                If myVar2 <> 0 Then myBOM_CompID = myBOM_CompID & Chr(myVar2)
            End If
        Next
        Return myBOM_CompID
    End Function


End Class
