﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("NETABLINK.vb.UnsolicitedMessage")> 
<Assembly: AssemblyDescription("NET.ABLINK 3.0 Unsolicited Message Example")> 
<Assembly: AssemblyCompany("CimQuest INGEAR, LLC")> 
<Assembly: AssemblyProduct("INGEAR.NET.ABLINK 3.0")> 
<Assembly: AssemblyCopyright("Copyright ©  2011")> 
<Assembly: AssemblyTrademark("INGEAR. INGEAR.NET")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("248ddc3e-07dc-44be-b5bb-804f7d1280ed")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("3.0.2010.2018")> 
<Assembly: AssemblyFileVersion("3.0.2010.2018")> 
