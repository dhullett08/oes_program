Imports System.Net
Imports ABLink
Imports Logix
Imports System.Configuration
Imports System.Diagnostics
Imports System.IO
Imports System.Timers
Imports System.Threading

Public Class Form1
    Public dat As OES_helper
    Public datSLC As SLC_MSG
    Public datLogix As LOGIX_MSG
    Public constr As String
    Public StartTime As DateTime
    Public ElapsedTime As TimeSpan
    Public msgItem As ListViewItem
    Public Shared xCount As Integer
    Public Shared cb_log As Integer
    Public Shared ScreenList As New ArrayList

    'Public PLC As New ABLink.Controller()
    ' *********************************
    ' * instance of ABLINK.PeerMessage Class with
    ' * event handler
    'Dim WithEvents peerMsg As New ABLink.PeerMessage()
    Dim WithEvents peerMsg As New Logix.PeerMessage()
    Dim WithEvents peerMsg2 As New Logix.PeerMessage()
    Dim senderIP, fromDataTable, stHIP As String
    Dim inDataArray(0 To 49) As Integer
    Dim outDataArray(0 To 49) As Integer
    Dim outBOM_DataArray(0 To 70) As Integer
    Dim P_Cell_ID, P_ITEM_ID, P_COMPONENT_ID, P_PERSON_ID, P_MODEL_ID, ProdIP, SetupIP, LoginIP, GetSerialIP, Result2, sBad, sResend, ProdIPLogix As String
    Dim SetUpIPLogix, LoginIPLogix, GetSerialIPLogix As String
    Dim P_PROCESS_INDICATOR, P_SUCCESS_INDICATOR, P_PROCESS_FAULT_CODE, P_TNX_ID, P_OPERATION_ID As Integer
    Dim P_VAL_1, P_VAL_2, P_VAL_3, P_VAL_4, P_VAL_5, P_VAL_6, P_VAL_7, P_VAL_8, P_VAL_9, P_VAL_10 As Integer
    Dim P_VAL_11, P_VAL_12, P_VAL_13, P_VAL_14, P_VAL_15, P_VAL_16, P_VAL_17, P_VAL_18, P_VAL_19, P_VAL_20 As Integer
    Dim P_VAL_21, P_VAL_22, P_VAL_23, P_VAL_24, P_VAL_25, P_VAL_26, P_VAL_27, P_VAL_28, P_STATUS_CODE, cN227, cN237 As Integer
    Dim cN197, cN207, cN217, cN247, z, P_ACCESS_ID_1, P_ACCESS_ID_2, P_ACCESS_ID_3, P_ACCESS_ID_4, P_ACCESS_ID_5, P_ACCESS_ID_6 As Integer
    Dim INWORD0, INWORD1, INWORD2, INWORD3, INWORD4, INWORD5, INWORD6, INWORD7, INWORD8, INWORD9, INWORD10, INWORD11 As Integer


    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        '* **********************************
        '* * add colums to list view
        '* *
        listView.View = View.Details
        listView.Columns.Add("OES DATA", 500, HorizontalAlignment.Left)

        StartTime = Now

        constr = ConfigurationManager.ConnectionStrings("DBConnection").ConnectionString
        Dim Production_Process As New Production_Process(constr)
        Dim Login_Process As New Login_Process(constr)
        Dim GetSerial_Process As New GetSerial_Process(constr)

        dat = New OES_helper(constr)
        datSLC = New SLC_MSG()
        datLogix = New LOGIX_MSG()
        'cycle count
        xCount = 0
        cN197 = 0
        cN207 = 0
        cN217 = 0
        cN227 = 0
        cN237 = 0
        cN247 = 0
        z = 0

        Me.cb_SLC.Enabled = True
        Me.cb_LOGIX.Enabled = True

        Me.lbl_Build.Text = "BUILD: " & "201209041105"
        Me.lbl_IP_Address.Text = "SLC IP: " & ConfigurationManager.AppSettings("AB_PLC") & vbCrLf & "Logix IP: " & ConfigurationManager.AppSettings("logix_PLC")

        Try
            Me.lbl_DB.Text = "DB: " & dat.GetBusinessId
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Me.cb_SLC.CheckState = CheckState.Checked
        Me.cb_LOGIX.CheckState = CheckState.Checked

        Dim FILE_NAME As String = "C:\OES\OES_LOG\OES_PROGRAM_LOG.txt"
        Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.Hour.ToString.PadLeft(2, "0") & ":" & Now.Minute.ToString.PadLeft(2, "0") & ":" & Now.Second.ToString.PadLeft(2, "0")
        Dim sOutput As String = rDateF & " - START OES"
        Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
        objWriter.WriteLine(sOutput)
        objWriter.Close()

    End Sub

    Public Delegate Sub MsgReceivedDelegateSLC()

    Private Sub peerMsg_Received(ByVal sender As Object, ByVal e As System.EventArgs) Handles peerMsg.Received
        ' ******************************************
        ' * since Received event is generated in a different thread
        ' * than the UI we'll need a delegate
        'Console.WriteLine("Thread Count SLC: {0}", Process.GetCurrentProcess().Threads.Count)
        Try
            Dim args As Logix.MessageEventArgs = e

            ' * event argument parameters
            'args = e
            ' ***************************
            ' * process the event data

            Dim w As WaitCallback = New WaitCallback(AddressOf datSLC.MsgReceivedSLCT)
            ThreadPool.QueueUserWorkItem(w, args)

            'datSLC.MsgReceivedSLCT(args.IPSender, args.ItemName, args.Value, args.Length)
            'Console.WriteLine("Thread Count SLC 2: {0}", Process.GetCurrentProcess().Threads.Count)
        Catch ex As System.Exception
            Console.WriteLine(ex.Message)
            Dim FILE_NAME As String = "C:\OES\OES_LOG\OES_PROGRAM_LOG.txt"
            Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.Hour.ToString.PadLeft(2, "0") & ":" & Now.Minute.ToString.PadLeft(2, "0") & ":" & Now.Second.ToString.PadLeft(2, "0")
            Dim sOutput As String = rDateF & " - PEERMSG SLC ERROR - " & ex.Message
            Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
            objWriter.WriteLine(sOutput)
            objWriter.WriteLine(vbCrLf)
            objWriter.Close()
        End Try
    End Sub
    Public Delegate Sub MsgReceivedDelegateLogix()

    Private Sub peerMsg2_Received(ByVal sender As Object, ByVal e As System.EventArgs) Handles peerMsg2.Received
        ' ******************************************
        ' * since Received event is generated in a different thread
        ' * than the UI we'll need a delegate
        Try
            Dim args2 As Logix.MessageEventArgs
            args2 = e

            Dim w As WaitCallback = New WaitCallback(AddressOf datLogix.MsgReceivedlogixT)
            ThreadPool.QueueUserWorkItem(w, args2)

        Catch ex As Exception
            'MessageBox.Show(ex.Message & "  PeerMsg Logix Error")
            Dim FILE_NAME As String = "C:\OES\OES_LOG\OES_PROGRAM_LOG.txt"
            Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.Hour.ToString.PadLeft(2, "0") & ":" & Now.Minute.ToString.PadLeft(2, "0") & ":" & Now.Second.ToString.PadLeft(2, "0")
            Dim sOutput As String = rDateF & " - PEERMSG LOGIX ERROR - " & ex.Message
            Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
            objWriter.WriteLine(sOutput)
            objWriter.WriteLine(vbCrLf)
            objWriter.Close()
        End Try
    End Sub
    Private Sub btnListen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnListen.Click
        Try
            peerMsg2.IPAddressNIC = ConfigurationManager.AppSettings("Logix_PLC")
            peerMsg2.Protocol = Logix.PeerMessage.MSGPROTOCOL.CIP

            peerMsg.IPAddressNIC = ConfigurationManager.AppSettings("AB_PLC")
            '''''''peerMsg.Protocol = ABLink.PeerMessage.MSGPROTOCOL.CSP
            peerMsg.Protocol = Logix.PeerMessage.MSGPROTOCOL.CSP

            peerMsg.Connections = 120
            peerMsg2.Connections = 120


            If Me.cb_SLC.CheckState = CheckState.Checked Then
                peerMsg.Listen()
            End If
            If Me.cb_LOGIX.CheckState = CheckState.Checked Then
                peerMsg2.Listen()
            End If

            btnListen.Enabled = False
            btnShutdown.Enabled = True
            Me.cb_SLC.Enabled = False
            Me.cb_LOGIX.Enabled = False

            StartTime = Now

            Dim rDate As String = Now.Year & Now.Month.ToString.PadLeft(2, "0") & Now.Day.ToString.PadLeft(2, "0") & Now.Hour.ToString.PadLeft(2, "0") & Now.Minute.ToString.PadLeft(2, "0") & Now.Second.ToString.PadLeft(2, "0")
            Dim sLogFile As String = ConfigurationManager.AppSettings("OES_LOG")

            If File.Exists(sLogFile & ".txt") Then
                File.Copy(sLogFile & ".txt", sLogFile & "_" & rDate & ".txt")
                File.Delete(sLogFile & ".txt")
            End If

            If File.Exists(sLogFile & "_SLC.txt") Then
                File.Copy(sLogFile & "_SLC.txt", sLogFile & "_SLC" & rDate & ".txt")
                File.Delete(sLogFile & "_SLC.txt")
            End If

            Me.Timer1.Start()

            Dim FILE_NAME As String = "C:\OES\OES_LOG\OES_PROGRAM_LOG.txt"
            Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.Hour.ToString.PadLeft(2, "0") & ":" & Now.Minute.ToString.PadLeft(2, "0") & ":" & Now.Second.ToString.PadLeft(2, "0")
            Dim sOutput As String = rDateF & " - LISTEN STARTED"
            Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
            objWriter.WriteLine(sOutput)
            objWriter.Close()

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub btnShutdown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnShutdown.Click

        peerMsg.ShutDown()
        peerMsg2.ShutDown()
        btnListen.Enabled = True
        btnShutdown.Enabled = False
        Me.Timer1.Stop()

        Dim FILE_NAME As String = "C:\OES\OES_LOG\OES_PROGRAM_LOG.txt"
        Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.Hour.ToString.PadLeft(2, "0") & ":" & Now.Minute.ToString.PadLeft(2, "0") & ":" & Now.Second.ToString.PadLeft(2, "0")
        Dim sOutput As String = rDateF & " - LISTEN STOPPED"
        Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
        objWriter.WriteLine(sOutput)
        objWriter.Close()

    End Sub

    Private Sub btnClearList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearList.Click

        listView.Items.Clear()

        Dim FILE_NAME As String = "C:\OES\OES_LOG\OES_PROGRAM_LOG.txt"
        Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.Hour.ToString.PadLeft(2, "0") & ":" & Now.Minute.ToString.PadLeft(2, "0") & ":" & Now.Second.ToString.PadLeft(2, "0")
        Dim sOutput As String = rDateF & " - CLEAR LIST"
        Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
        objWriter.WriteLine(sOutput)
        objWriter.Close()

    End Sub


    Private Sub Form1_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If MessageBox.Show("Are you sure you want to close OES?", "Form Close", MessageBoxButtons.YesNo, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
            e.Cancel = True
        Else
            Me.btnShutdown.PerformClick()
            Dim FILE_NAME As String = "C:\OES\OES_LOG\OES_PROGRAM_LOG.txt"
            Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.Hour.ToString.PadLeft(2, "0") & ":" & Now.Minute.ToString.PadLeft(2, "0") & ":" & Now.Second.ToString.PadLeft(2, "0")
            Dim sOutput As String = rDateF & " - CLOSE OES"
            Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
            objWriter.WriteLine(sOutput)
            objWriter.Close()
        End If
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick

        ElapsedTime = Now().Subtract(StartTime)
        Dim etTme As Integer = CInt(ElapsedTime.TotalMinutes) Mod 60
        Dim stStartStopListen As String = ConfigurationManager.AppSettings("LISTEN_START_STOP_WAIT")
        Dim i As Integer

        If stStartStopListen = "ON" Then
            If etTme > 9 Then
                Dim etX As Integer
                Me.btnShutdown.PerformClick()
                For etX = 1 To 5000
                    Console.WriteLine(etX)
                Next
                Me.btnListen.PerformClick()
                ElapsedTime = Now().Subtract(StartTime)

                Dim FILE_NAME As String = "C:\OES\OES_LOG\OES_PROGRAM_LOG.txt"
                Dim rDateF As String = Now.Year & "-" & Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & " " & Now.Hour.ToString.PadLeft(2, "0") & ":" & Now.Minute.ToString.PadLeft(2, "0") & ":" & Now.Second.ToString.PadLeft(2, "0")
                Dim sOutput As String = rDateF & " SLC Listening: " & peerMsg.IsListening & " - LOGIX Listening: " & peerMsg2.IsListening & " Elapsed Time: " & CInt(ElapsedTime.TotalMinutes) Mod 60
                Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
                objWriter.WriteLine(sOutput)
                objWriter.Close()

            End If
        End If

        Dim num As Integer = ScreenList.Count - 1

        If num > 0 Then
            For nn As Integer = 0 To num
                Dim val As String = ScreenList(nn).ToString()
                msgItem = listView.Items.Insert(0, val)
            Next
            ScreenList.Clear()
        End If


        Me.lbl_TotalTrans.Text = xCount





    End Sub

    Private Sub cb_LogFile_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cb_LogFile.CheckedChanged
        cb_log = cb_LogFile.CheckState
    End Sub

End Class
