Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Data
Imports System.Configuration
Public Class OES_helper
    Private conn As New OleDbConnection
    Public Sub New(ByVal ConnectionString As String)
        ' Dummy parameter is here simply to make the signature different
        conn.ConnectionString = ConnectionString
    End Sub

    Public Function ConnectionString() As String
        Return conn.ConnectionString.ToString
    End Function

    Public Function GetSysdate() As String
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Author:                       Jeff ElDridge
        ' Date:                         2012-03-27
        ' Schema:                       
        ' Table References:             
        ' Description:                  Returns the Sysdate
        ' Revisions:
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim Cmdstr As String = "Select sysdate from dual"

        Dim cmd As New OleDbCommand(Cmdstr, conn)
        Try
            conn.Open()
            Return cmd.ExecuteScalar
        Catch ex As OleDbException
            Throw New Exception(ex.Message)
        Finally
            conn.Close()
        End Try
    End Function

    Public Function GetBusinessId() As String
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Author:                       Jeff ElDridge
        ' Date:                         2012-03-27
        ' Schema:                       
        ' Table References:             
        ' Description:                  Returns the Business id
        ' Revisions:
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim Cmdstr As String = "select business_id from MANITDBA.MANIT_PARAMETERS"

        Dim cmd As New OleDbCommand(Cmdstr, conn)
        Try
            conn.Open()
            Return cmd.ExecuteScalar
        Catch ex As OleDbException
            Throw New Exception(ex.Message)
        Finally
            conn.Close()
        End Try
    End Function

    Public Function GetPlcModelSetupCount(ByVal P_CELL_ID As String, ByVal P_MODEL_ID As String) As Integer
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Author:                       Jeff ElDridge
        ' Date:                         2012-07-18
        ' Schema:                       
        ' Table References:             
        ' Description:                  Returns Count of PLC MODEL SETUP for Model \ Cell
        ' Revisions:
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim Cmdstr As String = "select nvl(count(*),0) from plc_model_setup " & _
                               "where cell_id = '" & P_CELL_ID & "' and model_id = '" & P_MODEL_ID & "'"

        Dim cmd As New OleDbCommand(Cmdstr, conn)
        Try
            conn.Open()
            Return cmd.ExecuteScalar
        Catch ex As OleDbException
            Throw New Exception(ex.Message)
        Finally
            conn.Close()
        End Try
    End Function
End Class
