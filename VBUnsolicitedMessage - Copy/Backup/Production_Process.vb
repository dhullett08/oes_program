Imports System.Data.OleDb

Public Class Production_Process

    Private conn As New OleDbConnection


    Public Sub New(ByVal ConnectionString As String)
        conn.ConnectionString = ConnectionString
    End Sub


    Dim mCELL_ID As String
    Dim mITEM_ID As String
    Dim mCOMPONENT_ID As String
    Dim mPROCESS_INDICATOR As Integer
    Dim mSUCCESS_INDICATOR As Integer
    Dim mPROCESS_FAULT_CODE As Integer
    Dim mVAL_1 As Double
    Dim mVAL_2 As Double
    Dim mVAL_3 As Double
    Dim mVAL_4 As Double
    Dim mVAL_5 As Double
    Dim mVAL_6 As Double
    Dim mVAL_7 As Double
    Dim mVAL_8 As Double
    Dim mVAL_9 As Double
    Dim mVAL_10 As Double
    Dim mVAL_11 As Double
    Dim mVAL_12 As Double
    Dim mVAL_13 As Double
    Dim mVAL_14 As Double
    Dim mVAL_15 As Double
    Dim mVAL_16 As Double
    Dim mVAL_17 As Double
    Dim mVAL_18 As Double
    Dim mVAL_19 As Double
    Dim mVAL_20 As Double
    Dim mVAL_21 As Double
    Dim mVAL_22 As Double
    Dim mVAL_23 As Double
    Dim mVAL_24 As Double
    Dim mVAL_25 As Double
    Dim mVAL_26 As Double
    Dim mVAL_27 As Double
    Dim mVAL_28 As Double
    Dim mINWORD0 As Double
    Dim mINWORD1 As Double
    Dim mINWORD2 As Double
    Dim mINWORD3 As Double
    Dim mINWORD4 As Double
    Dim mINWORD5 As Double
    Dim mINWORD6 As Double
    Dim mINWORD7 As Double
    Dim mINWORD8 As Double
    Dim mINWORD9 As Double
    Dim mINWORD10 As Double
    Dim mINWORD11 As Double
    Dim mOUTWORD18 As Double
    Dim mOUTWORD19 As Double
    Dim mOUTWORD20 As Double
    Dim mSTATUS_CODE As Double
    Dim mOUTWORD22 As Double
    Dim mOUTWORD23 As Double
    Dim mOUTWORD24 As Double
    Dim mOUTWORD25 As Double
    Dim mOUTWORD26 As Double
    Dim mOUTWORD27 As Double
    Dim mOUTWORD28 As Double
    Dim mOUTWORD29 As Double
    Dim mOUTWORD30 As Double
    Dim mOUTWORD31 As Double
    Dim mOUTWORD32 As Double
    Dim mOUTWORD33 As Double
    Dim mOUTWORD34 As Double
    Dim mOUTWORD35 As Double
    Dim mOUTWORD36 As Double
    Dim mOUTWORD37 As Double
    Dim mOUTWORD38 As Double
    Dim mOUTWORD39 As Double
    Dim mOUTWORD40 As Double
    Dim mOUTWORD41 As Double
    Dim mOUTWORD42 As Double
    Dim mOUTWORD43 As Double
    Dim mOUTWORD44 As Double
    Dim mOUTWORD45 As Double
    Dim mOUTWORD46 As Double
    Dim mOUTWORD47 As Double
    Dim mOUTWORD48 As Double
    Dim mOUTWORD49 As Double
    Dim mOUTWORD0 As Double
    Dim mOUTWORD1 As Double
    Dim mOUTWORD2 As Double
    Dim mOUTWORD3 As Double
    Dim mOUTWORD4 As Double
    Dim mOUTWORD5 As Double
    Dim mOUTWORD6 As Double
    Dim mOUTWORD7 As Double
    Dim mOUTWORD8 As Double
    Dim mOUTWORD9 As Double
    Dim mOUTWORD10 As Double
    Dim mOUTWORD11 As Double
    Dim mOUTWORD13 As Double

    Public Property CELL_ID()
        Get
            Return mCELL_ID
        End Get
        Set(ByVal value)
            mCELL_ID = value
        End Set
    End Property

    Public Property ITEM_ID()
        Get
            Return mITEM_ID
        End Get
        Set(ByVal value)
            mITEM_ID = value
        End Set
    End Property
    Public Property COMPONENT_ID()
        Get
            Return mCOMPONENT_ID
        End Get
        Set(ByVal value)
            mCOMPONENT_ID = value
        End Set
    End Property
    Public Property PROCESS_INDICATOR()
        Get
            Return mPROCESS_INDICATOR
        End Get
        Set(ByVal value)
            mPROCESS_INDICATOR = value
        End Set
    End Property
    Public Property SUCCESS_INDICATOR()
        Get
            Return mSUCCESS_INDICATOR
        End Get
        Set(ByVal value)
            mSUCCESS_INDICATOR = value
        End Set
    End Property
    Public Property PROCESS_FAULT_CODE()
        Get
            Return mPROCESS_FAULT_CODE
        End Get
        Set(ByVal value)
            mPROCESS_FAULT_CODE = value
        End Set
    End Property
    Public Property VAL_1()
        Get
            Return mVAL_1
        End Get
        Set(ByVal value)
            mVAL_1 = value
        End Set
    End Property
    Public Property VAL_2()
        Get
            Return mVAL_2
        End Get
        Set(ByVal value)
            mVAL_2 = value
        End Set
    End Property
    Public Property VAL_3()
        Get
            Return mVAL_3
        End Get
        Set(ByVal value)
            mVAL_3 = value
        End Set
    End Property
    Public Property VAL_4()
        Get
            Return mVAL_4
        End Get
        Set(ByVal value)
            mVAL_4 = value
        End Set
    End Property
    Public Property VAL_5()
        Get
            Return mVAL_5
        End Get
        Set(ByVal value)
            mVAL_5 = value
        End Set
    End Property
    Public Property VAL_6()
        Get
            Return mVAL_6
        End Get
        Set(ByVal value)
            mVAL_6 = value
        End Set
    End Property
    Public Property VAL_7()
        Get
            Return mVAL_7
        End Get
        Set(ByVal value)
            mVAL_7 = value
        End Set
    End Property
    Public Property VAL_8()
        Get
            Return mVAL_8
        End Get
        Set(ByVal value)
            mVAL_8 = value
        End Set
    End Property
    Public Property VAL_9()
        Get
            Return mVAL_9
        End Get
        Set(ByVal value)
            mVAL_9 = value
        End Set
    End Property
    Public Property VAL_10()
        Get
            Return mVAL_10
        End Get
        Set(ByVal value)
            mVAL_10 = value
        End Set
    End Property
    Public Property VAL_11()
        Get
            Return mVAL_11
        End Get
        Set(ByVal value)
            mVAL_11 = value
        End Set
    End Property
    Public Property VAL_12()
        Get
            Return mVAL_12
        End Get
        Set(ByVal value)
            mVAL_12 = value
        End Set
    End Property
    Public Property VAL_13()
        Get
            Return mVAL_13
        End Get
        Set(ByVal value)
            mVAL_13 = value
        End Set
    End Property
    Public Property VAL_14()
        Get
            Return mVAL_14
        End Get
        Set(ByVal value)
            mVAL_14 = value
        End Set
    End Property
    Public Property VAL_15()
        Get
            Return mVAL_15
        End Get
        Set(ByVal value)
            mVAL_15 = value
        End Set
    End Property
    Public Property VAL_16()
        Get
            Return mVAL_16
        End Get
        Set(ByVal value)
            mVAL_16 = value
        End Set
    End Property
    Public Property VAL_17()
        Get
            Return mVAL_17
        End Get
        Set(ByVal value)
            mVAL_17 = value
        End Set
    End Property
    Public Property VAL_18()
        Get
            Return mVAL_18
        End Get
        Set(ByVal value)
            mVAL_18 = value
        End Set
    End Property
    Public Property VAL_19()
        Get
            Return mVAL_19
        End Get
        Set(ByVal value)
            mVAL_19 = value
        End Set
    End Property
    Public Property VAL_20()
        Get
            Return mVAL_20
        End Get
        Set(ByVal value)
            mVAL_20 = value
        End Set
    End Property
    Public Property VAL_21()
        Get
            Return mVAL_21
        End Get
        Set(ByVal value)
            mVAL_21 = value
        End Set
    End Property
    Public Property VAL_22()
        Get
            Return mVAL_22
        End Get
        Set(ByVal value)
            mVAL_22 = value
        End Set
    End Property
    Public Property VAL_23()
        Get
            Return mVAL_23
        End Get
        Set(ByVal value)
            mVAL_23 = value
        End Set
    End Property
    Public Property VAL_24()
        Get
            Return mVAL_24
        End Get
        Set(ByVal value)
            mVAL_24 = value
        End Set
    End Property
    Public Property VAL_25()
        Get
            Return mVAL_25
        End Get
        Set(ByVal value)
            mVAL_25 = value
        End Set
    End Property
    Public Property VAL_26()
        Get
            Return mVAL_26
        End Get
        Set(ByVal value)
            mVAL_26 = value
        End Set
    End Property
    Public Property VAL_27()
        Get
            Return mVAL_27
        End Get
        Set(ByVal value)
            mVAL_27 = value
        End Set
    End Property
    Public Property VAL_28()
        Get
            Return mVAL_28
        End Get
        Set(ByVal value)
            mVAL_28 = value
        End Set
    End Property
    Public Property INWORD0()
        Get
            Return mINWORD0
        End Get
        Set(ByVal value)
            mINWORD0 = value
        End Set
    End Property
    Public Property INWORD1()
        Get
            Return mINWORD1
        End Get
        Set(ByVal value)
            mINWORD1 = value
        End Set
    End Property
    Public Property INWORD2()
        Get
            Return mINWORD2
        End Get
        Set(ByVal value)
            mINWORD2 = value
        End Set
    End Property

    Public Property INWORD3()
        Get
            Return mINWORD3
        End Get
        Set(ByVal value)
            mINWORD3 = value
        End Set
    End Property
    Public Property INWORD4()
        Get
            Return mINWORD4
        End Get
        Set(ByVal value)
            mINWORD4 = value
        End Set
    End Property
    Public Property INWORD5()
        Get
            Return mINWORD5
        End Get
        Set(ByVal value)
            mINWORD5 = value
        End Set
    End Property
    Public Property INWORD6()
        Get
            Return mINWORD6
        End Get
        Set(ByVal value)
            mINWORD6 = value
        End Set
    End Property
    Public Property INWORD7()
        Get
            Return mINWORD7
        End Get
        Set(ByVal value)
            mINWORD7 = value
        End Set
    End Property
    Public Property INWORD8()
        Get
            Return mINWORD8
        End Get
        Set(ByVal value)
            mINWORD8 = value
        End Set
    End Property
    Public Property INWORD9()
        Get
            Return mINWORD9
        End Get
        Set(ByVal value)
            mINWORD9 = value
        End Set
    End Property
    Public Property INWORD10()
        Get
            Return mINWORD10
        End Get
        Set(ByVal value)
            mINWORD10 = value
        End Set
    End Property
    Public Property INWORD11()
        Get
            Return mINWORD11
        End Get
        Set(ByVal value)
            mINWORD11 = value
        End Set
    End Property
    Public Property OUTWORD18()
        Get
            Return mOUTWORD18
        End Get
        Set(ByVal value)
            mOUTWORD18 = value
        End Set
    End Property
    Public Property OUTWORD19()
        Get
            Return mOUTWORD19
        End Get
        Set(ByVal value)
            mOUTWORD19 = value
        End Set
    End Property
    Public Property OUTWORD20()
        Get
            Return mOUTWORD20
        End Get
        Set(ByVal value)
            mOUTWORD20 = value
        End Set
    End Property
    Public Property STATUS_CODE()
        Get
            Return mSTATUS_CODE
        End Get
        Set(ByVal value)
            mSTATUS_CODE = value
        End Set
    End Property
    Public Property OUTWORD22()
        Get
            Return mOUTWORD22
        End Get
        Set(ByVal value)
            mOUTWORD22 = value
        End Set
    End Property
    Public Property OUTWORD23()
        Get
            Return mOUTWORD23
        End Get
        Set(ByVal value)
            mOUTWORD23 = value
        End Set
    End Property
    Public Property OUTWORD24()
        Get
            Return mOUTWORD24
        End Get
        Set(ByVal value)
            mOUTWORD24 = value
        End Set
    End Property
    Public Property OUTWORD25()
        Get
            Return mOUTWORD25
        End Get
        Set(ByVal value)
            mOUTWORD25 = value
        End Set
    End Property
    Public Property OUTWORD26()
        Get
            Return mOUTWORD26
        End Get
        Set(ByVal value)
            mOUTWORD26 = value
        End Set
    End Property
    Public Property OUTWORD27()
        Get
            Return mOUTWORD27
        End Get
        Set(ByVal value)
            mOUTWORD27 = value
        End Set
    End Property
    Public Property OUTWORD28()
        Get
            Return mOUTWORD28
        End Get
        Set(ByVal value)
            mOUTWORD28 = value
        End Set
    End Property
    Public Property OUTWORD29()
        Get
            Return mOUTWORD29
        End Get
        Set(ByVal value)
            mOUTWORD29 = value
        End Set
    End Property
    Public Property OUTWORD30()
        Get
            Return mOUTWORD30
        End Get
        Set(ByVal value)
            mOUTWORD30 = value
        End Set
    End Property
    Public Property OUTWORD31()
        Get
            Return mOUTWORD31
        End Get
        Set(ByVal value)
            mOUTWORD31 = value
        End Set
    End Property
    Public Property OUTWORD32()
        Get
            Return mOUTWORD32
        End Get
        Set(ByVal value)
            mOUTWORD32 = value
        End Set
    End Property
    Public Property OUTWORD33()
        Get
            Return mOUTWORD33
        End Get
        Set(ByVal value)
            mOUTWORD33 = value
        End Set
    End Property
    Public Property OUTWORD34()
        Get
            Return mOUTWORD34
        End Get
        Set(ByVal value)
            mOUTWORD34 = value
        End Set
    End Property
    Public Property OUTWORD35()
        Get
            Return mOUTWORD35
        End Get
        Set(ByVal value)
            mOUTWORD35 = value
        End Set
    End Property
    Public Property OUTWORD36()
        Get
            Return mOUTWORD36
        End Get
        Set(ByVal value)
            mOUTWORD36 = value
        End Set
    End Property
    Public Property OUTWORD37()
        Get
            Return mOUTWORD37
        End Get
        Set(ByVal value)
            mOUTWORD37 = value
        End Set
    End Property
    Public Property OUTWORD38()
        Get
            Return mOUTWORD38
        End Get
        Set(ByVal value)
            mOUTWORD38 = value
        End Set
    End Property
    Public Property OUTWORD39()
        Get
            Return mOUTWORD39
        End Get
        Set(ByVal value)
            mOUTWORD39 = value
        End Set
    End Property
    Public Property OUTWORD40()
        Get
            Return mOUTWORD40
        End Get
        Set(ByVal value)
            mOUTWORD40 = value
        End Set
    End Property
    Public Property OUTWORD41()
        Get
            Return mOUTWORD41
        End Get
        Set(ByVal value)
            mOUTWORD41 = value
        End Set
    End Property
    Public Property OUTWORD42()
        Get
            Return mOUTWORD42
        End Get
        Set(ByVal value)
            mOUTWORD42 = value
        End Set
    End Property
    Public Property OUTWORD43()
        Get
            Return mOUTWORD43
        End Get
        Set(ByVal value)
            mOUTWORD43 = value
        End Set
    End Property
    Public Property OUTWORD44()
        Get
            Return mOUTWORD44
        End Get
        Set(ByVal value)
            mOUTWORD44 = value
        End Set
    End Property
    Public Property OUTWORD45()
        Get
            Return mOUTWORD45
        End Get
        Set(ByVal value)
            mOUTWORD45 = value
        End Set
    End Property
    Public Property OUTWORD46()
        Get
            Return mOUTWORD46
        End Get
        Set(ByVal value)
            mOUTWORD46 = value
        End Set
    End Property
    Public Property OUTWORD47()
        Get
            Return mOUTWORD47
        End Get
        Set(ByVal value)
            mOUTWORD47 = value
        End Set
    End Property
    Public Property OUTWORD48()
        Get
            Return mOUTWORD48
        End Get
        Set(ByVal value)
            mOUTWORD48 = value
        End Set
    End Property
    Public Property OUTWORD49()
        Get
            Return mOUTWORD49
        End Get
        Set(ByVal value)
            mOUTWORD49 = value
        End Set
    End Property
    Public Property OUTWORD0()
        Get
            Return mOUTWORD0
        End Get
        Set(ByVal value)
            mOUTWORD0 = value
        End Set
    End Property
    Public Property OUTWORD1()
        Get
            Return mOUTWORD1
        End Get
        Set(ByVal value)
            mOUTWORD1 = value
        End Set
    End Property
    Public Property OUTWORD2()
        Get
            Return mOUTWORD2
        End Get
        Set(ByVal value)
            mOUTWORD2 = value
        End Set
    End Property
    Public Property OUTWORD3()
        Get
            Return mOUTWORD3
        End Get
        Set(ByVal value)
            mOUTWORD3 = value
        End Set
    End Property
    Public Property OUTWORD4()
        Get
            Return mOUTWORD4
        End Get
        Set(ByVal value)
            mOUTWORD4 = value
        End Set
    End Property
    Public Property OUTWORD5()
        Get
            Return mOUTWORD5
        End Get
        Set(ByVal value)
            mOUTWORD5 = value
        End Set
    End Property
    Public Property OUTWORD6()
        Get
            Return mOUTWORD6
        End Get
        Set(ByVal value)
            mOUTWORD6 = value
        End Set
    End Property
    Public Property OUTWORD7()
        Get
            Return mOUTWORD7
        End Get
        Set(ByVal value)
            mOUTWORD7 = value
        End Set
    End Property
    Public Property OUTWORD8()
        Get
            Return mOUTWORD8
        End Get
        Set(ByVal value)
            mOUTWORD8 = value
        End Set
    End Property
    Public Property OUTWORD9()
        Get
            Return mOUTWORD9
        End Get
        Set(ByVal value)
            mOUTWORD9 = value
        End Set
    End Property
    Public Property OUTWORD10()
        Get
            Return mOUTWORD10
        End Get
        Set(ByVal value)
            mOUTWORD10 = value
        End Set
    End Property

    Public Property OUTWORD11()
        Get
            Return mOUTWORD11
        End Get
        Set(ByVal value)
            mOUTWORD11 = value
        End Set
    End Property

    Public Property OUTWORD13()
        Get
            Return mOUTWORD13
        End Get
        Set(ByVal value)
            mOUTWORD13 = value
        End Set
    End Property


    Public Function CallProductionProcess(ByVal ProdObject As Production_Process) As String
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Author:                       Jeff Judkins
        ' Date:                         03-09-2007
        ' Schema:                       
        ' Table References:                   
        ' Description:                  
        ' Revisions:
        ' Notes:                        
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Dim cmd As New OleDbCommand("arc.oes_prod_50", conn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim piCELL_ID As New OleDbParameter("CELL_ID", OleDbType.VarChar, 40)
        piCELL_ID.Direction = ParameterDirection.Input
        cmd.Parameters.Add(piCELL_ID).Value = ProdObject.CELL_ID

        Dim piITEM_ID As New OleDbParameter("ITEM_ID", OleDbType.VarChar, 40)
        piITEM_ID.Direction = ParameterDirection.Input
        cmd.Parameters.Add(piITEM_ID).Value = ProdObject.ITEM_ID

        Dim piCOMPONENT_ID As New OleDbParameter("COMPONENT_ID", OleDbType.VarChar, 40)
        piCOMPONENT_ID.Direction = ParameterDirection.Input
        cmd.Parameters.Add(piCOMPONENT_ID).Value = ProdObject.COMPONENT_ID

        Dim piPROCESS_INDICATOR As New OleDbParameter("PROCESS_INDICATOR", OleDbType.VarNumeric)
        piPROCESS_INDICATOR.Direction = ParameterDirection.Input
        cmd.Parameters.Add(piPROCESS_INDICATOR).Value = ProdObject.PROCESS_INDICATOR

        Dim piSUCCESS_INDICATOR As New OleDbParameter("SUCCESS_INDICATOR", OleDbType.VarNumeric)
        piSUCCESS_INDICATOR.Direction = ParameterDirection.Input
        cmd.Parameters.Add(piSUCCESS_INDICATOR).Value = ProdObject.SUCCESS_INDICATOR

        Dim piPROCESS_FAULT_CODE As New OleDbParameter("PROCESS_FAULT_CODE", OleDbType.VarNumeric)
        piPROCESS_FAULT_CODE.Direction = ParameterDirection.Input
        cmd.Parameters.Add(piPROCESS_FAULT_CODE).Value = ProdObject.PROCESS_FAULT_CODE

        Dim piVAL_1 As New OleDbParameter("VAL_1", OleDbType.VarNumeric)
        piVAL_1.Direction = ParameterDirection.Input
        cmd.Parameters.Add(piVAL_1).Value = ProdObject.VAL_1

        Dim piVAL_2 As New OleDbParameter("VAL_2", OleDbType.VarNumeric)
        piVAL_2.Direction = ParameterDirection.Input
        cmd.Parameters.Add(piVAL_2).Value = ProdObject.VAL_2

        Dim piVAL_3 As New OleDbParameter("VAL_3", OleDbType.VarNumeric)
        piVAL_3.Direction = ParameterDirection.Input
        cmd.Parameters.Add(piVAL_3).Value = ProdObject.VAL_3

        Dim piVAL_4 As New OleDbParameter("VAL_4", OleDbType.VarNumeric)
        piVAL_4.Direction = ParameterDirection.Input
        cmd.Parameters.Add(piVAL_4).Value = ProdObject.VAL_4

        Dim piVAL_5 As New OleDbParameter("VAL_5", OleDbType.VarNumeric)
        piVAL_5.Direction = ParameterDirection.Input
        cmd.Parameters.Add(piVAL_5).Value = ProdObject.VAL_5

        Dim piVAL_6 As New OleDbParameter("VAL_6", OleDbType.VarNumeric)
        piVAL_6.Direction = ParameterDirection.Input
        cmd.Parameters.Add(piVAL_6).Value = ProdObject.VAL_6

        Dim piVAL_7 As New OleDbParameter("VAL_7", OleDbType.VarNumeric)
        piVAL_7.Direction = ParameterDirection.Input
        cmd.Parameters.Add(piVAL_7).Value = ProdObject.VAL_7

        Dim piVAL_8 As New OleDbParameter("VAL_8", OleDbType.VarNumeric)
        piVAL_8.Direction = ParameterDirection.Input
        cmd.Parameters.Add(piVAL_8).Value = ProdObject.VAL_8

        Dim piVAL_9 As New OleDbParameter("VAL_9", OleDbType.VarNumeric)
        piVAL_9.Direction = ParameterDirection.Input
        cmd.Parameters.Add(piVAL_9).Value = ProdObject.VAL_9

        Dim piVAL_10 As New OleDbParameter("VAL_10", OleDbType.VarNumeric)
        piVAL_10.Direction = ParameterDirection.Input
        cmd.Parameters.Add(piVAL_10).Value = ProdObject.VAL_10

        Dim piVAL_11 As New OleDbParameter("VAL_11", OleDbType.VarNumeric)
        piVAL_11.Direction = ParameterDirection.Input
        cmd.Parameters.Add(piVAL_11).Value = ProdObject.VAL_11

        Dim piVAL_12 As New OleDbParameter("VAL_12", OleDbType.VarNumeric)
        piVAL_12.Direction = ParameterDirection.Input
        cmd.Parameters.Add(piVAL_12).Value = ProdObject.VAL_12

        Dim piVAL_13 As New OleDbParameter("VAL_13", OleDbType.VarNumeric)
        piVAL_13.Direction = ParameterDirection.Input
        cmd.Parameters.Add(piVAL_13).Value = ProdObject.VAL_13

        Dim piVAL_14 As New OleDbParameter("VAL_14", OleDbType.VarNumeric)
        piVAL_14.Direction = ParameterDirection.Input
        cmd.Parameters.Add(piVAL_14).Value = ProdObject.VAL_14

        Dim piVAL_15 As New OleDbParameter("VAL_15", OleDbType.VarNumeric)
        piVAL_15.Direction = ParameterDirection.Input
        cmd.Parameters.Add(piVAL_15).Value = ProdObject.VAL_15

        Dim piVAL_16 As New OleDbParameter("VAL_16", OleDbType.VarNumeric)
        piVAL_16.Direction = ParameterDirection.Input
        cmd.Parameters.Add(piVAL_16).Value = ProdObject.VAL_16

        Dim piVAL_17 As New OleDbParameter("VAL_17", OleDbType.VarNumeric)
        piVAL_17.Direction = ParameterDirection.Input
        cmd.Parameters.Add(piVAL_17).Value = ProdObject.VAL_17

        Dim piVAL_18 As New OleDbParameter("VAL_18", OleDbType.VarNumeric)
        piVAL_18.Direction = ParameterDirection.Input
        cmd.Parameters.Add(piVAL_18).Value = ProdObject.VAL_18

        Dim piVAL_19 As New OleDbParameter("VAL_19", OleDbType.VarNumeric)
        piVAL_19.Direction = ParameterDirection.Input
        cmd.Parameters.Add(piVAL_19).Value = ProdObject.VAL_19

        Dim piVAL_20 As New OleDbParameter("VAL_20", OleDbType.VarNumeric)
        piVAL_20.Direction = ParameterDirection.Input
        cmd.Parameters.Add(piVAL_20).Value = ProdObject.VAL_20

        Dim piVAL_21 As New OleDbParameter("VAL_21", OleDbType.VarNumeric)
        piVAL_21.Direction = ParameterDirection.Input
        cmd.Parameters.Add(piVAL_21).Value = ProdObject.VAL_21

        Dim piVAL_22 As New OleDbParameter("VAL_22", OleDbType.VarNumeric)
        piVAL_22.Direction = ParameterDirection.Input
        cmd.Parameters.Add(piVAL_22).Value = ProdObject.VAL_22

        Dim piVAL_23 As New OleDbParameter("VAL_23", OleDbType.VarNumeric)
        piVAL_23.Direction = ParameterDirection.Input
        cmd.Parameters.Add(piVAL_23).Value = ProdObject.VAL_23

        Dim piVAL_24 As New OleDbParameter("VAL_24", OleDbType.VarNumeric)
        piVAL_24.Direction = ParameterDirection.Input
        cmd.Parameters.Add(piVAL_24).Value = ProdObject.VAL_24

        Dim piVAL_25 As New OleDbParameter("VAL_25", OleDbType.VarNumeric)
        piVAL_25.Direction = ParameterDirection.Input
        cmd.Parameters.Add(piVAL_25).Value = ProdObject.VAL_25

        Dim piVAL_26 As New OleDbParameter("VAL_26", OleDbType.VarNumeric)
        piVAL_26.Direction = ParameterDirection.Input
        cmd.Parameters.Add(piVAL_26).Value = ProdObject.VAL_26

        Dim piVAL_27 As New OleDbParameter("VAL_27", OleDbType.VarNumeric)
        piVAL_27.Direction = ParameterDirection.Input
        cmd.Parameters.Add(piVAL_27).Value = ProdObject.VAL_27

        Dim piVAL_28 As New OleDbParameter("VAL_28", OleDbType.VarNumeric)
        piVAL_28.Direction = ParameterDirection.Input
        cmd.Parameters.Add(piVAL_28).Value = ProdObject.VAL_28

        Dim piINWORD0 As New OleDbParameter("INWORD0", OleDbType.VarNumeric)
        piINWORD0.Direction = ParameterDirection.Input
        cmd.Parameters.Add(piINWORD0).Value = ProdObject.INWORD0

        Dim piINWORD1 As New OleDbParameter("INWORD1", OleDbType.VarNumeric)
        piINWORD1.Direction = ParameterDirection.Input
        cmd.Parameters.Add(piINWORD1).Value = ProdObject.INWORD1

        Dim piINWORD2 As New OleDbParameter("INWORD2", OleDbType.VarNumeric)
        piINWORD2.Direction = ParameterDirection.Input
        cmd.Parameters.Add(piINWORD2).Value = ProdObject.INWORD2

        Dim piINWORD3 As New OleDbParameter("INWORD3", OleDbType.VarNumeric)
        piINWORD3.Direction = ParameterDirection.Input
        cmd.Parameters.Add(piINWORD3).Value = ProdObject.INWORD3

        Dim piINWORD4 As New OleDbParameter("INWORD4", OleDbType.VarNumeric)
        piINWORD4.Direction = ParameterDirection.Input
        cmd.Parameters.Add(piINWORD4).Value = ProdObject.INWORD4

        Dim piINWORD5 As New OleDbParameter("INWORD5", OleDbType.VarNumeric)
        piINWORD5.Direction = ParameterDirection.Input
        cmd.Parameters.Add(piINWORD5).Value = ProdObject.INWORD5

        Dim piINWORD6 As New OleDbParameter("INWORD6", OleDbType.VarNumeric)
        piINWORD6.Direction = ParameterDirection.Input
        cmd.Parameters.Add(piINWORD6).Value = ProdObject.INWORD6

        Dim piINWORD7 As New OleDbParameter("INWORD7", OleDbType.VarNumeric)
        piINWORD7.Direction = ParameterDirection.Input
        cmd.Parameters.Add(piINWORD7).Value = ProdObject.INWORD7

        Dim piINWORD8 As New OleDbParameter("INWORD8", OleDbType.VarNumeric)
        piINWORD8.Direction = ParameterDirection.Input
        cmd.Parameters.Add(piINWORD8).Value = ProdObject.INWORD8

        Dim piINWORD9 As New OleDbParameter("INWORD9", OleDbType.VarNumeric)
        piINWORD9.Direction = ParameterDirection.Input
        cmd.Parameters.Add(piINWORD9).Value = ProdObject.INWORD9

        Dim piINWORD10 As New OleDbParameter("INWORD10", OleDbType.VarNumeric)
        piINWORD10.Direction = ParameterDirection.Input
        cmd.Parameters.Add(piINWORD10).Value = ProdObject.INWORD10

        Dim piINWORD11 As New OleDbParameter("INWORD11", OleDbType.VarNumeric)
        piINWORD11.Direction = ParameterDirection.Input
        cmd.Parameters.Add(piINWORD11).Value = ProdObject.INWORD11

        ' OUTWORDS

        Dim poOUTWORD18 As New OleDbParameter("OUTWORD18", OleDbType.VarNumeric)
        poOUTWORD18.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(poOUTWORD18)

        Dim poOUTWORD19 As New OleDbParameter("OUTWORD19", OleDbType.VarNumeric)
        poOUTWORD19.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(poOUTWORD19)

        Dim poOUTWORD20 As New OleDbParameter("OUTWORD20", OleDbType.VarNumeric)
        poOUTWORD20.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(poOUTWORD20)

        Dim poSTATUS_CODE As New OleDbParameter("STATUS_CODE", OleDbType.VarNumeric)
        poSTATUS_CODE.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(poSTATUS_CODE)

        Dim poOUTWORD22 As New OleDbParameter("OUTWORD22", OleDbType.VarNumeric)
        poOUTWORD22.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(poOUTWORD22)

        Dim poOUTWORD23 As New OleDbParameter("OUTWORD23", OleDbType.VarNumeric)
        poOUTWORD23.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(poOUTWORD23)

        Dim poOUTWORD24 As New OleDbParameter("OUTWORD24", OleDbType.VarNumeric)
        poOUTWORD24.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(poOUTWORD24)

        Dim poOUTWORD25 As New OleDbParameter("OUTWORD25", OleDbType.VarNumeric)
        poOUTWORD25.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(poOUTWORD25)

        Dim poOUTWORD26 As New OleDbParameter("OUTWORD26", OleDbType.VarNumeric)
        poOUTWORD26.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(poOUTWORD26)

        Dim poOUTWORD27 As New OleDbParameter("OUTWORD27", OleDbType.VarNumeric)
        poOUTWORD27.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(poOUTWORD27)

        Dim poOUTWORD28 As New OleDbParameter("OUTWORD28", OleDbType.VarNumeric)
        poOUTWORD28.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(poOUTWORD28)

        Dim poOUTWORD29 As New OleDbParameter("OUTWORD29", OleDbType.VarNumeric)
        poOUTWORD29.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(poOUTWORD29)

        Dim poOUTWORD30 As New OleDbParameter("OUTWORD30", OleDbType.VarNumeric)
        poOUTWORD30.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(poOUTWORD30)

        Dim poOUTWORD31 As New OleDbParameter("OUTWORD31", OleDbType.VarNumeric)
        poOUTWORD31.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(poOUTWORD31)

        Dim poOUTWORD32 As New OleDbParameter("OUTWORD32", OleDbType.VarNumeric)
        poOUTWORD32.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(poOUTWORD32)

        Dim poOUTWORD33 As New OleDbParameter("OUTWORD33", OleDbType.VarNumeric)
        poOUTWORD33.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(poOUTWORD33)

        Dim poOUTWORD34 As New OleDbParameter("OUTWORD34", OleDbType.VarNumeric)
        poOUTWORD34.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(poOUTWORD34)

        Dim poOUTWORD35 As New OleDbParameter("OUTWORD35", OleDbType.VarNumeric)
        poOUTWORD35.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(poOUTWORD35)

        Dim poOUTWORD36 As New OleDbParameter("OUTWORD36", OleDbType.VarNumeric)
        poOUTWORD36.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(poOUTWORD36)

        Dim poOUTWORD37 As New OleDbParameter("OUTWORD37", OleDbType.VarNumeric)
        poOUTWORD37.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(poOUTWORD37)

        Dim poOUTWORD38 As New OleDbParameter("OUTWORD38", OleDbType.VarNumeric)
        poOUTWORD38.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(poOUTWORD38)

        Dim poOUTWORD39 As New OleDbParameter("OUTWORD39", OleDbType.VarNumeric)
        poOUTWORD39.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(poOUTWORD39)

        Dim poOUTWORD40 As New OleDbParameter("OUTWORD40", OleDbType.VarNumeric)
        poOUTWORD40.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(poOUTWORD40)

        Dim poOUTWORD41 As New OleDbParameter("OUTWORD41", OleDbType.VarNumeric)
        poOUTWORD41.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(poOUTWORD41)

        Dim poOUTWORD42 As New OleDbParameter("OUTWORD42", OleDbType.VarNumeric)
        poOUTWORD42.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(poOUTWORD42)

        Dim poOUTWORD43 As New OleDbParameter("OUTWORD43", OleDbType.VarNumeric)
        poOUTWORD43.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(poOUTWORD43)

        Dim poOUTWORD44 As New OleDbParameter("OUTWORD44", OleDbType.VarNumeric)
        poOUTWORD44.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(poOUTWORD44)

        Dim poOUTWORD45 As New OleDbParameter("OUTWORD45", OleDbType.VarNumeric)
        poOUTWORD45.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(poOUTWORD45)

        Dim poOUTWORD46 As New OleDbParameter("OUTWORD46", OleDbType.VarNumeric)
        poOUTWORD46.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(poOUTWORD46)

        Dim poOUTWORD47 As New OleDbParameter("OUTWORD47", OleDbType.VarNumeric)
        poOUTWORD47.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(poOUTWORD47)

        Dim poOUTWORD48 As New OleDbParameter("OUTWORD48", OleDbType.VarNumeric)
        poOUTWORD48.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(poOUTWORD48)

        Dim poOUTWORD49 As New OleDbParameter("OUTWORD49", OleDbType.VarNumeric)
        poOUTWORD49.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(poOUTWORD49)

        Dim poOUTWORD0 As New OleDbParameter("OUTWORD0", OleDbType.VarNumeric)
        poOUTWORD0.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(poOUTWORD0)

        Dim poOUTWORD1 As New OleDbParameter("OUTWORD1", OleDbType.VarNumeric)
        poOUTWORD1.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(poOUTWORD1)

        Dim poOUTWORD2 As New OleDbParameter("OUTWORD2", OleDbType.VarNumeric)
        poOUTWORD2.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(poOUTWORD2)

        Dim poOUTWORD3 As New OleDbParameter("OUTWORD3", OleDbType.VarNumeric)
        poOUTWORD3.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(poOUTWORD3)

        Dim poOUTWORD4 As New OleDbParameter("OUTWORD4", OleDbType.VarNumeric)
        poOUTWORD4.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(poOUTWORD4)

        Dim poOUTWORD5 As New OleDbParameter("OUTWORD5", OleDbType.VarNumeric)
        poOUTWORD5.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(poOUTWORD5)

        Dim poOUTWORD6 As New OleDbParameter("OUTWORD6", OleDbType.VarNumeric)
        poOUTWORD6.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(poOUTWORD6)

        Dim poOUTWORD7 As New OleDbParameter("OUTWORD7", OleDbType.VarNumeric)
        poOUTWORD7.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(poOUTWORD7)

        Dim poOUTWORD8 As New OleDbParameter("OUTWORD8", OleDbType.VarNumeric)
        poOUTWORD8.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(poOUTWORD8)

        Dim poOUTWORD9 As New OleDbParameter("OUTWORD9", OleDbType.VarNumeric)
        poOUTWORD9.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(poOUTWORD9)

        Dim poOUTWORD10 As New OleDbParameter("OUTWORD10", OleDbType.VarNumeric)
        poOUTWORD10.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(poOUTWORD10)

        Dim poOUTWORD11 As New OleDbParameter("OUTWORD11", OleDbType.VarNumeric)
        poOUTWORD11.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(poOUTWORD11)

        Dim poOUTWORD13 As New OleDbParameter("OUTWORD13", OleDbType.VarNumeric)
        poOUTWORD13.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(poOUTWORD13)


        Try
            conn.Open()
            cmd.ExecuteNonQuery()
            Dim ReturnString As String
            ReturnString = poOUTWORD0.Value.ToString & "," & poOUTWORD1.Value.ToString & "," & poOUTWORD2.Value.ToString & "," & poOUTWORD3.Value.ToString & "," & poOUTWORD4.Value.ToString & "," & poOUTWORD5.Value.ToString & "," & poOUTWORD6.Value.ToString & "," & poOUTWORD7.Value.ToString & "," & poOUTWORD8.Value.ToString & "," & poOUTWORD9.Value.ToString & "," & poOUTWORD10.Value.ToString & "," & poOUTWORD11.Value.ToString & "," & poOUTWORD13.Value.ToString & "," & poOUTWORD18.Value.ToString & "," & poOUTWORD19.Value.ToString & "," & poOUTWORD20.Value.ToString & "," & poSTATUS_CODE.Value.ToString & "," & poOUTWORD22.Value.ToString & "," & poOUTWORD23.Value.ToString & "," & poOUTWORD24.Value.ToString & "," & poOUTWORD25.Value.ToString & "," & poOUTWORD26.Value.ToString & "," & poOUTWORD27.Value.ToString & "," & poOUTWORD28.Value.ToString & "," & poOUTWORD29.Value.ToString & "," & poOUTWORD30.Value.ToString & "," & poOUTWORD31.Value.ToString & "," & poOUTWORD32.Value.ToString & "," & poOUTWORD33.Value.ToString & "," & poOUTWORD34.Value.ToString & "," & poOUTWORD35.Value.ToString & "," & poOUTWORD36.Value.ToString & "," & poOUTWORD37.Value.ToString & "," & poOUTWORD38.Value.ToString & "," & poOUTWORD39.Value.ToString & "," & poOUTWORD40.Value.ToString & "," & poOUTWORD41.Value.ToString & "," & poOUTWORD42.Value.ToString & "," & poOUTWORD43.Value.ToString & "," & poOUTWORD44.Value.ToString & "," & poOUTWORD45.Value.ToString & "," & poOUTWORD46.Value.ToString & "," & poOUTWORD47.Value.ToString & "," & poOUTWORD48.Value.ToString & "," & poOUTWORD49.Value.ToString
            Return ReturnString

        Catch ex As OleDbException
            Throw New Exception(ex.Message)
        Finally
            conn.Close()
        End Try
    End Function
End Class
