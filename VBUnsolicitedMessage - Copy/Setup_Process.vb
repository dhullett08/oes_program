Imports System.Data.OleDb

Public Class Setup_Process
    Private conn As New OleDbConnection


    Public Sub New(ByVal ConnectionString As String)
        conn.ConnectionString = ConnectionString
    End Sub

    Dim mTRANSACTION_ID As Integer  'IN
    Dim mCELL_ID As String          'IN
    Dim mMODEL_ID As String         'IN
    Dim mOPERATION As String        'IN
    Dim mITEM_ID As String          'IN
    Dim mACCESS_ID As Integer       'IN

    'OUT WORDS
    Dim mVal_1, mVal_2, mVal_3, mVal_4, mVal_5, mVal_6, mVal_7, mVal_8, mVal_9, mVal_10, mAccess_ID_1 As Integer
    Dim mVal_11, mVal_12, mVal_13, mVal_14, mVal_15, mVal_16, mVal_17, mVal_18, mVal_19, mVal_20, mAccess_ID_2 As Integer
    Dim mVal_21, mVal_22, mVal_23, mVal_24, mVal_25, mVal_26, mVal_27, mVal_28, mVal_29, mVal_30, mAccess_ID_3 As Integer
    Dim mVal_31, mVal_32, mVal_33, mVal_34, mVal_35, mVal_36, mVal_37, mVal_38, mVal_39, mVal_40, mAccess_ID_4 As Integer
    Dim mVal_41, mVal_42, mVal_43, mVal_44, mVal_45, mVal_46, mVal_47, mVal_48, mVal_49, mVal_50, mAccess_ID_5 As Integer
    Dim mVal_51, mVal_52, mVal_53, mVal_54, mVal_55, mVal_56, mVal_57, mVal_58, mVal_59, mVal_60, mAccess_ID_6 As Integer

    Dim mQUANTITY As Integer        'OUT
    Dim mQUANTITY_2 As Integer      'OUT
    Dim mACKNOWLEDGE As Integer     'OUT
    Dim mERROR_CODE As Integer      'OUT
    Public Property TRANSACTION_ID()
        Get
            Return mTRANSACTION_ID
        End Get
        Set(ByVal value)
            mTRANSACTION_ID = value
        End Set
    End Property

    Public Property CELL_ID()
        Get
            Return mCELL_ID
        End Get
        Set(ByVal value)
            mCELL_ID = value
        End Set
    End Property

    Public Property MODEL_ID()
        Get
            Return mMODEL_ID
        End Get
        Set(ByVal value)
            mMODEL_ID = value
        End Set
    End Property
    Public Property OPERATION()
        Get
            Return mOPERATION
        End Get
        Set(ByVal value)
            mOPERATION = value
        End Set
    End Property
    Public Property ITEM_ID()
        Get
            Return mITEM_ID
        End Get
        Set(ByVal value)
            mITEM_ID = value
        End Set
    End Property
    Public Property ACCESS_ID()
        Get
            Return mACCESS_ID
        End Get
        Set(ByVal value)
            mACCESS_ID = value
        End Set
    End Property
    Public Property P_VAL_1()
        Get
            Return mVal_1
        End Get
        Set(ByVal value)
            mVal_1 = value
        End Set
    End Property
    Public Property P_VAL_2()
        Get
            Return mVal_2
        End Get
        Set(ByVal value)
            mVal_2 = value
        End Set
    End Property
    Public Property P_VAL_3()
        Get
            Return mVal_3
        End Get
        Set(ByVal value)
            mVal_3 = value
        End Set
    End Property
    Public Property P_VAL_4()
        Get
            Return mVal_4
        End Get
        Set(ByVal value)
            mVal_4 = value
        End Set
    End Property
    Public Property P_VAL_5()
        Get
            Return mVal_5
        End Get
        Set(ByVal value)
            mVal_5 = value
        End Set
    End Property
    Public Property P_VAL_6()
        Get
            Return mVal_6
        End Get
        Set(ByVal value)
            mVal_6 = value
        End Set
    End Property
    Public Property P_VAL_7()
        Get
            Return mVal_7
        End Get
        Set(ByVal value)
            mVal_7 = value
        End Set
    End Property
    Public Property P_VAL_8()
        Get
            Return mVal_8
        End Get
        Set(ByVal value)
            mVal_8 = value
        End Set
    End Property
    Public Property P_VAL_9()
        Get
            Return mVal_9
        End Get
        Set(ByVal value)
            mVal_9 = value
        End Set
    End Property
    Public Property P_VAL_10()
        Get
            Return mVal_10
        End Get
        Set(ByVal value)
            mVal_10 = value
        End Set
    End Property
    Public Property P_Access_ID_1()
        Get
            Return mAccess_ID_1
        End Get
        Set(ByVal value)
            mAccess_ID_1 = value
        End Set
    End Property
    Public Property P_VAL_11()
        Get
            Return mVal_11
        End Get
        Set(ByVal value)
            mVal_11 = value
        End Set
    End Property
    Public Property P_VAL_12()
        Get
            Return mVal_12
        End Get
        Set(ByVal value)
            mVal_12 = value
        End Set
    End Property
    Public Property P_VAL_13()
        Get
            Return mVal_13
        End Get
        Set(ByVal value)
            mVal_13 = value
        End Set
    End Property
    Public Property P_VAL_14()
        Get
            Return mVal_14
        End Get
        Set(ByVal value)
            mVal_14 = value
        End Set
    End Property
    Public Property P_VAL_15()
        Get
            Return mVal_15
        End Get
        Set(ByVal value)
            mVal_15 = value
        End Set
    End Property
    Public Property P_VAL_16()
        Get
            Return mVal_16
        End Get
        Set(ByVal value)
            mVal_16 = value
        End Set
    End Property
    Public Property P_VAL_17()
        Get
            Return mVal_17
        End Get
        Set(ByVal value)
            mVal_17 = value
        End Set
    End Property
    Public Property P_VAL_18()
        Get
            Return mVal_18
        End Get
        Set(ByVal value)
            mVal_18 = value
        End Set
    End Property
    Public Property P_VAL_19()
        Get
            Return mVal_19
        End Get
        Set(ByVal value)
            mVal_19 = value
        End Set
    End Property
    Public Property P_VAL_20()
        Get
            Return mVal_20
        End Get
        Set(ByVal value)
            mVal_20 = value
        End Set
    End Property
    Public Property P_Access_ID_2()
        Get
            Return mAccess_ID_2
        End Get
        Set(ByVal value)
            mAccess_ID_2 = value
        End Set
    End Property
    Public Property P_VAL_21()
        Get
            Return mVal_21
        End Get
        Set(ByVal value)
            mVal_21 = value
        End Set
    End Property
    Public Property P_VAL_22()
        Get
            Return mVal_22
        End Get
        Set(ByVal value)
            mVal_22 = value
        End Set
    End Property
    Public Property P_VAL_23()
        Get
            Return mVal_23
        End Get
        Set(ByVal value)
            mVal_23 = value
        End Set
    End Property
    Public Property P_VAL_24()
        Get
            Return mVal_24
        End Get
        Set(ByVal value)
            mVal_24 = value
        End Set
    End Property
    Public Property P_VAL_25()
        Get
            Return mVal_25
        End Get
        Set(ByVal value)
            mVal_25 = value
        End Set
    End Property
    Public Property P_VAL_26()
        Get
            Return mVal_26
        End Get
        Set(ByVal value)
            mVal_26 = value
        End Set
    End Property
    Public Property P_VAL_27()
        Get
            Return mVal_27
        End Get
        Set(ByVal value)
            mVal_27 = value
        End Set
    End Property
    Public Property P_VAL_28()
        Get
            Return mVal_28
        End Get
        Set(ByVal value)
            mVal_28 = value
        End Set
    End Property
    Public Property P_VAL_29()
        Get
            Return mVal_29
        End Get
        Set(ByVal value)
            mVal_29 = value
        End Set
    End Property
    Public Property P_VAL_30()
        Get
            Return mVal_30
        End Get
        Set(ByVal value)
            mVal_30 = value
        End Set
    End Property
    Public Property P_Access_ID_3()
        Get
            Return mAccess_ID_3
        End Get
        Set(ByVal value)
            mAccess_ID_3 = value
        End Set
    End Property
    Public Property P_VAL_31()
        Get
            Return mVal_31
        End Get
        Set(ByVal value)
            mVal_31 = value
        End Set
    End Property
    Public Property P_VAL_32()
        Get
            Return mVal_32
        End Get
        Set(ByVal value)
            mVal_32 = value
        End Set
    End Property
    Public Property P_VAL_33()
        Get
            Return mVal_33
        End Get
        Set(ByVal value)
            mVal_33 = value
        End Set
    End Property
    Public Property P_VAL_34()
        Get
            Return mVal_34
        End Get
        Set(ByVal value)
            mVal_34 = value
        End Set
    End Property
    Public Property P_VAL_35()
        Get
            Return mVal_35
        End Get
        Set(ByVal value)
            mVal_35 = value
        End Set
    End Property
    Public Property P_VAL_36()
        Get
            Return mVal_36
        End Get
        Set(ByVal value)
            mVal_36 = value
        End Set
    End Property
    Public Property P_VAL_37()
        Get
            Return mVal_37
        End Get
        Set(ByVal value)
            mVal_37 = value
        End Set
    End Property
    Public Property P_VAL_38()
        Get
            Return mVal_38
        End Get
        Set(ByVal value)
            mVal_38 = value
        End Set
    End Property
    Public Property P_VAL_39()
        Get
            Return mVal_39
        End Get
        Set(ByVal value)
            mVal_39 = value
        End Set
    End Property
    Public Property P_VAL_40()
        Get
            Return mVal_41
        End Get
        Set(ByVal value)
            mVal_41 = value
        End Set
    End Property
    Public Property P_Access_ID_4()
        Get
            Return mAccess_ID_4
        End Get
        Set(ByVal value)
            mAccess_ID_4 = value
        End Set
    End Property

    Public Property P_VAL_41()
        Get
            Return mVal_41
        End Get
        Set(ByVal value)
            mVal_1 = value
        End Set
    End Property
    Public Property P_VAL_42()
        Get
            Return mVal_42
        End Get
        Set(ByVal value)
            mVal_42 = value
        End Set
    End Property
    Public Property P_VAL_43()
        Get
            Return mVal_43
        End Get
        Set(ByVal value)
            mVal_43 = value
        End Set
    End Property
    Public Property P_VAL_44()
        Get
            Return mVal_44
        End Get
        Set(ByVal value)
            mVal_44 = value
        End Set
    End Property
    Public Property P_VAL_45()
        Get
            Return mVal_45
        End Get
        Set(ByVal value)
            mVal_45 = value
        End Set
    End Property
    Public Property P_VAL_46()
        Get
            Return mVal_46
        End Get
        Set(ByVal value)
            mVal_46 = value
        End Set
    End Property
    Public Property P_VAL_47()
        Get
            Return mVal_47
        End Get
        Set(ByVal value)
            mVal_47 = value
        End Set
    End Property
    Public Property P_VAL_48()
        Get
            Return mVal_48
        End Get
        Set(ByVal value)
            mVal_48 = value
        End Set
    End Property
    Public Property P_VAL_49()
        Get
            Return mVal_49
        End Get
        Set(ByVal value)
            mVal_49 = value
        End Set
    End Property
    Public Property P_VAL_50()
        Get
            Return mVal_50
        End Get
        Set(ByVal value)
            mVal_50 = value
        End Set
    End Property
    Public Property P_Access_ID_5()
        Get
            Return mAccess_ID_5
        End Get
        Set(ByVal value)
            mAccess_ID_5 = value
        End Set
    End Property
    Public Property P_VAL_51()
        Get
            Return mVal_51
        End Get
        Set(ByVal value)
            mVal_51 = value
        End Set
    End Property
    Public Property P_VAL_52()
        Get
            Return mVal_52
        End Get
        Set(ByVal value)
            mVal_52 = value
        End Set
    End Property
    Public Property P_VAL_53()
        Get
            Return mVal_53
        End Get
        Set(ByVal value)
            mVal_53 = value
        End Set
    End Property
    Public Property P_VAL_54()
        Get
            Return mVal_54
        End Get
        Set(ByVal value)
            mVal_54 = value
        End Set
    End Property
    Public Property P_VAL_55()
        Get
            Return mVal_55
        End Get
        Set(ByVal value)
            mVal_55 = value
        End Set
    End Property
    Public Property P_VAL_56()
        Get
            Return mVal_56
        End Get
        Set(ByVal value)
            mVal_56 = value
        End Set
    End Property
    Public Property P_VAL_57()
        Get
            Return mVal_57
        End Get
        Set(ByVal value)
            mVal_57 = value
        End Set
    End Property
    Public Property P_VAL_58()
        Get
            Return mVal_58
        End Get
        Set(ByVal value)
            mVal_58 = value
        End Set
    End Property
    Public Property P_VAL_59()
        Get
            Return mVal_59
        End Get
        Set(ByVal value)
            mVal_59 = value
        End Set
    End Property
    Public Property P_VAL_60()
        Get
            Return mVal_60
        End Get
        Set(ByVal value)
            mVal_60 = value
        End Set
    End Property
    Public Property P_Access_ID_6()
        Get
            Return mAccess_ID_6
        End Get
        Set(ByVal value)
            mAccess_ID_6 = value
        End Set
    End Property
    Public Property QUANTITY()
        Get
            Return mQUANTITY
        End Get
        Set(ByVal value)
            mQUANTITY = value
        End Set
    End Property
    Public Property QUANTITY_2()
        Get
            Return mQUANTITY_2
        End Get
        Set(ByVal value)
            mQUANTITY_2 = value
        End Set
    End Property
    Public Property ACKNOWLEDGE()
        Get
            Return mACKNOWLEDGE
        End Get
        Set(ByVal value)
            mACKNOWLEDGE = value
        End Set
    End Property
    Public Property ERROR_CODE()
        Get
            Return mERROR_CODE
        End Get
        Set(ByVal value)
            mERROR_CODE = value
        End Set
    End Property




    Public Function CallSetupProcess(ByVal SetupObject As Setup_Process)
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Author:                       Jeff Judkins
        ' Date:                         03-14-2007
        ' Schema:                       
        ' Table References:                   
        ' Description:                  
        ' Revisions:
        ' Notes:                        
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim cmd As New OleDbCommand("arc.oes_BOM_SETUP", conn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim P_TRANSACTION_ID As New OleDbParameter("TRANSACTION_ID", OleDbType.VarNumeric)
        P_TRANSACTION_ID.Direction = ParameterDirection.Input
        cmd.Parameters.Add(P_TRANSACTION_ID).Value = SetupObject.TRANSACTION_ID

        Dim P_CELL_ID As New OleDbParameter("CELL_ID", OleDbType.VarChar, 40)
        P_CELL_ID.Direction = ParameterDirection.Input
        cmd.Parameters.Add(P_CELL_ID).Value = SetupObject.CELL_ID

        Dim P_MODEL_ID As New OleDbParameter("MODEL_ID", OleDbType.VarChar, 40)
        P_MODEL_ID.Direction = ParameterDirection.Input
        cmd.Parameters.Add(P_MODEL_ID).Value = SetupObject.MODEL_ID

        Dim P_OPERATION As New OleDbParameter("OPERATION", OleDbType.VarChar, 40)
        P_OPERATION.Direction = ParameterDirection.Input
        cmd.Parameters.Add(P_OPERATION).Value = SetupObject.OPERATION

        Dim P_ITEM_ID As New OleDbParameter("ITEM_ID", OleDbType.VarChar, 40)
        P_ITEM_ID.Direction = ParameterDirection.Input
        cmd.Parameters.Add(P_ITEM_ID).Value = SetupObject.ITEM_ID

        Dim P_ACCESS_ID As New OleDbParameter("ACCESS_ID", OleDbType.VarNumeric)
        P_ACCESS_ID.Direction = ParameterDirection.Input
        cmd.Parameters.Add(P_ACCESS_ID).Value = SetupObject.ACCESS_ID

        ' OUTWORDS
        Dim P_VAL_1 As New OleDbParameter("VAL_1", OleDbType.VarNumeric)
        P_VAL_1.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_1)

        Dim P_VAL_2 As New OleDbParameter("VAL_2", OleDbType.VarNumeric)
        P_VAL_2.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_2)

        Dim P_VAL_3 As New OleDbParameter("VAL_3", OleDbType.VarNumeric)
        P_VAL_3.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_3)

        Dim P_VAL_4 As New OleDbParameter("VAL_4", OleDbType.VarNumeric)
        P_VAL_4.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_4)

        Dim P_VAL_5 As New OleDbParameter("VAL_5", OleDbType.VarNumeric)
        P_VAL_5.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_5)

        Dim P_VAL_6 As New OleDbParameter("VAL_6", OleDbType.VarNumeric)
        P_VAL_6.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_6)

        Dim P_VAL_7 As New OleDbParameter("VAL_7", OleDbType.VarNumeric)
        P_VAL_7.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_7)

        Dim P_VAL_8 As New OleDbParameter("VAL_8", OleDbType.VarNumeric)
        P_VAL_8.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_8)

        Dim P_VAL_9 As New OleDbParameter("VAL_9", OleDbType.VarNumeric)
        P_VAL_9.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_9)

        Dim P_VAL_10 As New OleDbParameter("VAL_10", OleDbType.VarNumeric)
        P_VAL_10.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_10)

        Dim P_ACCESS_ID_1 As New OleDbParameter("ACCESS_ID_1", OleDbType.VarNumeric)
        P_ACCESS_ID_1.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_ACCESS_ID_1)

        Dim P_VAL_11 As New OleDbParameter("VAL_11", OleDbType.VarNumeric)
        P_VAL_11.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_11)

        Dim P_VAL_12 As New OleDbParameter("VAL_12", OleDbType.VarNumeric)
        P_VAL_12.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_12)

        Dim P_VAL_13 As New OleDbParameter("VAL_13", OleDbType.VarNumeric)
        P_VAL_13.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_13)

        Dim P_VAL_14 As New OleDbParameter("VAL_14", OleDbType.VarNumeric)
        P_VAL_14.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_14)

        Dim P_VAL_15 As New OleDbParameter("VAL_15", OleDbType.VarNumeric)
        P_VAL_15.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_15)

        Dim P_VAL_16 As New OleDbParameter("VAL_16", OleDbType.VarNumeric)
        P_VAL_16.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_16)

        Dim P_VAL_17 As New OleDbParameter("VAL_17", OleDbType.VarNumeric)
        P_VAL_17.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_17)

        Dim P_VAL_18 As New OleDbParameter("VAL_18", OleDbType.VarNumeric)
        P_VAL_18.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_18)

        Dim P_VAL_19 As New OleDbParameter("VAL_19", OleDbType.VarNumeric)
        P_VAL_19.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_19)

        Dim P_VAL_20 As New OleDbParameter("VAL_20", OleDbType.VarNumeric)
        P_VAL_20.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_20)

        Dim P_ACCESS_ID_2 As New OleDbParameter("ACCESS_ID_2", OleDbType.VarNumeric)
        P_ACCESS_ID_2.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_ACCESS_ID_2)

        Dim P_VAL_21 As New OleDbParameter("VAL_21", OleDbType.VarNumeric)
        P_VAL_21.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_21)

        Dim P_VAL_22 As New OleDbParameter("VAL_22", OleDbType.VarNumeric)
        P_VAL_22.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_22)

        Dim P_VAL_23 As New OleDbParameter("VAL_23", OleDbType.VarNumeric)
        P_VAL_23.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_23)

        Dim P_VAL_24 As New OleDbParameter("VAL_24", OleDbType.VarNumeric)
        P_VAL_24.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_24)

        Dim P_VAL_25 As New OleDbParameter("VAL_25", OleDbType.VarNumeric)
        P_VAL_25.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_25)

        Dim P_VAL_26 As New OleDbParameter("VAL_26", OleDbType.VarNumeric)
        P_VAL_26.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_26)

        Dim P_VAL_27 As New OleDbParameter("VAL_27", OleDbType.VarNumeric)
        P_VAL_27.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_27)

        Dim P_VAL_28 As New OleDbParameter("VAL_28", OleDbType.VarNumeric)
        P_VAL_28.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_28)

        Dim P_VAL_29 As New OleDbParameter("VAL_29", OleDbType.VarNumeric)
        P_VAL_29.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_29)

        Dim P_VAL_30 As New OleDbParameter("VAL_30", OleDbType.VarNumeric)
        P_VAL_30.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_30)

        Dim P_ACCESS_ID_3 As New OleDbParameter("ACCESS_ID_3", OleDbType.VarNumeric)
        P_ACCESS_ID_3.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_ACCESS_ID_3)

        Dim P_VAL_31 As New OleDbParameter("VAL_31", OleDbType.VarNumeric)
        P_VAL_31.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_31)

        Dim P_VAL_32 As New OleDbParameter("VAL_32", OleDbType.VarNumeric)
        P_VAL_32.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_32)

        Dim P_VAL_33 As New OleDbParameter("VAL_33", OleDbType.VarNumeric)
        P_VAL_33.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_33)

        Dim P_VAL_34 As New OleDbParameter("VAL_34", OleDbType.VarNumeric)
        P_VAL_34.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_34)

        Dim P_VAL_35 As New OleDbParameter("VAL_35", OleDbType.VarNumeric)
        P_VAL_35.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_35)

        Dim P_VAL_36 As New OleDbParameter("VAL_36", OleDbType.VarNumeric)
        P_VAL_36.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_36)

        Dim P_VAL_37 As New OleDbParameter("VAL_37", OleDbType.VarNumeric)
        P_VAL_37.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_37)

        Dim P_VAL_38 As New OleDbParameter("VAL_38", OleDbType.VarNumeric)
        P_VAL_38.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_38)

        Dim P_VAL_39 As New OleDbParameter("VAL_39", OleDbType.VarNumeric)
        P_VAL_39.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_39)

        Dim P_VAL_40 As New OleDbParameter("VAL_40", OleDbType.VarNumeric)
        P_VAL_40.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_40)

        Dim P_ACCESS_ID_4 As New OleDbParameter("ACCESS_ID_4", OleDbType.VarNumeric)
        P_ACCESS_ID_4.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_ACCESS_ID_4)

        Dim P_VAL_41 As New OleDbParameter("VAL_41", OleDbType.VarNumeric)
        P_VAL_41.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_41)

        Dim P_VAL_42 As New OleDbParameter("VAL_42", OleDbType.VarNumeric)
        P_VAL_42.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_42)

        Dim P_VAL_43 As New OleDbParameter("VAL_43", OleDbType.VarNumeric)
        P_VAL_43.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_43)

        Dim P_VAL_44 As New OleDbParameter("VAL_44", OleDbType.VarNumeric)
        P_VAL_44.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_44)

        Dim P_VAL_45 As New OleDbParameter("VAL_45", OleDbType.VarNumeric)
        P_VAL_45.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_45)

        Dim P_VAL_46 As New OleDbParameter("VAL_46", OleDbType.VarNumeric)
        P_VAL_46.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_46)

        Dim P_VAL_47 As New OleDbParameter("VAL_47", OleDbType.VarNumeric)
        P_VAL_47.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_47)

        Dim P_VAL_48 As New OleDbParameter("VAL_48", OleDbType.VarNumeric)
        P_VAL_48.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_48)

        Dim P_VAL_49 As New OleDbParameter("VAL_49", OleDbType.VarNumeric)
        P_VAL_49.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_49)

        Dim P_VAL_50 As New OleDbParameter("VAL_50", OleDbType.VarNumeric)
        P_VAL_50.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_50)

        Dim P_ACCESS_ID_5 As New OleDbParameter("ACCESS_ID_5", OleDbType.VarNumeric)
        P_ACCESS_ID_5.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_ACCESS_ID_5)

        Dim P_VAL_51 As New OleDbParameter("VAL_51", OleDbType.VarNumeric)
        P_VAL_51.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_51)

        Dim P_VAL_52 As New OleDbParameter("VAL_52", OleDbType.VarNumeric)
        P_VAL_52.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_52)

        Dim P_VAL_53 As New OleDbParameter("VAL_53", OleDbType.VarNumeric)
        P_VAL_53.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_53)

        Dim P_VAL_54 As New OleDbParameter("VAL_54", OleDbType.VarNumeric)
        P_VAL_54.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_54)

        Dim P_VAL_55 As New OleDbParameter("VAL_55", OleDbType.VarNumeric)
        P_VAL_55.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_55)

        Dim P_VAL_56 As New OleDbParameter("VAL_56", OleDbType.VarNumeric)
        P_VAL_56.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_56)

        Dim P_VAL_57 As New OleDbParameter("VAL_57", OleDbType.VarNumeric)
        P_VAL_57.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_57)

        Dim P_VAL_58 As New OleDbParameter("VAL_58", OleDbType.VarNumeric)
        P_VAL_58.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_58)

        Dim P_VAL_59 As New OleDbParameter("VAL_59", OleDbType.VarNumeric)
        P_VAL_59.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_59)

        Dim P_VAL_60 As New OleDbParameter("VAL_60", OleDbType.VarNumeric)
        P_VAL_60.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_VAL_60)

        Dim P_ACCESS_ID_6 As New OleDbParameter("ACCESS_ID_6", OleDbType.VarNumeric)
        P_ACCESS_ID_6.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_ACCESS_ID_6)

        Dim P_QUANTITY As New OleDbParameter("QUANTITY", OleDbType.VarNumeric)
        P_QUANTITY.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_QUANTITY)

        Dim P_QUANTITY_2 As New OleDbParameter("QUANTITY_2", OleDbType.VarNumeric)
        P_QUANTITY_2.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_QUANTITY_2)

        Dim P_ACKNOWLEDGE As New OleDbParameter("ACKNOWLEDGE", OleDbType.VarNumeric)
        P_ACKNOWLEDGE.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_ACKNOWLEDGE)

        Dim P_ERROR_CODE As New OleDbParameter("ERROR_CODE", OleDbType.VarNumeric)
        P_ERROR_CODE.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_ERROR_CODE)


        Try
            conn.Open()
            cmd.ExecuteNonQuery()
            Dim ReturnString As String
            ReturnString = P_VAL_1.Value.ToString & "," & P_VAL_2.Value.ToString & "," & P_VAL_3.Value.ToString & "," & P_VAL_4.Value.ToString & "," & P_VAL_5.Value.ToString & "," & P_VAL_6.Value.ToString & "," & P_VAL_7.Value.ToString & "," & P_VAL_8.Value.ToString & "," & P_VAL_9.Value.ToString & "," & P_VAL_10.Value.ToString & "," & P_ACCESS_ID_1.Value & "," & P_VAL_11.Value.ToString & "," & P_VAL_12.Value.ToString & "," & P_VAL_13.Value.ToString & "," & P_VAL_14.Value.ToString & "," & P_VAL_15.Value.ToString & "," & P_VAL_16.Value.ToString & "," & P_VAL_17.Value.ToString & "," & P_VAL_18.Value.ToString & "," & P_VAL_19.Value.ToString & "," & P_VAL_20.Value.ToString & "," & P_ACCESS_ID_2.Value.ToString & "," & P_VAL_21.Value.ToString & "," & P_VAL_22.Value.ToString & "," & P_VAL_23.Value.ToString & "," & P_VAL_24.Value.ToString & "," & P_VAL_25.Value.ToString & "," & P_VAL_26.Value.ToString & "," & P_VAL_27.Value.ToString & "," & P_VAL_28.Value.ToString & "," & P_VAL_29.Value.ToString & "," & P_VAL_30.Value.ToString & "," & P_ACCESS_ID_3.Value.ToString & "," & P_VAL_31.Value.ToString & "," & P_VAL_32.Value.ToString & "," & P_VAL_33.Value.ToString & "," & P_VAL_34.Value.ToString & "," & P_VAL_35.Value.ToString & "," & P_VAL_36.Value.ToString & "," & P_VAL_37.Value.ToString & "," & P_VAL_38.Value.ToString & "," & P_VAL_39.Value.ToString & "," & P_VAL_40.Value.ToString & "," & P_ACCESS_ID_4.Value & "," & P_VAL_41.Value.ToString & "," & P_VAL_42.Value.ToString & "," & P_VAL_43.Value.ToString & "," & P_VAL_44.Value.ToString & "," & P_VAL_45.Value.ToString & "," & P_VAL_46.Value.ToString & "," & P_VAL_47.Value.ToString & "," & P_VAL_48.Value.ToString & "," & P_VAL_49.Value.ToString & "," & P_VAL_50.Value.ToString & "," & P_ACCESS_ID_5.Value & "," & P_VAL_51.Value.ToString & "," & P_VAL_52.Value.ToString & "," & P_VAL_53.Value.ToString & "," & P_VAL_54.Value.ToString & "," & P_VAL_55.Value.ToString & "," & P_VAL_56.Value.ToString & "," & P_VAL_57.Value.ToString & "," & P_VAL_58.Value.ToString & "," & P_VAL_59.Value.ToString & "," & P_VAL_60.Value.ToString & "," & P_ACCESS_ID_6.Value.ToString & "," & P_QUANTITY.Value.ToString & "," & P_QUANTITY_2.Value.ToString & "," & P_ACKNOWLEDGE.Value.ToString & "," & P_ERROR_CODE.Value.ToString
            Return ReturnString
        Catch ex As OleDbException
            Throw New Exception(ex.Message)
        Finally

            If conn IsNot Nothing Then
                conn.Close()
                conn.Dispose()
                cmd.Dispose()
            End If
        End Try
    End Function
End Class

