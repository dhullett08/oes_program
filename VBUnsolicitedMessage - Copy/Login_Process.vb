Imports System.Data.OleDb

Public Class Login_Process
    Private conn As New OleDbConnection


    Public Sub New(ByVal ConnectionString As String)
        conn.ConnectionString = ConnectionString
    End Sub

    Dim mTNX_ID As Integer          'IN
    Dim mCELL_ID As String          'IN
    Dim mEMPLOYEE_ID As String      'IN
    Dim mERROR_CODE As Integer      'OUT
    Dim mACKNOWLEDGE As Integer     'OUT

    Public Property TNX_ID()
        Get
            Return mTNX_ID
        End Get
        Set(ByVal value)
            mTNX_ID = value
        End Set
    End Property
    Public Property CELL_ID()
        Get
            Return mCELL_ID
        End Get
        Set(ByVal value)
            mCELL_ID = value
        End Set
    End Property
    Public Property EMPLOYEE_ID()
        Get
            Return mEMPLOYEE_ID
        End Get
        Set(ByVal value)
            mEMPLOYEE_ID = value
        End Set
    End Property
    Public Property ERROR_CODE()
        Get
            Return mERROR_CODE
        End Get
        Set(ByVal value)
            mERROR_CODE = value
        End Set
    End Property
    Public Property ACKNOWLEDGE()
        Get
            Return mACKNOWLEDGE
        End Get
        Set(ByVal value)
            mACKNOWLEDGE = value
        End Set
    End Property


    Public Function CallLoginProcess(ByVal LoginObject As Login_Process) As String
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ' Author:                       Jeff Judkins
        ' Date:                         03-13-2007
        ' Schema:                       
        ' Table References:                   
        ' Description:                  
        ' Revisions:
        ' Notes:                        
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim cmd As New OleDbCommand("arc.oes_log_inout", conn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim P_TNX_ID As New OleDbParameter("TNX_ID", OleDbType.VarChar, 40)
        P_TNX_ID.Direction = ParameterDirection.Input
        cmd.Parameters.Add(P_TNX_ID).Value = LoginObject.mTNX_ID

        Dim P_CELL_ID As New OleDbParameter("CELL_ID", OleDbType.VarChar, 40)
        P_CELL_ID.Direction = ParameterDirection.Input
        cmd.Parameters.Add(P_CELL_ID).Value = LoginObject.mCELL_ID

        Dim P_EMPLOYEE_ID As New OleDbParameter("EMPLOYEE_ID", OleDbType.VarChar, 40)
        P_EMPLOYEE_ID.Direction = ParameterDirection.Input
        cmd.Parameters.Add(P_EMPLOYEE_ID).Value = LoginObject.mEMPLOYEE_ID


        ' OUTWORDS

        Dim P_ERROR_CODE As New OleDbParameter("ERROR_CODE", OleDbType.VarNumeric)
        P_ERROR_CODE.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_ERROR_CODE)

        Dim P_ACKNOWLEDGE As New OleDbParameter("ACKNOWLEDGE", OleDbType.VarNumeric)
        P_ACKNOWLEDGE.Direction = ParameterDirection.ReturnValue
        cmd.Parameters.Add(P_ACKNOWLEDGE)

        Try
            conn.Open()
            cmd.ExecuteNonQuery()
            Dim ReturnString As String
            ReturnString = P_ERROR_CODE.Value & "," & P_ACKNOWLEDGE.Value
            Return ReturnString

        Catch ex As OleDbException
            Throw New Exception(ex.Message)
        Finally
            conn.Close()
        End Try
    End Function
End Class

